#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define MEM_SIZE 512
#define BUF_SIZE 1024

int *load(int *pos) {
    char buf[BUF_SIZE];
    int *m = calloc(MEM_SIZE, sizeof(int));
    char *end;

    *pos = 0;
    while (fgets(buf, BUF_SIZE, stdin)) {
        char *tok = strtok(buf, ",");
        m[(*pos)++] = strtol(tok, &end, 10);
        assert(*end == '\0' || *end == '\n');
        assert(*pos < MEM_SIZE);

        while ((tok = strtok(NULL, ","))) {
            m[(*pos)++] = strtol(tok, &end, 10);
            assert(*end == '\0' || *end == '\n');
            assert(*pos < MEM_SIZE);
        }
    }
    return m;
}

void dump_memory(int *mem, int size) {
    for (int i = 0; i < size; i++) {
        printf("%d,", mem[i]);
    }
    putchar('\n');
}

void run(int *mem) {
    int pc = 0;

    while (mem[pc] != 99) {
        switch(mem[pc]) {
            case 1:
                mem[mem[pc + 3]] = mem[mem[pc + 1]] + mem[mem[pc + 2]];
                break;
            case 2:
                mem[mem[pc + 3]] = mem[mem[pc + 1]] * mem[mem[pc + 2]];
                break;
        }
        pc += 4;
    }
}

int main(void) {
    int size;

    /* load code from stdin */
    int *mem = load(&size);

    /* setup program */
    mem[1] = 12;
    mem[2] = 2;

    /* run the interpreter */
    run(mem);

    printf("%d\n", mem[0]);
    return 0;
}


