#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define BUFSIZE 255

#define DEFAULT_SIZE 10

struct object {
    char name[32];
    struct list *orbits;
};

/* Dynamic list of objects. Used for index and in objects */
struct list {
    size_t size;
    size_t capacity;
    struct object **data;
};

struct list *list_init(void) {
    struct list *l = malloc(sizeof(struct list));
    if (!l) {
        return NULL;
    }
    l->data = malloc(DEFAULT_SIZE * sizeof(struct object*));
    if (!l->data) {
        free(l);
        return NULL;
    }
    l->size = 0;
    l->capacity = DEFAULT_SIZE;
    return l;
}

void list_cleanup(struct list *l) {
    free(l->data);
    free(l);
}

int list_push(struct list *l, struct object *object) {
    if (l->size == l->capacity) {
        struct object **tmp_data = realloc(l->data, l->capacity * 2 * sizeof(struct object*));
        if (!tmp_data) {
            return 1;
        }
        l->data = tmp_data;
        l->capacity *= 2;
    }
    l->data[l->size] = object;
    l->size += 1;
    return 0;
}

struct object *list_pop(struct list *l) {
    assert(l->size > 1);
    l->size--;
    return l->data[l->size];
}

/* create object with name and empty orbits list */
struct object *create_object(char *name) {
    struct object *obj = malloc(sizeof(struct object));
    if (!obj) {
        return NULL;
    }
    strcpy(obj->name, name);
    obj->orbits = list_init();
    if (!obj->orbits) {
        free(obj);
        return NULL;
    }
    return obj;
}

void cleanup_object(struct object *obj) {
    for (size_t i = 0; i < obj->orbits->size; i++) {
        cleanup_object(obj->orbits->data[i]);
    }
    list_cleanup(obj->orbits);
    free(obj);
}

/* return **obj because then we can remove the object if we want */
struct object **find_object_in_index(char *name, struct list *l) {
    for (size_t i = 0; i < l->size; i++) {
        if (strcmp(name, l->data[i]->name) == 0) {
            return &(l->data[i]);
        }
    }
    return NULL;
}

/* recursively search object tree */
struct object *search_subtree(char *name, struct object *object) {
    struct object *res;
    if (strcmp(name, object->name) == 0) {
        return object;
    }
    for (size_t i = 0; i < object->orbits->size; i++) {
         res = search_subtree(name, object->orbits->data[i]);
         if (res) {
             return res;
         }
    }
    return NULL;
}

/* find object in index and all subtrees of objects in index */
struct object *find_object_in_subtrees(char *name, struct list *l) {
    struct object *object;
    for (size_t i = 0; i < l->size; i++) {
        if (strcmp(name, l->data[i]->name) == 0) {
            return l->data[i];
        } else if ( (object = search_subtree(name, l->data[i])) ) {
            return object;
        }
    }
    return NULL;
}

/* find obj, create and add to index if not found */
struct object *get_and_add(char *name, struct list *l) {
    struct object *object = find_object_in_subtrees(name, l);
    if (!object) {
        object = create_object(name);
        list_push(l, object);
    }
    return object;
}

/* find object, remove from list if found. */
struct object *get_and_remove(char *name, struct list *l) {
    struct object *res;
    /* find object */
    struct object **object = find_object_in_index(name, l);
    if (!object) {
        return create_object(name);
    }
    /* remove from list and fill hole */
    struct object *last = list_pop(l);
    res = *object;
    if (last != *object) {
        *object = last;
    }
    return res;
}

/* read orbit data. */
struct object *read_orbits(void) {
    char buf[BUFSIZE];
    struct object *obj1 = NULL;
    struct object *obj2 = NULL;
    struct list *l = list_init();

    while (fgets(buf, BUFSIZE, stdin)) {
        char *name1 = strtok(buf, ")\n");
        char *name2 = strtok(NULL, ")\n");
        printf("%s %s\n", name1, name2);

        obj1 = get_and_add(name1, l);
        obj2 = get_and_remove(name2, l);
        list_push(obj1->orbits, obj2);
    }

    assert(l->size == 1);
    struct object *com = l->data[0];
    list_cleanup(l);
    return com;
}

void num_orbits(struct object *obj, int len_to_here, int *total_path) {
    for (size_t i = 0; i < obj->orbits->size; i++) {
        num_orbits(obj->orbits->data[i], len_to_here + 1, total_path);
    }
    *total_path += len_to_here;
    return;
}

int main(void) {
    struct object *com = read_orbits();
    if (!com) {
        printf("read_orbits failed\n");
        return 1;
    }

    int total_path = 0;
    num_orbits(com, 0, &total_path);
    printf("%d\n", total_path);

    cleanup_object(com);
    return 0;
}
