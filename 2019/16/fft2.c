#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NCOPIES 4

/* global pattern */
const int base_pat[] = {0, 1, 0, -1};
size_t pat_len = sizeof(base_pat) / sizeof(base_pat[0]);

/* read sequence as nul terminated string. */
char *read_sequence(void) {
    char *buf = NULL;
    size_t size = 0;
    if (getline(&buf, &size, stdin) == -1) {
        free(buf);
        return NULL;
    }
    if (buf[strlen(buf) - 1]  == '\n') {
        buf[strlen(buf) - 1]  = '\0';
    }
    // need to copy the input 10000 times
    char *res = calloc(NCOPIES + 1, strlen(buf)); // waste some space for term
    for (size_t i = 0; i < NCOPIES; i++) {
        strcpy(res + i * strlen(buf), buf);
    }
    // already null terminated because of calloc.
    free(buf);
    return res;
}

void print_pattern(int *pat, size_t size) {
    for (size_t i = 0; i < size; i++) {
        printf("%d ", pat[i]);
    }
    putchar('\n');
}

/* return the generated pattern for position pos. */
int *gen_pattern(size_t pos, size_t *size) {
    pos += 1; // pos starts at 0, we need to start at 1.
    int *pat = calloc(pos * pat_len, sizeof(int));
    if (!pat) {
        return NULL;
    }
    for (size_t i = 0; i < pat_len; i++) {
        for (size_t j = 0; j < pos; j++) {
            pat[i * pos + j] = base_pat[i];
        }
    }
    *size = pos * pat_len;
    return pat;
}

char calc_entry(char *seq, size_t pos) {
    size_t size = 0;
    int *gen_pat = gen_pattern(pos, &size);

    long sum = 0;
    size_t i = 1;
    for (char *c = seq; *c; c++) {
        sum += (*c - '0') * gen_pat[i++];
        if (i % size == 0) {
            i = 0;
        }
    }
    free(gen_pat);
    return '0' + labs(sum) % 10; // return first digit as ascii char.
}

char *calc_phase(char *seq) {
    char *out_seq = calloc(strlen(seq) + 1, sizeof(char));
    for (size_t i = 0; i < strlen(seq); i++) {
        out_seq[i] = calc_entry(seq, i);
    }
    return out_seq;
}

int main(void) {
    char *seq = read_sequence();
    char *out = NULL;

    printf("len: %lu\n", strlen(seq));
    for (size_t i = 0; i < 100; i++) {
        printf("%s\n", seq);
        out = calc_phase(seq);
        // compare seq and out here
        free(seq);
        seq = out;
    }
    printf("%s\n", seq);
    char output[9] = {0};
    strncpy(output, seq, 8);
    printf("solution: %s\n", output);

    /* cleanup */
    free(seq);
    return 0;
}
