/* allow acces to data structure */
struct list {
    size_t size;
    size_t capacity;
    int *data;
};

struct list *list_init(void);
void list_cleanup(struct list *l);
int list_push(struct list *l, int e);
int list_pop(struct list *l);
int list_remove(struct list *l, size_t index);
struct list *list_clone(struct list *l);

