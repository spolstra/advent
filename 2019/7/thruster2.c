#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "list.h"

#define MEM_SIZE 2048
#define BUF_SIZE 1024
#define MAX_PARAM 3

#define NUM_THRUSTERS 5

/* input to the program */
#define INPUT 5

enum instr_t {ADD = 1, MULT = 2, READ = 3, WRITE = 4,
              JMPT = 5, JMPF = 6, SETLT = 7, SETE = 8, HALT = 99};

int *load(int *pos) {
    char buf[BUF_SIZE];
    int *m = calloc(MEM_SIZE, sizeof(int));
    char *end;

    *pos = 0;
    while (fgets(buf, BUF_SIZE, stdin)) {
        char *tok = strtok(buf, ",");
        m[(*pos)++] = strtol(tok, &end, 10);
        assert(*end == '\0' || *end == '\n');
        assert(*pos < MEM_SIZE);

        while ((tok = strtok(NULL, ","))) {
            m[(*pos)++] = strtol(tok, &end, 10);
            assert(*end == '\0' || *end == '\n');
            assert(*pos < MEM_SIZE);
        }
    }
    return m;
}

void dump_memory(int *mem, int size) {
    for (int i = 0; i < size; i++) {
        printf("%d,", mem[i]);
    }
    putchar('\n');
}

void get_params(int pc, int *mem, int *params, int n) {
    int param_base[MAX_PARAM] = {100, 1000, 10000};
    for (int i = 0; i < n; i++) {
        if ((mem[pc] / param_base[i]) % 10 == 1) {
            assert(i != 2); // cannot write to dest parameter as immediate
            params[i] = mem[pc + i + 1];
        } else {
            params[i] = mem[mem[pc + i + 1]];
        }
    }
}


int run(int *mem, int pc, struct list *input, struct list *output) {
    int params[MAX_PARAM];

    while ((mem[pc] % 100) != HALT) {
        switch(mem[pc] % 100) {
            case ADD:
                get_params(pc, mem, params, 3);
                mem[mem[pc + 3]] = params[0] + params[1];
                pc += 4;
                break;
            case MULT:
                get_params(pc, mem, params, 3);
                mem[mem[pc + 3]] = params[0] * params[1];
                pc += 4;
                break;
            case READ:
                if (input->size == 0) {
                    return pc; // preempt this instance if we have no data */
                }
                mem[mem[pc + 1]] = list_remove(input, 0); // read from front of list
                pc += 2;
                break;
            case WRITE:
                get_params(pc, mem, params, 1);
                list_push(output, params[0]);
                pc += 2;
                break;
            case JMPT:
                get_params(pc, mem, params, 2);
                if (params[0] != 0) {
                    pc = params[1];
                } else {
                    pc += 3;
                }
                break;
            case JMPF:
                get_params(pc, mem, params, 2);
                if (params[0] == 0) {
                    pc = params[1];
                } else {
                    pc += 3;
                }
                break;
            case SETLT:
                get_params(pc, mem, params, 3);
                if (params[0] < params[1]) {
                    mem[mem[pc + 3]] = 1; // TODO always position?
                } else {
                    mem[mem[pc + 3]] = 0;
                }
                pc += 4;
                break;
            case SETE:
                get_params(pc, mem, params, 3);
                if (params[0] == params[1]) {
                    mem[mem[pc + 3]] = 1; // TODO always position?
                } else {
                    mem[mem[pc + 3]] = 0;
                }
                pc += 4;
                break;
            default:
                printf("Unknown opcode %d at pc %d\n", mem[pc], pc);
                return -1;
        }
    }
    return 0;
}

void permutate(struct list *prefix, struct list *l, char *out_buf) {
    char buf[10];
    if (l->size == 1) {
        for (size_t i = 0; i < prefix->size; i++) {
            sprintf(buf, "%d", prefix->data[i]);
            strcat(out_buf, buf);
        }
        sprintf(buf, "%d", l->data[0]);
        strcat(out_buf, buf);
        return;
    }
    for (size_t i = 0; i < l->size; i++) {
        struct list *l2 = list_clone(l);
        list_push(prefix, l2->data[i]);
        list_remove(l2, i);
        permutate(prefix, l2, out_buf);
        list_remove(prefix, prefix->size - 1);
        list_cleanup(l2);
    }
}

char *gen_permutations(int num, int offset) {
    struct list *l = list_init();
    struct list *prefix = list_init();
    char *buf = calloc(4096, sizeof(char));
    for (int i = offset; i < num + offset; i++) {
        list_push(l, i);
    }
    permutate(prefix, l, buf);
    list_cleanup(l);
    list_cleanup(prefix);
    return buf;
}

void print_perms(char *perms) {
    size_t index = 0;
    while (perms[index]) {
        printf("%c ", perms[index]);
        index++;
        if (index % 5 == 0) {
            putchar('\n');

        }
    }
}

/* initialize all mems with contents of mem */
int **setup_mems(int *mem, int num) {
    int **mems = malloc(sizeof(int*) * num);
    for (int i = 0; i < num; i++) {
        mems[i] = malloc(sizeof(int) * MEM_SIZE);
        memcpy(mems[i], mem, MEM_SIZE * sizeof(int));
    }
    return mems;
}

void cleanup_mems(int **mems, int num) {
    for (int i = 0; i < num; i++) {
        free(mems[i]);
    }
    free(mems);
}

struct list **setup_buffers(int num) {
    struct list **buffers = malloc(sizeof(struct list*) * num);
    for (int i = 0; i < num; i++) {
        buffers[i] = list_init();
    }
    return buffers;
}

void cleanup_buffers(struct list **buffers, int num) {
    for (int i = 0; i < num; i++) {
        list_cleanup(buffers[i]);
    }
    free(buffers);
}

/* return input index for instance i.
 * Defined as i - 1, module 5, so input of 0 is 4. */
int input_buf(int i) {
    return (i + NUM_THRUSTERS - 1) % NUM_THRUSTERS;
}

int output_buf(int i) {
    return i;
}

int main(void) {
    int size;
    int ret_code = 0;
    char *perms = gen_permutations(NUM_THRUSTERS, 5);
    // print_perms(perms);
    // free(perms) ; return 0;

    /* setup instance memories, pc's and buffers */
    int *mem_orig = load(&size);
    int pcs[NUM_THRUSTERS] = {0};
    struct list **buffers = setup_buffers(NUM_THRUSTERS);

    size_t pi = 0; // index in permutation string
    int max_thrust = 0;
    int max_seq[NUM_THRUSTERS] = {0};
    int thrust = 0;

    while (perms[pi]) {
        printf("pi: %zu\n", pi);
        int **mems = setup_mems(mem_orig, NUM_THRUSTERS);
        for (int i = 0; i < NUM_THRUSTERS; i++) {
            // add phase input to all buffers
            list_push(buffers[input_buf(i)], perms[pi++] - '0'); // phase setting
        }
        list_push(buffers[input_buf(0)], 0); // 0 as initial input to amp A

        /* run until amp E has halted */
        int runs = 0;
        while(1) {
            /* do one run of amps A to E */
            for (int i = 0; i < NUM_THRUSTERS; i++) {
                /* run the interpreter */
                pcs[i] = run(mems[i], pcs[i], buffers[input_buf(i)], buffers[output_buf(i)]);
                if (pcs[i] == -1) {
                    printf("Error while executing program. Exiting..\n");
                    ret_code = 1;
                    goto cleanup;
                }
            }
            if (pcs[NUM_THRUSTERS - 1] == 0) { // If amp E halted
                thrust = list_remove(buffers[output_buf(NUM_THRUSTERS - 1)], 0);
                /* check if all instances have stopped. */
                for (int i = 0; i < NUM_THRUSTERS; i++) {
                    assert(pcs[i] == 0);
                }
                /* check if all buffers are empty. */
                for (int i = 0; i < NUM_THRUSTERS; i++) {
                    assert(buffers[i]->size == 0);
                }
                printf("end of %d runs\n", runs);
                break;
            }
            runs++;
        }
        assert(pi % NUM_THRUSTERS == 0);
        if (thrust > max_thrust) {
            max_thrust = thrust;
            /* also record phase sequence that gave the maximum thrust */
            for (int i = 0; i < NUM_THRUSTERS; i++) {
                max_seq[i] = perms[pi - NUM_THRUSTERS + i] - '0';
            }
        }
        cleanup_mems(mems, NUM_THRUSTERS);
    }

    printf("%d\n", max_thrust);
    for (int i = 0; i < NUM_THRUSTERS; i++) {
        printf("%d ", max_seq[i]);
    }
    putchar('\n');

    /* cleanup */
cleanup:
    cleanup_buffers(buffers, NUM_THRUSTERS);
    free(mem_orig);
    free(perms);
    return ret_code;
}
