#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "list.h"

#define MEM_SIZE 2048
#define BUF_SIZE 1024
#define MAX_PARAM 3

#define NUM_THRUSTERS 5

/* input to the program */
#define INPUT 5

enum instr_t {ADD = 1, MULT = 2, READ = 3, WRITE = 4,
              JMPT = 5, JMPF = 6, SETLT = 7, SETE = 8, HALT = 99};

int *load(int *pos) {
    char buf[BUF_SIZE];
    int *m = calloc(MEM_SIZE, sizeof(int));
    char *end;

    *pos = 0;
    while (fgets(buf, BUF_SIZE, stdin)) {
        char *tok = strtok(buf, ",");
        m[(*pos)++] = strtol(tok, &end, 10);
        assert(*end == '\0' || *end == '\n');
        assert(*pos < MEM_SIZE);

        while ((tok = strtok(NULL, ","))) {
            m[(*pos)++] = strtol(tok, &end, 10);
            assert(*end == '\0' || *end == '\n');
            assert(*pos < MEM_SIZE);
        }
    }
    return m;
}

void dump_memory(int *mem, int size) {
    for (int i = 0; i < size; i++) {
        printf("%d,", mem[i]);
    }
    putchar('\n');
}

void get_params(int pc, int *mem, int *params, int n) {
    int param_base[MAX_PARAM] = {100, 1000, 10000};
    for (int i = 0; i < n; i++) {
        if ((mem[pc] / param_base[i]) % 10 == 1) {
            assert(i != 2); // cannot write to dest parameter as immediate
            params[i] = mem[pc + i + 1];
        } else {
            params[i] = mem[mem[pc + i + 1]];
        }
    }
}


int run(int *mem, struct list *input, struct list *output) {
    int pc = 0;
    int params[MAX_PARAM];

    while ((mem[pc] % 100) != HALT) {
        switch(mem[pc] % 100) {
            case ADD:
                get_params(pc, mem, params, 3);
                mem[mem[pc + 3]] = params[0] + params[1];
                pc += 4;
                break;
            case MULT:
                get_params(pc, mem, params, 3);
                mem[mem[pc + 3]] = params[0] * params[1];
                pc += 4;
                break;
            case READ:
                mem[mem[pc + 1]] = list_remove(input, 0); // read from front of list
                pc += 2;
                break;
            case WRITE:
                get_params(pc, mem, params, 1);
                // printf("%d\n", params[0]);
                list_push(output, params[0]);
                pc += 2;
                break;
            case JMPT:
                get_params(pc, mem, params, 2);
                if (params[0] != 0) {
                    pc = params[1];
                } else {
                    pc += 3;
                }
                break;
            case JMPF:
                get_params(pc, mem, params, 2);
                if (params[0] == 0) {
                    pc = params[1];
                } else {
                    pc += 3;
                }
                break;
            case SETLT:
                get_params(pc, mem, params, 3);
                if (params[0] < params[1]) {
                    mem[mem[pc + 3]] = 1; // TODO always position?
                } else {
                    mem[mem[pc + 3]] = 0;
                }
                pc += 4;
                break;
            case SETE:
                get_params(pc, mem, params, 3);
                if (params[0] == params[1]) {
                    mem[mem[pc + 3]] = 1; // TODO always position?
                } else {
                    mem[mem[pc + 3]] = 0;
                }
                pc += 4;
                break;
            default:
                printf("Unknown opcode %d at pc %d\n", mem[pc], pc);
                return 1;
        }
    }
    return 0;
}

void permutate(struct list *prefix, struct list *l, char *out_buf) {
    char buf[10];
    if (l->size == 1) {
        for (size_t i = 0; i < prefix->size; i++) {
            sprintf(buf, "%d", prefix->data[i]);
            strcat(out_buf, buf);
        }
        sprintf(buf, "%d", l->data[0]);
        strcat(out_buf, buf);
        return;
    }
    for (size_t i = 0; i < l->size; i++) {
        struct list *l2 = list_clone(l);
        list_push(prefix, l2->data[i]);
        list_remove(l2, i);
        permutate(prefix, l2, out_buf);
        list_remove(prefix, prefix->size - 1);
        list_cleanup(l2);
    }
}

char *gen_permutations(int num) {
    struct list *l = list_init();
    struct list *prefix = list_init();
    char *buf = calloc(4096, sizeof(char));
    for (int i = 0; i < num; i++) {
        list_push(l, i);
    }
    permutate(prefix, l, buf);
    list_cleanup(l);
    list_cleanup(prefix);
    return buf;
}

void print_perms(char *perms) {
    size_t index = 0;
    while (perms[index]) {
        printf("%c ", perms[index]);
        index++;
        if (index % 5 == 0) {
            putchar('\n');

        }
    }
}

int main(void) {
    int size;
    int ret_code = 0;
    char *perms = gen_permutations(NUM_THRUSTERS);
    // print_perms(perms);

    int *mem_orig = load(&size); // load input program from stdin
    int *mem = malloc(sizeof(int) * MEM_SIZE); // working memory

    size_t pi = 0;
    int max_thrust = 0;
    int thrust;
    struct list *input = list_init();
    struct list *output = list_init();
    while (perms[pi]) {
        list_push(output, 0); // input to amp A
        for (int i = 0; i < NUM_THRUSTERS; i++) {
            list_push(input, perms[pi++] - '0'); // phase setting
            list_push(input, list_pop(output)); // previous thrust
            memcpy(mem, mem_orig, MEM_SIZE * sizeof(int)); // setup memory
            /* run the interpreter */
            if (run(mem, input, output) != 0) {
                printf("Error while executing program. Exiting..\n");
                ret_code = 1;
                goto cleanup;
            }
        }
        assert(pi % NUM_THRUSTERS == 0);
        thrust = list_pop(output);
        if (thrust > max_thrust) {
            max_thrust = thrust;
        }
    }

    printf("%d\n", max_thrust);

    /* cleanup */
cleanup:
    free(mem);
    free(mem_orig);
    free(perms);
    list_cleanup(input);
    list_cleanup(output);
    return ret_code;
}
