#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "intcode.h"

long *intcode_load_mem(size_t *pos) {
    char *buf = NULL; // let getline allocate it.
    size_t size = 0;
    long *m = calloc(MEM_SIZE, sizeof(long));
    char *end;

    *pos = 0;
    while (getline(&buf, &size, stdin) != -1) {
        char *tok = strtok(buf, ",");
        m[(*pos)++] = strtol(tok, &end, 10);
        assert(*end == '\0' || *end == '\n');
        assert(*pos < MEM_SIZE);

        while ((tok = strtok(NULL, ","))) {
            m[(*pos)++] = strtol(tok, &end, 10);
            assert(*end == '\0' || *end == '\n');
            assert(*pos < MEM_SIZE);
        }
    }
    free(buf);
    return m;
}

struct intcode *intcode_init(long *m, size_t s, struct list *input,
        struct list *output) {
    struct intcode *inst = malloc(sizeof(struct intcode));
    if (!inst) {
        return NULL;
    }
    inst->input = inst->output = NULL;

    /* setup memory */
    inst->mem = NULL;
    if (!m) {
        inst->mem = intcode_load_mem(&inst->size);
    } else {
        inst->mem = m;
        inst->size = s;
    }
    if (!inst->mem) {
        intcode_cleanup(inst);
        return NULL;
    }

    /* setup buffers */
    if (!input) {
        input = list_init();
    }
    if (!output) {
        output = list_init();
    }
    if (!inst || !input) {
        intcode_cleanup(inst);
        return NULL;
    }

    inst->input = input;
    inst->output = output;
    inst->pc = 0;
    inst->base = 0;
    return inst;
}

void intcode_dump(struct intcode *inst) {
    for (size_t i = 0; i < inst->size; i++) {
        if (i != 0) {
            putchar(',');
        }
        printf("%ld", inst->mem[i]);
    }
    putchar('\n');
}

/* memory accessor functions that increase memory when needed */
static void increase_memory(struct intcode *inst, size_t new_size) {
    // could also have realloced and zero'd
    assert(new_size >= inst->size);
    long *tmp = calloc(new_size, sizeof(long));
    if (!tmp) {
        fprintf(stderr, "calloc failed in mget\n");
        exit(1);
    }
    memcpy(tmp, inst->mem, inst->size * sizeof(long));
    free(inst->mem);
    inst->mem = tmp;
    inst->size = new_size;
    // printf("new size: %lu\n", new_size);
}

static long mget(struct intcode *inst, size_t i) {
    if (i >= inst->size) {
        increase_memory(inst, i * 2);
    }
    return inst->mem[i];
}

static void mset(struct intcode *inst, size_t i, long val) {
    if (i >= inst->size) {
        increase_memory(inst, i * 2);
    }
    inst->mem[i] = val;
}

static long get_rel(struct intcode *inst, size_t offset) {
    return mget(inst, inst->pc + offset);
}

/* not needed?
static void set_rel(struct intcode *inst, size_t offset, long val) {
   mset(inst, inst->pc + offset, val);
}

static void set_reli(struct intcode *inst, size_t offset, long val) {
   mset(inst, get_rel(inst, offset), val);
}
*/

static void get_params(struct intcode *inst, long *params, int n) {
    long param_base[MAX_PARAM] = {100, 1000, 10000};
    for (int i = 0; i < n; i++) {
        switch((get_rel(inst, 0) / param_base[i]) % 10) {
            case 0:
                // params[i] = mem[mem[pc + i + 1]];
                params[i] = mget(inst, get_rel(inst, i + 1));
                break;
            case 1:
                assert(i != 2); // cannot write to dest parameter as imm
                params[i] = get_rel(inst, i + 1);
                break;
            case 2:
                params[i] = mget(inst, get_rel(inst, i + 1) + inst->base);
                break;
            default:
                fprintf(stderr, "Unknown parameter mode\n");
                exit(1);
        }
    }
}

long get_lparam(struct intcode *inst, int i) {
     long param_base[MAX_PARAM] = {100, 1000, 10000};
     switch((get_rel(inst, 0) / param_base[i]) % 10) {
         case 0:
             return get_rel(inst, i + 1);
         case 1:
             assert(0); // no imm l-value
         case 2:
             return get_rel(inst, i + 1) + inst->base;
         default:
             fprintf(stderr, "Unknown parameter mode\n");
             exit(1);
     }
 }

/* run instance until it halts, needs input or crashes.
 * return 1 if it is blocked on read.
 * return 0 if it is halted.
 * return -1 if an error condition is encountered. */
int intcode_run(struct intcode *inst) {
    long params[MAX_PARAM];

    while ((get_rel(inst, 0) % 100) != HALT) {
        switch(get_rel(inst, 0) % 100) {
            case ADD:
                get_params(inst, params, 3);
                mset(inst, get_lparam(inst, 2), params[0] + params[1]);
                inst->pc += 4;
                break;
            case MULT:
                get_params(inst, params, 3);
                mset(inst, get_lparam(inst, 2), params[0] * params[1]);
                inst->pc += 4;
                break;
            case READ:
                if (inst->input->size == 0) {
                    return INST_BLOCKED; // preempt this instance if we have no data
                }
                // read from front of list
                mset(inst, get_lparam(inst, 0), list_remove(inst->input, 0));
                inst->pc += 2;
                break;
            case WRITE:
                get_params(inst, params, 1);
                list_push(inst->output, params[0]);
                inst->pc += 2;
                break;
            case JMPT:
                get_params(inst, params, 2);
                if (params[0] != 0) {
                    inst->pc = params[1];
                } else {
                    inst->pc += 3;
                }
                break;
            case JMPF:
                get_params(inst, params, 2);
                if (params[0] == 0) {
                    inst->pc = params[1];
                } else {
                    inst->pc += 3;
                }
                break;
            case SETLT:
                get_params(inst, params, 3);
                if (params[0] < params[1]) {
                    mset(inst, get_lparam(inst, 2), 1);
                } else {
                    mset(inst, get_lparam(inst, 2), 0);
                }
                inst->pc += 4;
                break;
            case SETE:
                get_params(inst, params, 3);
                if (params[0] == params[1]) {
                    mset(inst, get_lparam(inst, 2), 1);
                } else {
                    mset(inst, get_lparam(inst, 2), 0);
                }
                inst->pc += 4;
                break;
            case RBASE:
                get_params(inst, params, 1);
                inst->base += params[0];
                inst->pc += 2;
                break;
            default:
                printf("Unknown opcode %ld at pc %zu\n", get_rel(inst, 0), inst->pc);
                return INST_ERROR;
        }
    }
    return INST_HALTED; // halted
}

void intcode_cleanup(struct intcode *inst) {
    free(inst->mem);
    if (inst->input) {
        list_cleanup(inst->input);
    }
    if (inst->input) {
        list_cleanup(inst->output);
    }
    free(inst);
}

