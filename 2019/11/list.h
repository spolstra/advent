#ifndef LIST_H
#define LIST_H

/* allow acces to data structure */
struct list {
    size_t size;
    size_t capacity;
    long *data;
};

struct list *list_init(void);
void list_cleanup(struct list *l);
int list_push(struct list *l, long e);
long list_pop(struct list *l);
void list_print(struct list *l);
long list_remove(struct list *l, size_t index);
struct list *list_clone(struct list *l);

#endif // LIST_H
