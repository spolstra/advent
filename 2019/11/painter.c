#include <stdio.h>
#include <assert.h>

#include "intcode.h"

#define HSIZE 600
#define INITPOS 300

enum {BLACK = 0, WHITE = 1, UNPAINTED = 2};
enum {UP = 0, LEFT = 1, DOWN = 2, RIGHT = 3};
enum {TURN_LEFT = 0, TURN_RIGHT = 1};

char hull[HSIZE][HSIZE];

size_t count_painted(void) {
    size_t count = 0;
    for (size_t x = 0; x < HSIZE;  x++) {
        for (size_t y = 0; y < HSIZE;  y++) {
            if (hull[x][y] != UNPAINTED)
                count++;
        }
    }
    return count;
}

int main(void) {
    int retcode = 0;

    for (size_t x = 0; x < HSIZE;  x++) {
        for (size_t y = 0; y < HSIZE;  y++) {
            hull[x][y] = UNPAINTED;
        }
    }

    size_t size = 0;
    struct intcode *inst = intcode_init(NULL, size, NULL, NULL);
    if (!inst) {
        printf("error creating intcode instance\n");
        return 1;
    }

    int x = INITPOS;
    int y = INITPOS;
    int direction = UP;
    while (1) {
        assert(x >= 0 && x < HSIZE && y >= 0 && y < HSIZE);
        // give color of current x, y as input.
        list_push(inst->input, hull[x][y] == WHITE ? WHITE : BLACK);
        int r = intcode_run(inst);
        if (r == INST_HALTED) {
            break;
        }
        if (r == INST_ERROR) {
            printf("intcode instance encountered an error\n");
            retcode = 1;
            goto cleanup;
        }

        /* Now paint current x, y position. */
        long new_color = list_remove(inst->output, 0);  // first output
        long turn = list_remove(inst->output, 0); // second output
        hull[x][y] = (char) new_color;
        /* Do turn */
        switch(turn) {
            case TURN_LEFT:
                direction = (direction + 1) % 4;
                break;
            case TURN_RIGHT:
                direction = direction - 1;
                if (direction < 0) {
                    direction = 3;
                }
                break;
        }
        /* do step */
        switch(direction) {
            case UP:
                y--;
                break;
            case DOWN:
                y++;
                break;
            case LEFT:
                x--;
                break;
            case RIGHT:
                x++;
                break;
        }
    }

    printf("%lu\n", count_painted());
cleanup:
    intcode_cleanup(inst);
    return retcode;
}
