#ifndef INTCODE_H
#define INTCODE_H
// because #pragma once is not standard C

#include "list.h"

#define MAX_PARAM 3

#define MEM_SIZE 2048

struct intcode {
    long *mem;
    size_t size;
    size_t pc;
    size_t base;
    struct list *input;
    struct list *output;
};

enum instr_t {ADD = 1, MULT = 2, READ = 3, WRITE = 4,
              JMPT = 5, JMPF = 6, SETLT = 7, SETE = 8, RBASE = 9,
              HALT = 99};

enum {INST_HALTED = 0, INST_BLOCKED = 1, INST_ERROR = -1};

long *intcode_load_mem(size_t *pos);
int intcode_run(struct intcode *inst);
struct intcode *intcode_init(long *m, size_t s, struct list *input,
        struct list *output);
void intcode_dump(struct intcode *inst);
void intcode_cleanup(struct intcode *inst);

#endif // INTCODE_H
