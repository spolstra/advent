#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <limits.h>

#define BUF_SIZE 2048
#define PATH_LENGTH 1024
#define N_POS 1<<20
#define END     -1

/* Read a path description from stdin and return it. */
int *read_path(void) {
    char buf[BUF_SIZE];
    char *tok;
    int *path = malloc(sizeof(int) * PATH_LENGTH);

    /* path: [dir_0, len_0, dir_1, len_1..] eg. ['U', 10, 'L', 2,..] */
    fgets(buf, BUF_SIZE, stdin); // read only 1 line.
    assert(strlen(buf) < BUF_SIZE - 1);

    int cnt = 0;
    tok = strtok(buf, ",");
    do {
        path[cnt] = *tok; // dir character
        path[cnt + 1] = strtol(tok + 1, NULL, 10);
        cnt += 2;
    } while ((tok = strtok(NULL, ",")));
    path[cnt] = END;

    return path;
}

/* Convert a path description to a list of (x,y) points */
int *to_pos(int *p, int *pos_size) {
    int *pos = malloc(sizeof(int) * N_POS);

    int cnt = 0;        // path counter
    int pos_cnt = 0;    // position counter
    int x = 0, y = 0;   // starting point is 0, 0
    // don't mark origin
    while (p[cnt] != END) {
        // add all positions on path.
        for (int i = 1; i <= p[cnt + 1]; i++) {
            switch(p[cnt]) {
                case 'U': y += 1; break;
                case 'D': y -= 1; break;
                case 'L': x -= 1; break;
                case 'R': x += 1; break;
            }
            pos[pos_cnt++] = x;
            pos[pos_cnt++] = y;
            assert(pos_cnt < N_POS);
        }
        cnt += 2;
    }
    *pos_size = pos_cnt;
    return pos;
}

/* Find the intersection of the wire points of pos1 and pos2.
 * Return the manhattan distance from the intersection to origin. */
int find_min_dist(int *pos1, int pos1_size, int *pos2, int pos2_size) {
    int x1 = 0;
    int y1 = 1;
    int min_dist = INT_MAX;
    while (y1 < pos1_size) {  // For every point in pos1..
        int x2 = 0;
        int y2 = 1;
        while (y2 < pos2_size) {  // ..compare it with every point in pos2
            if (pos1[x1] == pos2[x2] && pos1[y1] == pos2[y2]) {  // if they intersect
                if (abs(pos1[x1]) + abs(pos1[y1]) < min_dist) {  // and they are closer
                    min_dist = abs(pos1[x1]) + abs(pos1[y1]);  // record the new min distance
                }
            }
            x2 += 2;
            y2 += 2;
        }
        x1 += 2;
        y1 += 2;
    }
    return min_dist;
}

/* Print the path description p. */
void print_path(int *p) {
    int cnt = 0;

    while (p[cnt] != END) {
        printf("%c%d,", p[cnt], p[cnt + 1]);
        cnt += 2;
    }
    putchar('\n');
}

/* Print the position list pos. */
void print_pos(int *pos, int pos_size) {
    int x = 0;
    int y = 1;

    while (y < pos_size) {
        printf("(%d,%d),", pos[x], pos[y]);
        x += 2;
        y += 2;
    }
    putchar('\n');
}

int main(void) {
    /* read paths from stdin */
    int *p1 = read_path();
    int *p2 = read_path();

    /* convert to positions */
    int pos1_size, pos2_size;
    int *pos1 = to_pos(p1, &pos1_size);
    int *pos2 = to_pos(p2, &pos2_size);

    /* find minimal intersect */
    printf("%d\n", find_min_dist(pos1, pos1_size, pos2, pos2_size));

    /* cleanup */
    free(p1);
    free(p2);
    free(pos1);
    free(pos2);
    return 0;
}
