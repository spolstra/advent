#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "intcode.h"
#include "list.h"

enum {EMPTY = 0, WALL = 1, BLOCK = 2, PADDLE = 3, BALL = 4};
enum {D_EMPTY = ' ', D_WALL = '|', D_BLOCK = '#', D_PADDLE = '=', D_BALL = 'o'};

#define DISP_SIZE 35 

// Use globals for the ball position.
long ball_x = 0;
long ball_y = 0;
long paddle_x = 0;
long paddle_y = 0;
long score = 0;

char display[DISP_SIZE][DISP_SIZE];

/* show display. return the number of blocks printed. */
void show_display(char d[DISP_SIZE][DISP_SIZE]) {
    for (size_t y = 0; y < DISP_SIZE; y++) {
        for (size_t x = 0; x < DISP_SIZE; x++) {
            switch (d[x][y]) {
                case EMPTY:
                    putchar(D_EMPTY);
                    break;
                case WALL:
                    putchar(D_WALL);
                    break;
                case BLOCK:
                    putchar(D_BLOCK);
                    break;
                case PADDLE:
                    putchar(D_PADDLE);
                    break;
                case BALL:
                    putchar(D_BALL);
                    break;
            }
        }
        putchar('\n');
    }
}

/* returns score or 0 if no score was given in the inst output. */
int draw_tile(char d[DISP_SIZE][DISP_SIZE], struct list *l) {
    if (l->size >= 3) {
        long x = list_remove(l, 0);
        long y = list_remove(l, 0);
        long t = list_remove(l, 0);
        if (x == -1 && y == 0) {
            return (int) t; // return score
        }
        if (t == BALL) {
            // record ball position!
            ball_x = x;
            ball_y = y;
        }
        if (t == PADDLE) {
            paddle_x = x;
            paddle_y = y;
        }
        assert(x >= 0 && x < DISP_SIZE);
        assert(y >= 0 && y < DISP_SIZE);
        assert(t >= 0 && t <= BALL);
        d[x][y] = (char) t;
    }
    return 0; // nothing to display
}

int draw_tiles(char d[DISP_SIZE][DISP_SIZE], struct list *l) {
    int r;
    while (l->size >= 3) {
        show_display(display);
        r = draw_tile(d, l);
        if (r > 0) {
            score = r;
        } else if (r == -2) {
            return -1;
        }
        printf("score %lu, x: %lu y: %lu p(%lu,%lu)\n", score,\
                ball_x, ball_y, paddle_x, paddle_y);
    }
    return 0;
}
// 216 blocks.


int main(void) {
    int res = 0;
    struct intcode *inst = intcode_init(NULL, 0, NULL, NULL);
    if (!inst) {
        return 1;
    }
    inst->mem[0] = 2; // give quaters.

    memset(display, EMPTY, sizeof(char) * DISP_SIZE * DISP_SIZE);

    // joystick input.
    list_push(inst->input, 1);
    /*
    for (size_t i = 0; i < 1000; i++) {
        list_push(inst->input, 0);
    }
    */
    
    int r, r_draw;
    while (1) {
        r = intcode_run(inst);
        r_draw = draw_tiles(display, inst->output);
        if (r == INST_HALTED) {
            break;
        }
        if (r == INST_BLOCKED) {
            if (ball_x < paddle_x) {
                list_push(inst->input, -1);
            } else if (ball_x > paddle_x) {
                list_push(inst->input, 1);
            } else {
                list_push(inst->input, 0);
            }
        }
    }

    if (r != 0) {
        printf("Error in draw_tile\n");
        res = 1;
        goto cleanup;
    }

cleanup:
    intcode_cleanup(inst);
    return res;
}
