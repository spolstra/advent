#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "intcode.h"
#include "list.h"

enum {EMPTY = 0, WALL = 1, BLOCK = 2, PADDLE = 3, BALL = 4};
enum {D_EMPTY = ' ', D_WALL = '|', D_BLOCK = '#', D_PADDLE = '=', D_BALL = 'o'};

#define DISP_SIZE 80

char display[DISP_SIZE][DISP_SIZE];

/* show display. return the number of blocks printed. */
size_t show_display(char d[DISP_SIZE][DISP_SIZE]) {
    size_t n_blocks = 0;
    for (size_t x = 0; x < DISP_SIZE; x++) {
        for (size_t y = 0; y < DISP_SIZE; y++) {
            switch (d[x][y]) {
                case EMPTY:
                    putchar(D_EMPTY);
                    break;
                case WALL:
                    putchar(D_WALL);
                    break;
                case BLOCK:
                    putchar(D_BLOCK);
                    n_blocks++;
                    break;
                case PADDLE:
                    putchar(D_PADDLE);
                    break;
                case BALL:
                    putchar(D_BALL);
                    break;
            }
        }
        putchar('\n');
    }
    return n_blocks;
}

int draw_tiles(char d[DISP_SIZE][DISP_SIZE], struct list *l) {
    int res = 1;
    while (l->size >= 3) {
        long x = list_remove(l, 0);
        long y = list_remove(l, 0);
        long t = list_remove(l, 0);
        assert(x >= 0 && x < DISP_SIZE);
        assert(y >= 0 && y < DISP_SIZE);
        assert(t >= 0 && t <= BALL);
        d[x][y] = (char) t;
        res = 0; // ok we drew at least one tile
    }
    return res;
}

// 216 blocks.

int main(void) {
    int res = 0;
    struct intcode *inst = intcode_init(NULL, 0, NULL, NULL);
    if (!inst) {
        return 1;
    }
    memset(display, EMPTY, sizeof(char) * DISP_SIZE * DISP_SIZE);

    int r;
    while (1) {
        r = intcode_run(inst);
        if (r == INST_HALTED) {
            break;
        }
    }

    r = draw_tiles(display, inst->output);
    if (r != 0) {
        printf("Error in draw_tile\n");
        res = 1;
        goto cleanup;
    }

    size_t n_blocks = show_display(display);
    printf("\n%lu\n", n_blocks);

cleanup:
    intcode_cleanup(inst);
    return res;
}
