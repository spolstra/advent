#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "list.h"

#define DEFAULT_SIZE  10

struct list *list_init(void) {
    struct list *l = malloc(sizeof(struct list));
    if (!l) {
        return NULL;
    }
    l->data = malloc(DEFAULT_SIZE * sizeof(long));
    if (!l->data) {
        free(l);
        return NULL;
    }
    l->size = 0;
    l->capacity = DEFAULT_SIZE;
    return l;
}

struct list *list_clone(struct list *l) {
    struct list *l2 = list_init();
    long *tmp_data = realloc(l2->data, l->capacity * sizeof(long));
    if (!tmp_data) {
        free(l2);
        return NULL;
    }
    l2->data = tmp_data;
    l2->size = l->size;
    l2->capacity = l->capacity;
    memcpy(l2->data, l->data, l->size * sizeof(long));
    return l2;
}

void list_cleanup(struct list *l) {
    free(l->data);
    free(l);
}

int list_push(struct list *l, long e) {
    if (l->size == l->capacity) {
        long *tmp_data = realloc(l->data, l->capacity * 2 * sizeof(long));
        if (!tmp_data) {
            return 1;
        }
        l->data = tmp_data;
        l->capacity *= 2;
    }
    l->data[l->size] = e;
    l->size += 1;
    return 0;
}

long list_pop(struct list *l) {
    assert(l->size >= 1);
    l->size--;
    return l->data[l->size];
}

void list_print(struct list *l) {
    for (size_t i = 0; i < l->size; i++) {
        printf("%ld ", l->data[i]);
    }
    putchar('\n');
    return;
}


long list_remove(struct list *l, size_t index) {
    assert(index < l->size);
    if (index == l->size - 1) { // removing last element is easy
        l->size--;
        return l->data[index];
    }

    /* remove element and fill by moving trailing elements to the left */
    long res = l->data[index];
    for (size_t i = index; i < l->size - 1; i++) {
        l->data[i] = l->data[i + 1];
    }
    l->size--;
    return res;
}
