#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 255

int read_range(int *start, int *stop) {
    char buf[BUFSIZE];

    fgets(buf, BUFSIZE, stdin);
    *start = strtol(strtok(buf, "-"), NULL, 10);
    *stop = strtol(strtok(NULL, "-"), NULL, 10);
    return 0;
}

/* returns 1 if buf has doubles, 0 otherwise */
int has_doubles(char *buf) {
    for (size_t i = 0; i < strlen(buf) - 1; i++) {
        if (buf[i] == buf[i + 1]) {
            return 1;
        }
    }
    return 0;
}

/* returns 1 if buf is monotonic */
int monotonic_inc(char *buf) {
    for (size_t i = 0; i < strlen(buf) - 1; i++) {
        if (buf[i] > buf[i + 1]) {
            return 0; // not monotonic.
        }
    }
    return 1;
}

int is_pw_ok(int pw) {
    char buf[BUFSIZE];
    sprintf(buf, "%d", pw);
    return has_doubles(buf) && monotonic_inc(buf) && strlen(buf) == 6;
}

int main(void) {
    int start, stop;
    int correct_cnt = 0;

    read_range(&start, &stop);
    printf("%d %d\n", start, stop);

    for (int pw = start; pw <= stop; pw++) {
        if (is_pw_ok(pw)) {
            correct_cnt++;
        }
    }

    printf("%d\n", correct_cnt);
    return 0;
}
