#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <ctype.h>
#include <stdbool.h>
#include <limits.h>
#include <string.h>

enum {WALL = '#', SPACE = '.', CUR_LOC = '@'};

struct xy {
	int x;
	int y;
	int dist;
};

#define XY_SIZE 100
struct xys {
	struct xy data[XY_SIZE];
	size_t size;
};

#define MAP_SIZE 100
typedef char map_t[MAP_SIZE][MAP_SIZE];

/* xys support functions. */
void xys_init(struct xys *xys) {
	xys->size = 0;
}

void xys_push(struct xys *xys, struct xy xy) {
	xys->data[xys->size] = xy;
	xys->size++;
	assert(xys->size < XY_SIZE);
}

struct xy xys_pop(struct xys *xys) {
	assert(xys->size > 0);
	return xys->data[--xys->size]; // yes, precendence is ok
}

void xys_print(map_t m, struct xys *xys) {
	for (size_t i = 0; i < xys->size; i++) {
		struct xy *p = &xys->data[i];
		printf("x:%d, y:%d dist:%d '%c'\n", p->x, p->y, p->dist,
				m[p->x][p->y]);
	}
}

void xys_copy(struct xys *dest, struct xys *src) {
	for (size_t i = 0; i < src->size; i++) {
		dest->data[i] = src->data[i];
	}
	dest->size = src->size;
}

int xys_member(struct xys *set, struct xy e) {
	for (size_t i = 0; i < set->size; i++) {
		if (set->data[i].x == e.x && set->data[i].y == e.y) {
			return 1;
		}
	}
	return 0;
}

void readmap(map_t m, int *maxx, int *maxy) {
	char c = 0;
	*maxx = *maxy = 0;
	int y;
	for (y = 0; c != EOF; y++) {
		int x;
		for (x = 0; (c = getchar()) != '\n' && c != EOF; x++) {
			m[x][y] = c;
		}
		*maxx = x > *maxx ? x : *maxx;
	}
	*maxy = y - 1;
}

void printmap(map_t m, int maxx, int maxy) {
	printf("x: %d, y: %d\n", maxx, maxy);
	for (int y = 0; y < maxy; y++) {
		for (int x = 0; x < maxx; x++) {
			putchar(m[x][y]);
		}
		putchar('\n');
	}
}

/* return 1 if p is a valid neighbour, 0 otherwise */
/* valid neigbours are dots (.) and keys (a-z). We stop at doors (A-Z) */
int is_neighbour(map_t m, struct xy p) {
	if (m[p.x][p.y] == '.' || islower(m[p.x][p.y])) {
		return 1;
	}
	return 0;
}

/* add neighbours of point p to the neighbours list. */
void find_neighbours(map_t m, struct xy p, struct xys *neighbours,
					 struct xys *prevs) {
	struct xy offset[] = {{.x = 0, .y = -1}, {.x = 0, .y= 1},
						   {.x = -1, .y = 0}, {.x = 1, .y= 0}};
	for (size_t i = 0; i < sizeof(offset) / sizeof(offset[0]); i++) {
		struct xy n = {.x = p.x + offset[i].x,
			           .y = p.y + offset[i].y,
		               .dist = p.dist + 1};
		if (n.x < 0 || n.y < 0) {
			continue;
		}
		if (is_neighbour(m, n) && !xys_member(prevs, n)) {
			xys_push(neighbours, n);
		}
	}
}

/* copy all keys from neighbours list to keys. check for duplicates */
void collect_keys(map_t m, struct xys *neighbours, struct xys *keys) {
	for (size_t i = 0; i < neighbours->size; i++) {
		struct xy p = neighbours->data[i];
		if (islower(m[p.x][p.y])) {
			/* check for duplicate keys */
			bool add = true;
			struct xy *replace = NULL;
			for (size_t i = 0; i < keys->size; i++) {
				if (keys->data[i].x == p.x && keys->data[i].y == p.y) {
					if (keys->data[i].dist <= p.dist) {
						add = false;
					} else { // store to overwrite distance
						replace = &keys->data[i];
					}
					break;
				}
			}
			if (replace) {
				replace->dist = p.dist;
			} else if (add) {
				xys_push(keys, p);
			}
		}
	}
}

/* return list of keys reachable from position pos. */
struct xys *find_keys(map_t m, struct xy pos) {
	struct xys points;
	xys_init(&points);
	struct xys neighbours;
	xys_init(&neighbours);
	struct xys *keys = calloc(1, sizeof(struct xys));
	xys_init(keys);
	struct xys cur_front;
	xys_init(&cur_front);
	struct xys prev_front;
	xys_init(&prev_front);

	pos.dist = 0;
	xys_push(&points, pos); // start searching at pos
	while (points.size > 0) {
		xys_copy(&cur_front, &points);

		while (points.size > 0) { // get all neighbours of points
			find_neighbours(m, xys_pop(&points), &neighbours, &prev_front);
		}
		collect_keys(m, &neighbours, keys);

		while (neighbours.size > 0) { // move all neighbours to points
			xys_push(&points, xys_pop(&neighbours));
		}
		xys_copy(&prev_front, &cur_front);
	}
	return keys;
}

/* find CUR_LOC on map. return it's positon. */
struct xy find_cur_loc(map_t m, int maxx, int maxy) {
	for (int y = 0; y < maxy; y++) {
		for (int x = 0; x < maxx; x++) {
			if (m[x][y] == CUR_LOC) {
				return (struct xy) {.x = x, .y = y};
			}
		}
	}
	assert(0); // cannot get here, there should be a start.
}

/* fill key_locs and door_locs with the locations of all the keys and
 * doors. */
void pre_calc_locs(map_t m, struct xys *key_locs, struct xys *door_locs) {
    for (int y = 0; y < MAP_SIZE; y++) {
        for (int x = 0; x < MAP_SIZE; x++) {
            if (islower(m[x][y])) {
                key_locs->data[m[x][y] - 'a'] =
                    (struct xy) {.x = x, .y = y};
                key_locs->size++;
            }
            if (isupper(m[x][y])) {
                door_locs->data[m[x][y] - 'A'] =
                    (struct xy) {.x = x, .y = y};
                door_locs->size++;
            }
        }
    }
    assert(key_locs->size <= 26);
    assert(door_locs->size <= 26);
}

/* return a pointer to the xy location of key or door. */
struct xy *get_location(char key_or_door, struct xys *locs) {
    return &locs->data[tolower(key_or_door) - 'a'];
}

/* remove key and matching door from map. */
/* location of key and door are given by key_loc and door_loc. */
void remove_key_and_door(map_t m, struct xy *key_loc,
        struct xy *door_loc) {
    m[key_loc->x][key_loc->y] = SPACE;
    m[door_loc->x][door_loc->y] = SPACE;
}

/* get_keys from pos on map in the shortest path possible.
 * return the number of steps from pos to get all the keys. */
int get_keys(map_t map, struct xy pos, struct xys *key_locs,
        struct xys *door_locs) {
	struct xys *keys = find_keys(map, pos);
	if (keys->size == 0) {
		free(keys);
		return 0; // terminate recursion
	}
	char cur_key = 0;
	int cur_minimum = INT_MAX;
	for (size_t i = 0; i < keys->size; i++) {
		// make copies, because we need fresh ones every iteration.
		struct xy new_pos = pos;
		map_t new_map;
		memcpy(new_map, map, MAP_SIZE * MAP_SIZE * sizeof(char));

		int steps_to_key = keys->data[i].dist; // get steps and move
		new_pos = keys->data[i];

        // remove key and door from the new_map
        char k = new_map[keys->data[i].x][keys->data[i].y];
		remove_key_and_door(new_map, get_location(k, key_locs),
                get_location(k, door_locs));
		new_map[pos.x][pos.y] = SPACE;
		new_map[new_pos.x][new_pos.y] = CUR_LOC;

		int total_steps = steps_to_key + get_keys(new_map, new_pos,
                key_locs, door_locs);
		if (total_steps < cur_minimum) { // keep track of best key.
			cur_minimum = total_steps;
			cur_key = map[new_pos.x][new_pos.y];
		}
	}
	free(keys); // not needed anymore
	assert(cur_key != 0);
	return cur_minimum;
}

int main(void) {
	int maxx = 0;
	int maxy = 0;
	map_t map;
    memset(map, 0, MAP_SIZE * MAP_SIZE * sizeof(char));

	readmap(map, &maxx, &maxy);
	printmap(map, maxx, maxy);

	struct xy start = find_cur_loc(map, maxx, maxy);
    struct xys doors, keys;
    xys_init(&doors);
    xys_init(&keys);

    pre_calc_locs(map, &keys, &doors);
	int pathlen = get_keys(map, start, &keys, &doors);

	printf("path length: %d\n", pathlen);

	// struct xys *keys = find_keys(map, start);
	// xys_print(map, keys);
	// free(keys);
	
	return 0;
}
