#include <stdio.h>
#include <string.h>
#include <glib.h>
#include <gmodule.h>
#include <assert.h>
#include <ctype.h>

// compile with:
// clang -Wall -Wextra `pkg-config --cflags glib-2.0`  `pkg-config --libs glib-2.0` hash_test.c

void print_path(int door, int last) {
    assert(islower(last));
    putchar('[');
    for (char d = '`'; d <= last; d++) {
        if (door & (1 << (d - '`'))) {
            putchar(d);
        }
    }
    putchar(']');
}

int path_to_int(char *path) {
    int res = 0;
    for (unsigned int i = 0; i < strlen(path); i++) {
        res |= 1 << (path[i] - '`');
    }
    return res;
}

int combine_path_loc(int path, int loc) {
    return path | ((loc - '`') << 27);
}

int extract_path(int val) {
    return val & ((1 << 27) - 1);
}

int extract_loc(int val) {
    return '`' + (val >> 27);
}

int main(void) {
    int ret;
    int key = 1;
    int val = 10;


    int ip = path_to_int("`abce");
    ip = combine_path_loc(ip, '`');
    print_path(extract_path(ip), 'z');
    putchar('\n');
    printf("%c\n", extract_loc(ip));

    GHashTable *hash = g_hash_table_new(g_direct_hash, g_direct_equal);

    g_hash_table_insert(hash, GINT_TO_POINTER(key), GINT_TO_POINTER(val));

    key = 1;
    printf("lookup of %d: %d\n", key,
            GPOINTER_TO_INT(g_hash_table_lookup(hash, GINT_TO_POINTER(key))));

    key = 2;
    ret = GPOINTER_TO_INT(g_hash_table_lookup(hash, GINT_TO_POINTER(key)));
    printf("lookup of %d: %d\n", key,
            GPOINTER_TO_INT(g_hash_table_lookup(hash, GINT_TO_POINTER(key))));

    g_hash_table_destroy(hash);

    return 0;
}
