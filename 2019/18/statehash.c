#include <stdio.h>
#include <glib.h>
#include <gmodule.h>

#include "statehash.h"

guint state_hash(gconstpointer key) {
    struct state *s = (struct state*) key;

    guint r = g_int_hash(&s->keys);
    for (int i = 0; i < s->nlocs; i++) {
        r ^= g_int_hash(&s->locs[i]);
    }
    return r;
}

gboolean state_equal(gconstpointer a, gconstpointer b) {
    struct state *s1 = (struct state*) a;
    struct state *s2 = (struct state*) b;

    if (s1->keys != s2->keys) {
        return FALSE;
    }

    for (int i = 0; i < s1->nlocs; i++) {
        if (s1->locs[i] != s2->locs[i]) {
            return FALSE;
        }
    }
    return TRUE;
}

void state_destroy(gpointer data) {
    free(data);
}

int state_lookup(GHashTable *h, struct state *s) {
    int r = GPOINTER_TO_INT(g_hash_table_lookup(h, s));
    // printf("lookup keys: %d, loc: %c, dist: %d\n", s->keys, s->locs[0], r);
    return r;
}

struct state *state_dup(struct state *s) {
    struct state *dup = malloc(sizeof(struct state));
    if (!dup) {
        return NULL;
    }
    *dup = *s;
    return dup;
}

// return 0 if new key, TRUE if entry was replaced and -1 if dup failed.
int state_insert(GHashTable *h, struct state *s, int val) {
    struct state *dup = state_dup(s);
    if (!dup) {
        return -1;
    }
    // printf("insert: keys: %d, [%c] -> %d\n", dup->keys, dup->locs[0], val);
    return g_hash_table_insert(h, dup, GINT_TO_POINTER(val));
}

GHashTable *state_hash_init(void) {
    return g_hash_table_new_full( state_hash, state_equal, state_destroy,
            NULL);
}

void state_hash_destroy(GHashTable *h) {
    g_hash_table_destroy(h);
}

#ifdef TEST_MAIN

// compile with:
// clang -g3 -DTEST_MAIN `pkg-config --cflags glib-2.0`  `pkg-config --libs glib-2.0` -fsanitize=address statehash.c
int main(void) {
    int val = 10;

    GHashTable *hash = g_hash_table_new_full(state_hash, state_equal, state_destroy, NULL);

    struct state s = {.keys = 1, .locs = {2, 2, 2, 2}, .nlocs = 4};
    /*
    struct state *s = malloc(sizeof(struct state));
    s->keys = 1;
    for (int i = 0; i < 4; i++) {
        s->locs[i] = 2;
    }
    */
    val = 12;
    printf("insert struct with keys: %d  val: %d\n", s.keys, val);
    state_insert(hash, &s, val);

    printf("lookup of struct with keys %d: %d\n", s.keys,
            state_lookup(hash, &s));

    // insert second (different) state
    s.keys = 100;
    for (int i = 0; i < 4; i++) {
        s.locs[i] = 2;
    }
    val = 100;

    // insert this second state.
    state_insert(hash, &s, val);

    printf("lookup of second struct with keys %d: %d\n", s.keys,
            state_lookup(hash, &s));

    // modify struct
    s.locs[0] = 0;

    // lookup modified struct
    printf("lookup of modified struct: %d\n", state_lookup(hash, &s));

    // copy of first struct
    struct state *s2 = malloc(sizeof(struct state));
    s2->keys = 1;
    for (int i = 0; i < 4; i++) {
        s2->locs[i] = 2;
    }
    printf("lookup of original via copy of struct: %d\n",
            state_lookup(hash, s2));
    
    *s2 = (struct state) {.keys = 1, .locs = {2, 2, 2, 2}, .nlocs = 4};
    printf("%d\n", state_insert(hash, s2, 13));
    printf("%d\n", state_insert(hash, s2, 13));
    printf("lookup expect 13: %d\n",
            state_lookup(hash, s2));

    free(s2);

    g_hash_table_destroy(hash);
    return 0;
}
#endif // TEST_MAIN
