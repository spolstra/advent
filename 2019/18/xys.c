#include <stdlib.h>
#include <assert.h>

#include "xys.h"

#define XY_SIZE 100

struct xys {
    struct xy data[XY_SIZE];
    size_t size;
    size_t head;
    size_t tail;
};

/* xys support functions. */
struct xys *xys_init(void) {
	struct xys *xys = malloc(sizeof(struct xys));
    xys->size = 0;
    xys->head = 0;
    xys->tail = 0;
	return xys;
}

void xys_reset(struct xys *xys) {
    xys->size = 0;
    xys->head = 0;
    xys->tail = 0;
}

// tail is first empty position
void xys_enque(struct xys *xys, struct xy xy) {
    assert(xys->size < XY_SIZE);
    xys->data[xys->tail] = xy;
    xys->tail = (xys->tail + 1) % XY_SIZE;
    xys->size++;
}

// head is first filled position
struct xy xys_deque(struct xys *xys) {
    assert(xys->size > 0);
    size_t i = xys->head;
    xys->head = (xys->head + 1) % XY_SIZE;
    xys->size--;
    return xys->data[i];
}

void xys_copy(struct xys *dest, struct xys *src) {
    for (size_t i = 0; i < src->size; i++) {
        dest->data[i] = src->data[i];
    }
    dest->size = src->size;
}

int xys_member(struct xys *set, struct xy e) {
    for (size_t i = 0; i < set->size; i++) {
        if (set->data[i].x == e.x && set->data[i].y == e.y) {
            return 1;
        }
    }
    return 0;
}

size_t xys_size(struct xys *xys) {
	return xys->size;
}
