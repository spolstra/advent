#include <stdio.h>
#include <glib.h>
#include <gmodule.h>

// compile with:
// clang `pkg-config --cflags glib-2.0`  `pkg-config --libs glib-2.0` hash.c

int main(void) {
    int ret;
    int key = 1;
    int val = 10;

    GHashTable *hash = g_hash_table_new(g_direct_hash, g_direct_equal);

    g_hash_table_insert(hash, GINT_TO_POINTER(key), GINT_TO_POINTER(val));

    key = 1;
    printf("lookup of %d: %d\n", key,
            GPOINTER_TO_INT(g_hash_table_lookup(hash, GINT_TO_POINTER(key))));

    key = 2;
    ret = GPOINTER_TO_INT(g_hash_table_lookup(hash, GINT_TO_POINTER(key)));
    printf("lookup of %d: %d\n", key,
            GPOINTER_TO_INT(g_hash_table_lookup(hash, GINT_TO_POINTER(key))));

    g_hash_table_destroy(hash);

    return 0;
}
