#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <ctype.h>
#include <stdbool.h>
#include <limits.h>
#include <string.h>
#include <glib.h>
#include <gmodule.h>

#include "xys.h"
#include "statehash.h"

#define MAXPATH 30

int verbose = 0;

enum {WALL = '#', SPACE = '.', CUR_LOC = '@'};

char PATH[MAXPATH];

/* global variable that contains the highest key. */
char g_highest = 'a';

/* map data structure */
#define MAP_SIZE 1000
typedef struct map_t {
    int x;
    int y;
    char key;
    char m[MAP_SIZE][MAP_SIZE];
} map_t;

/* distance lookup table: (src, dst) -> distance */
/* index with ascii chars in range < 128 */
typedef int table_t[128][128];

/* memoization cache data structure
 * This is hashtable that stores the minimal distance to all remaining keys
 * given the keys found so far and the current location.
 * (keys collected so far, cur location) --> shortest distance for keys
 */
GHashTable *dist_cache = NULL;

/* load map from stdin into map_t, also store mapsize and highest key
 * found. Replace start locations '@' with chars '0','1',...
 * Return the number of start locations found.
 */
int readmap(map_t *m) {
    char c = 0;
    m->x = m->y = m->key = 0;
    int y;
    int start_loc_char = '0';
    for (y = 0; c != EOF; y++) {
        int x;
        for (x = 0; (c = getchar()) != '\n' && c != EOF; x++) {
            if (c == '@') {
                c = start_loc_char++;
            }
            m->m[x][y] = c;
            if (islower(c) && c > m->key) {
                m->key = c;
            }
        }
        m->x = x > m->x ? x : m->x;
    }
    m->y = y - 1;
    return start_loc_char - '0';
}

/* print map_t m to stdout. */
void printmap(map_t *m) {
    printf("x: %d, y: %d\n", m->x, m->y);
    for (int y = 0; y < m->y; y++) {
        for (int x = 0; x < m->x; x++) {
            putchar(m->m[x][y]);
        }
        putchar('\n');
    }
}

/* return 1 if p is a valid neighbour, 0 otherwise */
/* valid neigbours are dots (.) and keys (a-z) or doors (A-Z) */
int is_neighbour(map_t *m, struct xy p) {
    if (m->m[p.x][p.y] == '.' || isalnum(m->m[p.x][p.y])) {
        return 1;
    }
    return 0;
}

/* add neighbours of point p to the neighbours list. */
void find_neighbours(map_t *m, struct xy p, struct xys *neighbours) {
    struct xy offset[] = {{.x = 0, .y = -1}, {.x = 0, .y= 1},
                           {.x = -1, .y = 0}, {.x = 1, .y= 0}};
    for (size_t i = 0; i < sizeof(offset) / sizeof(offset[0]); i++) {
        struct xy n = {.x = p.x + offset[i].x,
                       .y = p.y + offset[i].y,
                       .dist = p.dist + 1,
                       .path = p.path};
        if (n.x < 0 || n.y < 0) {
            continue;
        }
        if (is_neighbour(m, n)) {
            xys_enque(neighbours, n);
        }
    }
}

/* find CUR_LOC on map. return it's position. */
struct xy find_loc(map_t *m, char c) {
    for (int y = 0; y < m->y; y++) {
        for (int x = 0; x < m->x; x++) {
            if (m->m[x][y] == c) {
                return (struct xy) {.x = x, .y = y, .dist = 0, .path = 0};
            }
        }
    }
    return (struct xy) {.x = -1, .y = -1, .dist = -1};
}

/* Return distance of src->dst, store a bitmap of the doors passed (and thus
 * keys that are needed) in doors.
 * Return -1 is dst cannot be found
 */
int distance(map_t *m, char src, char dst, int *doors) {
    // locate start pos
    struct xy start_pos = find_loc(m, src);

    // scratch map nm: because we overwrite visited positions with walls.
    map_t *nm = malloc(sizeof(map_t));
    memcpy(nm, m, sizeof(map_t));

    // front set of our map flood.
    struct xys *front = xys_init();
    struct xys *neighbours = xys_init();
    xys_enque(front, start_pos);

    while (xys_size(front) > 0) {
        // Examine point of the front.
        struct xy cur = xys_deque(front);
        if (nm->m[cur.x][cur.y] == dst) {
            free(nm);
            *doors = cur.path; // path of keys to dst
            free(front);
            free(neighbours);
            return cur.dist; // done: found dst location.
        }
        if (isupper(nm->m[cur.x][cur.y])) { // append door to doors
            // Set bit for door X to 1.
            cur.path |= (1 << (nm->m[cur.x][cur.y] - 'A'));
        }
        nm->m[cur.x][cur.y] = WALL; // So we only visit it once.

        // Add neighbours of cur to front.
        xys_reset(neighbours);
        find_neighbours(nm, cur, neighbours);
        while (xys_size(neighbours) > 0) { // move all neighbours to points
            xys_enque(front, xys_deque(neighbours));
        }
    }
    free(nm);
    free(front);
    free(neighbours);
    return -1;
}

/* initialise dists array with distances between all keys (src, dst)
 */
void init_dist_door_tables(struct state s, map_t *m, table_t dists,
        table_t doors) {
    /* init start locs to keys. */
    for (int src = '0'; src < '0' + s.nlocs; src++) {
        for (int dst = 'a'; dst <= m->key; dst++) {
            int door = 0;
            dists[src][dst] = distance(m, src, dst, &door);
            doors[src][dst] = door;
        }
    }

    /* init doors and distance between all keys */
    for (int src = 'a'; src <= m->key; src++) {
        for (int dst = 'a'; dst <= m->key; dst++) {
            int door = 0;
            dists[src][dst] = distance(m, src, dst, &door);
            doors[src][dst] = door;
        }
    }
}

/* Print distances between keys.
 * dists: 2d array with distance matrix.
 *        indexed by ascii value of the key.
 * last:  last key (alphabetically) on the map.
 */
void print_dist_table(table_t dists, char last, struct state s) {
    // print header line
    printf(" ");
    for (char i = 'a'; i <= last; i++) {
        printf("   %c", i);
    }
    putchar('\n');

    /* src is start locs */
    for (int src = '0'; src < '0' + s.nlocs; src++) {
        printf("%c ", src);
        for (int dst = 'a'; dst <= last; dst++) {
            printf("%3d ", dists[src][dst]);
        }
        putchar('\n');
    }
    /* src is keys */
    for (int src = 'a'; src <= last; src++) {
        printf("%c ", src);
        for (int dst = 'a'; dst <= last; dst++) {
            printf("%3d ", dists[src][dst]);
        }
        putchar('\n');
    }
}

/* Door is encoded as a bit set on position char with 'a' as position
 * 0. print door letters as lowercase */
void print_door(int door, int last) {
    assert(islower(last));
    putchar('[');
    for (char d = 'a'; d <= last; d++) {
        if (door & (1 << (d - 'a'))) {
            putchar(d);
        }
    }
    putchar(']');
}

/* print matrix of doors between a pair of keys.
 * doors: 2d array that contains doors between keys.
 * last:  last key/door on the map.
 */
void print_door_table(table_t doors, char last, struct state s) {
    // start from doors
    for (int src = '0'; src < '0' + s.nlocs; src++) {
        printf("%c ", src);
        for (int dst = 'a'; dst <= last; dst++) {
            printf(" %c:", dst);
            print_door(doors[src][dst], last);
        }
        putchar('\n');
    }
    // start from keys
    for (int src = 'a'; src <= last; src++) {
        printf("%c ", src);
        for (int dst = 'a'; dst <= last; dst++) {
            printf(" %c:", dst);
            print_door(doors[src][dst], last);
        }
        putchar('\n');
    }

}

int get_reachable(struct state s, int loc_nr, table_t doors,
        table_t dists); // forward decl.


int state_add_key(int cur_keys, int key) {
    if (isdigit(key)) { // start loc not a real key
        return cur_keys;
    }
    return cur_keys |= 1 << (key - 'a');
}

/* Return the shortest path to all remaining keys starting at s.locs[0].
 * s:           state: loc and keys collected
 * path_dist:   distance `path` so far
 * doors:       table with door data (doors between src,dst)
 * dists:       table with distance data
 * hash:        memoization table of calculated shortest paths.
 *
 * return: the minimum path length of visiting all keys in path.
 */
int min_dist(struct state s, int path_dist,
        table_t doors, table_t dists, GHashTable *hash) {
    // find keys that are reachable from current location
    // add store original locs.
    int reachable_keys[MAX_LOCS];
    int original_locs[MAX_LOCS];
    int sum_reacheable = 0;
    // First add/visit the keys at all locs (only one will be changed)
    for (int i = 0; i < s.nlocs; i++) {
        s.keys = state_add_key(s.keys, s.locs[i]); // visit loc, collect key
    }
    for (int i = 0; i < s.nlocs; i++) {
        original_locs[i] = s.locs[i];
        reachable_keys[i] = get_reachable(s, i, doors, dists);
        sum_reacheable += reachable_keys[i];
    }
    if (sum_reacheable == 0) { // no keys left, we are done.
        return 0; // no key so distance is 0
    }

    int cur_min = INT_MAX;

    // First check if shortest path from this location with this set of
    // found keys is already in the cache.
    // lookup(keys, current_loc) -> dist
    int rest_dist = state_lookup(hash, &s);
    if (rest_dist != 0) {
        return rest_dist;
    }

    // For all reachable keys
    for (int loc = 0; loc < s.nlocs; loc++) {
        for (int dst = 'a'; reachable_keys[loc]; reachable_keys[loc] /= 2, dst++) {
            if (reachable_keys[loc] % 2) { // if dst is reachable.
                int dist = dists[original_locs[loc]][dst];
                // recursively calc min_dist with dst as next path item.

                int org_loc = s.locs[loc]; // save loc state
                s.locs[loc] = dst; // try: next move is to dst
                rest_dist =  min_dist(s, path_dist + dist,
                        doors, dists, hash);
                s.locs[loc] = org_loc; // restore loc state!

                if (dist + rest_dist < cur_min) { // record path if shortest
                    cur_min = dist + rest_dist;
                }
            }
        }
    }

    // record minimal distance starting at original loc with orig keys.
    state_insert(hash, &s, cur_min);

    return cur_min;
}

/* Return a bitmap of all keys that are reachable from current
 * state s
 *
 * s:     current location and keys collected
 * doors: table that gives doors between a src and dst.
 *
 * Return the keys that are reachable from current
 * location with the current list of keys (state).
 * Keys already collected are excluded from the reachable set.
 */
int get_reachable(struct state s, int loc_nr, table_t doors,
        table_t dists) {
    int reachable = 0; // bitmap of reachable keys

    for (int dst = 'a'; dst <= g_highest; dst++) {
        if (s.keys & (1 << (dst - 'a')) ||
                dists[s.locs[loc_nr]][dst] == -1) {
            // already visited dst or unreachable
            continue;
        }
        int keys_req = doors[s.locs[loc_nr]][dst];
        if ((keys_req & s.keys) == keys_req) { // if path has all req keys
            reachable |= 1 << (dst - 'a'); // turn on bit on dst position
        }
    }
    return reachable;
}

struct state init_state(int nlocs) {
    struct state s = {.keys = 0, .nlocs = nlocs};
    for (int i = 0; i < nlocs; i++) {
        s.locs[i] = '0' + i;
    }
    return s;
}

int main(void) {
	// setup memoization cache
    GHashTable *hash = state_hash_init();

    map_t *map = calloc(1, sizeof(map_t));
    table_t dists = {{0}}, doors = {{0}};

    int nlocs = readmap(map);
    struct state s = init_state(nlocs);

    printmap(map);

    g_highest = map->key; // store last key in global g_last

    init_dist_door_tables(s, map, dists, doors);

    print_dist_table(dists, g_highest, s); // debug prints
    print_door_table(doors, g_highest, s);

    int min = min_dist(s, 0, doors, dists, hash);
    printf("minimal path dist: %d\n", min);

    free(map);
    state_hash_destroy(hash);

    return 0;
}
