#include <stdio.h>
#include <glib.h>
#include <gmodule.h>

// compile with:
// clang `pkg-config --cflags glib-2.0`  `pkg-config --libs glib-2.0` -fsanitize=address hash2.c

#define LOCS 4

struct state {
    int keys; // bit list of keys that are found
    int locs[LOCS];
};

guint state_hash(gconstpointer key) {
    struct state *s = (struct state*) key;

    guint r = g_int_hash(&s->keys);
    for (int i = 0; i < LOCS; i++) {
        r ^= g_int_hash(&s->locs[i]);
    }
    return r;
}

gboolean state_equal(gconstpointer a, gconstpointer b) {
    struct state *s1 = (struct state*) a;
    struct state *s2 = (struct state*) b;

    if (s1->keys != s2->keys) {
        return FALSE;
    }

    for (int i = 0; i < LOCS; i++) {
        if (s1->locs[i] != s2->locs[i]) {
            return FALSE;
        }
    }
    return TRUE;
}

void state_destroy(gpointer data) {
    free(data);
}

int main(void) {
    int val = 10;

    GHashTable *hash = g_hash_table_new_full(state_hash, state_equal, state_destroy, NULL);

    struct state *s = malloc(sizeof(struct state));
    s->keys = 1;
    for (int i = 0; i < 4; i++) {
        s->locs[i] = 2;
    }
    val = 12;

    printf("insert struct with keys: %d  val: %d\n", s->keys, val);
    g_hash_table_insert(hash, s, GINT_TO_POINTER(val));

    printf("lookup of struct with keys %d: %d\n", s->keys,
            GPOINTER_TO_INT(g_hash_table_lookup(hash, s)));

    // insert second (different) state
    s = malloc(sizeof(struct state));
    s->keys = 100;
    for (int i = 0; i < 4; i++) {
        s->locs[i] = 2;
    }
    val = 100;

    // insert this second state.
    g_hash_table_insert(hash, s, GINT_TO_POINTER(val));

    printf("lookup of second struct: %d\n",
            GPOINTER_TO_INT(g_hash_table_lookup(hash, s)));

    // modify struct
    s->locs[0] = 0;

    // lookup modified struct
    printf("lookup of modified struct: %d\n",
            GPOINTER_TO_INT(g_hash_table_lookup(hash, s)));

    // copy of first struct
    struct state *s2 = malloc(sizeof(struct state));
    s2->keys = 1;
    for (int i = 0; i < 4; i++) {
        s2->locs[i] = 2;
    }
    printf("lookup of original via copy of struct: %d\n",
            GPOINTER_TO_INT(g_hash_table_lookup(hash, s2)));
    free(s2);

    g_hash_table_destroy(hash);

    return 0;
}
