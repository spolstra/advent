#include <glib.h>

/* glib hashtable wrapper to store struct state's and their distance value. */

#define MAX_LOCS 4

#ifndef STATEHASH_H
#define STATEHASH_H

struct state {
    int keys; 	// bit map of keys that are found. 'a' is bit 0 etc..
	int nlocs; 	// number of locations
    int locs[MAX_LOCS];
};


guint state_hash(gconstpointer key);

gboolean state_equal(gconstpointer a, gconstpointer b);

void state_destroy(gpointer data);

int state_lookup(GHashTable *h, struct state *s);

int state_insert(GHashTable *h, struct state *s, int val);

GHashTable *state_hash_init(void);

void state_hash_destroy(GHashTable *h);

#endif // STATEHASH_H
