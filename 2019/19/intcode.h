#ifndef INTCODE_H
#define INTCODE_H
// because #pragma once is not standard C

#include "list.h"

#define MAX_PARAM 3

#define MEM_SIZE 16384

struct intcode {
    long *mem;              /* pointer to program code */
    size_t size;            /* number of instrs (not bytes!) */
    size_t pc;              /* current program counter */
    size_t base;            /* base pointer */
    struct list *input;     /* program input queue */
    struct list *output;    /* program output queue */
};

enum {INST_HALTED = 0, INST_BLOCKED = 1, INST_ERROR = -1};

long *intcode_load_mem(size_t *pos);
int intcode_run(struct intcode *inst);
struct intcode *intcode_init(long *m, size_t s, struct list *input,
        struct list *output);
void intcode_dump(struct intcode *inst);
void intcode_cleanup(struct intcode *inst);
void intcode_reset(struct intcode *inst, long *mem, size_t size);
long *intcode_dupl(struct intcode *inst);

#endif // INTCODE_H
