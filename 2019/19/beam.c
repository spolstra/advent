#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "intcode.h"

#define MAX_X 50
#define MAX_Y 50

/* run intcode program with input x, y.
 * reset intcode and return result.
 */
int beam(struct intcode *m, int x, int y) {
    /* safe a copy of the original memory */
    long *copy = intcode_dupl(m);
    size_t orig_size = m->size;

    /* run the intcode with input x, y */
    list_push(m->input, x);
    list_push(m->input, y);
    int status = intcode_run(m);

    /* check result */
    if (status == INST_ERROR) {
        printf("error running intcode\n");
        free(copy);
        return -1;
    }
    int result = list_pop(m->output);

    /* reset intcode machine to original state. */
    intcode_reset(m, copy, orig_size);

    return result;
}

/* check a 50x50 grid for tracker beam status */
int main(void) {
    struct intcode *m = intcode_init(NULL, 0, NULL, NULL);
    int sum = 0;

    for (int x = 0; x < MAX_X; x++) {
        for (int y = 0; y < MAX_Y; y++) {
            int result = beam(m, x, y);
            printf("%d", result);
            if (result) {
                sum++;
            }
        }
        putchar('\n');
    }

    printf("sum: %d\n", sum);
    intcode_cleanup(m);
    return 0;
}
