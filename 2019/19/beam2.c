#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "intcode.h"

#define MIN_Y 10

/* run intcode program with input x, y.
 * reset intcode and return result.
 */
int beam(struct intcode *m, int x, int y) {
    /* safe a copy of the original memory */
    long *copy = intcode_dupl(m);
    size_t orig_size = m->size;

    /* run the intcode with input x, y */
    list_push(m->input, y);
    list_push(m->input, x);
    int status = intcode_run(m);

    /* check result */
    if (status == INST_ERROR) {
        printf("error running intcode\n");
        free(copy);
        return -1;
    }
    int result = list_pop(m->output);

    /* reset intcode machine to original state. */
    intcode_reset(m, copy, orig_size);

    return result;
}

/* check a 50x50 grid for tracker beam status */
int main(void) {
    struct intcode *m = intcode_init(NULL, 0, NULL, NULL);
    int offset = 99; /* search for a 100x100 block */

    for (int y = MIN_Y; ; y++) {
        for (int x = 0; ; x++) {
            int upleft = beam(m, x, y);
            if (upleft) {
                /* upleft ok */
                if (beam(m, x + offset, y)) {
                    /* upleft and upright ok */
                    if (beam(m, x, y + offset)) {
                        /* downleft ok */
                        if (beam(m, x + offset, y + offset)) {
                            /* downright ok, we're done! */
                            printf("x: %d, y: %d\n", x, y);
                            /* note: apparently I mixed up x and y at some point
                             * so calculate answer like this. */
                            printf("answer: %d\n", y * 10000 + x);
                            intcode_cleanup(m);
                            return 0;
                        } else {
                            /* downright fail */
                            break; /* try next line */
                        }
                    } else {
                        /* downleft fail */
                        continue; /* try next pos */
                    }
                } else {
                    /* upright fail */
                    break; /* try next line */
                }
            } else {
                /* upleft fails */
                continue; /* try next x */
            }
        }
    }


    printf("No %dx%d block found\n", offset, offset);
    intcode_cleanup(m);
    return 1;
}
