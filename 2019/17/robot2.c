#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "intcode.h"

enum {SCAF = '#', SPACE = '.', NEWL = 10,
      UP = '^', LEFT = '<', RIGHT = '>', DOWN = 'v', DONE='x'};

enum {NO_DIR = 0, NORTH = 1, SOUTH = 2, WEST = 3, EAST = 4};

#define MAPSIZE 100
typedef char map_t[MAPSIZE][MAPSIZE];

/* show map */
void show_map(map_t m, int maxx, int maxy) {
    printf("x: %d, y: %d\n", maxx, maxy);
    for (int y = 0; y < maxy; y++) {
        for (int x = 0; x < maxx; x++) {
            putchar(m[x][y]);
        }
        putchar('\n');
    }
}

/* read map from intcode instance */
int read_map(struct intcode *inst, map_t m, int *maxx, int *maxy) {
    int x = 0;
    int y = 0;
    int status = intcode_run(inst); // run intcode
    if (status == INST_ERROR) {
        return 1;
    }
    while (inst->output->size > 0) {
        char e = list_remove(inst->output, 0);
        if (e == NEWL) {
            *maxx = x > *maxx ? x : *maxx;
            x = 0;
            y++;
            continue;
        }
        if (x >= MAPSIZE || y >= MAPSIZE) {
            return 1;
        }
        m[x++][y] = e;
    }
    *maxy = y - 1; // last line is empty
    return 0;
}

/* return 1 if intersection, 0 otherwise. */
int is_intersect(map_t m, int x, int y) {
    if (m[x][y] == SCAF &&
            m[x - 1][y] == SCAF && m[x + 1][y] == SCAF &&
            m[x][y - 1] == SCAF && m[x][y + 1] == SCAF) {
        return 1;
    }
    return 0;
}

int find_intersect(map_t m, int maxx, int maxy) {
    int sum = 0;
    /* don't search borders, no intersections there.
     * So we can safely access x-1 and y-1 */
    for (int y = 1; y < maxy - 1; y++) {
        for (int x = 1; x < maxx - 1; x++) {
            if (is_intersect(m, x, y)) {
                // m[x][y] = 'O'; // TODO: calc offset here.
                sum += x * y;
            }
        }
    }
    return sum;
}

void write(struct list *l, char *cmds) {
    for (char *c = cmds; *c; c++) {
        list_push(l, *c);
    }
}

/* upload movements and run robot. */
int run_prog(struct intcode *inst) {
    assert(inst->mem[0] == 1);
    inst->mem[0] = 2;

    char main[] = "C,C,A,B,A,B,A,B,C,B\n";

    char A[] = "R,4,R,6,R,6,R,4,R,4\n";
    char B[] = "L,8,R,6,L,10,L,10\n";
    char C[] = "R,6,L,8,R,8\n";
    char feed[] = "n\n";
    printf("main: %lu\n", strlen(main));
    printf("A: %lu\n", strlen(A));
    printf("B: %lu\n", strlen(B));
    printf("C: %lu\n", strlen(C));

    // give input to instance.
    write(inst->input, main);
    write(inst->input, A);
    write(inst->input, B);
    write(inst->input, C);
    write(inst->input, feed);

    printf("output size: %lu\n", inst->output->size);
    int status = intcode_run(inst); // run intcode
    if (status == INST_ERROR) {
        return 1;
    }
    printf("status: %d\n", status);

    // print video feed
    while (inst->output->size > 1) {
        putchar(list_remove(inst->output, 0));
    }

    // last entry is dust particles (i hope)
    printf("dust: %ld\n", list_remove(inst->output, 0));

    return 0;
}

/* return left of current direction. */
int left(int dir) {
    switch (dir) {
        case NORTH:
            return WEST;
        case SOUTH:
            return EAST;
        case WEST:
            return SOUTH;
        case EAST:
            return NORTH;
        default:
            return NO_DIR;
    }
}

/* return right of current direction. */
int right(int dir) {
    switch (dir) {
        case NORTH:
            return EAST;
        case SOUTH:
            return WEST;
        case WEST:
            return NORTH;
        case EAST:
            return SOUTH;
        default:
            return NO_DIR;
    }
}

/* update x and y for a move in direction dir. */
int dir2pos(int dir, int *x, int *y) {
    switch (dir) {
        case NORTH:
            *y -= 1;
            break;
        case SOUTH:
            *y += 1;
            break;
        case WEST:
            *x -= 1;
            break;
        case EAST:
            *x += 1;
            break;
        default:
            return 1;
    }
    if (*x < 0 || *x > MAPSIZE || *y < 0 || *y > MAPSIZE) {
        return 1;
    }
    return 0;
}

/* return 1 if there is scaffold in direction dir */
int check_scaf(map_t m, int x, int y, int dir) {
    if (dir2pos(dir, &x, &y) != 0) {
        return 0; // cannot walk of the edge of the map
    }
    if (m[x][y] == SCAF) {
        return 1;
    }
    return 0;
}

/* see if we need to move left or right to follow path.
 * If neither works we are done. */
char find_dir(map_t m, int x, int y, int dir) {
    if (check_scaf(m, x, y, left(dir))) {
        return 'L';
    }
    if (check_scaf(m, x, y, right(dir))) {
        return 'R';
    }
    return DONE;
}


/* generate movement commands from path on map */
char *get_movements(map_t map) {
    const int movsize = 300;
    const int bufsize = 10;
    int x = 0; // starting position of roomba
    int y = 16;
    int dir = NORTH;
    char *movs = calloc(movsize, 1);
    char buf[bufsize]; // tmp move to create movement.
    while (1) {
        char turn = find_dir(map, x, y, dir);
        if (turn == DONE) {
            break;
        }
        int steps = 0;
        dir = turn == 'L' ? left(dir) : right(dir); // update dir
        while (check_scaf(map, x, y, dir)) {
            dir2pos(dir, &x, &y); // move in dir
            steps++;
        }
        if (strlen(movs) >= movsize - 4) { // 4 is max append len
            free(movs);
            return NULL;
        }
        snprintf(buf, bufsize, "%c,%d,", turn, steps);
        strcat(movs, buf);
    }
    movs[strlen(movs) - 1] = '\n'; // overwrite last , with newline
    return movs;
}

int main(void) {
    int res = 0;
    int maxx = 0;
    int maxy = 0;
    map_t map;
    memset(map, 0, sizeof(char) * MAPSIZE * MAPSIZE);

    struct intcode *inst = intcode_init(NULL, 0, NULL, NULL);
    if (!inst) {
        return 1;
    }

    // save memory for second run.
    /*
    long *orig_mem = calloc(inst->size, sizeof(long));
    size_t orig_size = inst->size;
    memcpy(orig_mem, inst->mem, inst->size * sizeof(long));
    */
    
    // run instance to get map.
    /*
    if (read_map(inst, map, &maxx, &maxy) != 0) {
        printf("error reading map\n");
        res = 1;
        goto cleanup;
    }
    int calibration = find_intersect(map, maxx, maxy);
    show_map(map, maxx, maxy);
    printf("calibration number: %d\n", calibration);
    */

    /*
    char *movs = get_movements(map);
    printf("movements: %s\n", movs);
    free(movs);
    */

    /*
    intcode_reset(inst, orig_mem, orig_size);
    */
    run_prog(inst); // see if we can run the intcode prog again

cleanup:
    intcode_cleanup(inst);
    return res;
}
