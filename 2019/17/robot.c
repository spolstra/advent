#include <stdio.h>
#include <string.h>

#include "intcode.h"

enum {SCAF = '#', SPACE = '.', NEWL = 10,
      UP = '^', LEFT = '<', RIGHT = '>', DOWN = 'v'};

#define MAPSIZE 100
typedef char map_t[MAPSIZE][MAPSIZE];

/* show map */
void show_map(map_t m, int maxx, int maxy) {
    printf("x: %d, y: %d\n", maxx, maxy);
    for (int y = 0; y < maxy; y++) {
        for (int x = 0; x < maxx; x++) {
            putchar(m[x][y]);
        }
        putchar('\n');
    }
}

/* read map from intcode instance */
int read_map(struct intcode *inst, map_t m, int *maxx, int *maxy) {
    int x = 0;
    int y = 0;
    int status = intcode_run(inst); // run intcode
    if (status == INST_ERROR) {
        return 1;
    }
    while (inst->output->size > 0) {
        char e = list_remove(inst->output, 0);
        if (e == NEWL) {
            *maxx = x > *maxx ? x : *maxx;
            x = 0;
            y++;
            continue;
        }
        if (x >= MAPSIZE || y >= MAPSIZE) {
            return 1;
        }
        m[x++][y] = e;
    }
    *maxy = y - 1; // last line is empty
    return 0;
}

/* return 1 if intersection, 0 otherwise. */
int is_intersect(map_t m, int x, int y) {
    if (m[x][y] == SCAF &&
            m[x - 1][y] == SCAF && m[x + 1][y] == SCAF &&
            m[x][y - 1] == SCAF && m[x][y + 1] == SCAF) {
        return 1;
    }
    return 0;
}

int find_intersect(map_t m, int maxx, int maxy) {
    int sum = 0;
    /* don't search borders, no intersections there.
     * So we can safely access x-1 and y-1 */
    for (int y = 1; y < maxy - 1; y++) {
        for (int x = 1; x < maxx - 1; x++) {
            if (is_intersect(m, x, y)) {
                // m[x][y] = 'O'; // TODO: calc offset here.
                sum += x * y;
            }
        }
    }
    return sum;
}

int main(void) {
    int res = 0;
    int maxx = 0;
    int maxy = 0;
    map_t map;
    memset(map, 0, sizeof(char) * MAPSIZE * MAPSIZE);

    struct intcode *inst = intcode_init(NULL, 0, NULL, NULL);
    if (!inst) {
        return 1;
    }

    if (read_map(inst, map, &maxx, &maxy) != 0) {
        printf("error reading map\n");
        res = 1;
        goto cleanup;
    }
    int calibration = find_intersect(map, maxx, maxy);
    show_map(map, maxx, maxy);
    printf("calibration number: %d\n", calibration);

cleanup:
    intcode_cleanup(inst);
    return res;
}
