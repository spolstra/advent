#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <assert.h>

#define STR_SIZE 64
#define MAX_RULES 256
#define MAX_INPUTS 16
#define LIST_SIZE 256
#define NOT_FOUND INT_MAX
#define TERMINATOR INT_MIN

struct pair {
    char name[STR_SIZE];
    int amount;
};

struct rule {
    struct pair output;
    struct pair inputs[MAX_INPUTS];
};

/* list of stuff we have (positive amounts) and stuff we need
   (negative amounts). */
struct pair stock[LIST_SIZE] = {(struct pair) {.amount = TERMINATOR}};

int read_pair(char *buf, struct pair *pair) {
    char *save_input;
    char *tok, *end;
    tok = strtok_r(buf, " ", &save_input);
    pair->amount = strtol(tok, &end, 10); // read amount
    if (*end != '\0') {
        printf("error reading amount\n");
        return -1;
    }
    tok = strtok_r(NULL, " ", &save_input); // read name
    if (strlen(tok) > 64 - 1) {
        return -1;
    }
    strcpy(pair->name, tok);
    if (pair->name[strlen(pair->name) - 1] == '\n') {
        pair->name[strlen(pair->name) - 1] = '\0';
    }
    return 0;
}

struct rule *read_rules(void) {
    char *inputs, *output;
    char *inputs_tok;
    char *save_rule, *save_inputs;
    char *buf = NULL;
    size_t size;
    struct rule *rules = calloc(MAX_RULES, sizeof(struct rule));

    size_t cur_rule = 0;
    while (getline(&buf, &size, stdin) != -1) {
        inputs = strtok_r(buf, "=", &save_rule);
        output = strtok_r(NULL, "=", &save_rule); // starts with '>' char
        // read inputs
        size_t cur_input = 0;
        while ( (inputs_tok = strtok_r(inputs, ",", &save_inputs)) ) {
            inputs = NULL;  // strtok already primed with inputs.
            if (read_pair(inputs_tok, &rules[cur_rule].inputs[cur_input]) != 0) {
                printf("Error reading input %lu of rule %lu\n", cur_input, \
                        cur_rule);
                free(rules);
                return NULL;
            }
            if (++cur_input > MAX_INPUTS) {
                printf("Too many inputs (%lu) in rule %lu\n", cur_input, \
                        cur_rule);
                free(rules);
                return NULL;
            }
        }
        // mark end of inputs
        rules[cur_rule].inputs[cur_input].amount = TERMINATOR;

        // read output
        if (output[0] != '>') {
            printf("Error reading output of rule %lu\n", cur_rule);
            free(rules);
            return NULL;
        }
        if (read_pair(output + 1, &rules[cur_rule].output) != 0) { // skip '>'
            printf("Error reading output of rule %lu\n", cur_rule);
            free(rules);
            return NULL;
        }
        cur_rule++;
        if (cur_rule > MAX_RULES) {
            printf("Too many rules %lu\n", cur_rule);
            free(rules);
            return NULL;
        }
    }
    // mark the end of the rule list
    rules[cur_rule].output.amount = TERMINATOR;
    free(buf);
    return rules;
}

void print_pair(struct pair *pair) {
    printf("%d, %s", pair->amount, pair->name);
}

void print_inputs(struct pair *inputs, char sep) {
    for (size_t i = 0; inputs[i].amount != TERMINATOR; i++) {
        print_pair(&inputs[i]);
        if (sep != '\0') {
            putchar(sep);
        }
    }
}

void print_list(struct pair *inputs) {
    print_inputs(inputs, '\n');
    return;
}

void print_rules(struct rule *rules) {
    for (size_t i = 0; rules[i].output.amount != TERMINATOR; i++) {
        print_inputs(rules[i].inputs, 0);
        printf(" => ");
        print_pair(&rules[i].output);
        putchar('\n');
    }
}

struct pair *find_list(struct pair *l, char *name) {
    size_t i;
    for (i = 0; l[i].amount != TERMINATOR; i++) {
        if (strcmp(l[i].name, name) == 0) {
            break;
        }
    }
    return &l[i];
}

struct pair *add_pair(struct pair *l, char *name) {
    size_t i;
    for (i = 0; l[i].amount != TERMINATOR; i++) {}
    strcpy(l[i].name, name);
    l[i].amount = 0;
    assert(i + 1 < LIST_SIZE);
    l[i + 1].amount = TERMINATOR; // terminate list!
    return &l[i];
}

int change_amount(struct pair *l, char *name, int amount) {
    struct pair *p = find_list(l, name);
    if (p->amount == TERMINATOR) {
        p = add_pair(l, name);
    }
    p->amount += amount;
    return p->amount;
}

/* return pointer to negative material (not ore) or NULL
 * if no negative is found. */
struct pair *find_neg(struct pair *l) {
    for (size_t i = 0; l[i].amount != TERMINATOR; i++) {
        if (l[i].amount < 0 && strcmp(l[i].name, "ORE") != 0) {
            return &l[i];
        }
    }
    return NULL;
}

/* find rule by output material */
struct rule *find_rule(struct rule *rules, char *output_name) {
    for (size_t i = 0; rules[i].output.amount != TERMINATOR; i++) {
        if (strcmp(rules[i].output.name, output_name) == 0) {
            return &rules[i];
        }
    }
    return NULL;
}

/* apply rule once to generate material */
void generate(struct rule *rule, struct pair *stock) {
    // consume inputs
    for (size_t i = 0; rule->inputs[i].amount != TERMINATOR; i++) {
        change_amount(stock, rule->inputs[i].name, - rule->inputs[i].amount);
    }
    // produce output
    change_amount(stock, rule->output.name, rule->output.amount);
}

void satisfy(struct rule *rules, struct pair *stock, struct pair *need) {
    struct rule *rule = find_rule(rules, need->name);
    while (need->amount < 0) {
        generate(rule, stock);
    }
}

void create_fuel(struct rule *rules, struct pair *stock) {
    struct pair *need = NULL;
    while ( (need = find_neg(stock)) ) {
        satisfy(rules, stock, need);
    }
}

int main() {
    struct rule *rules = read_rules();
    print_rules(rules);

    add_pair(stock, "FUEL");
    change_amount(stock, "FUEL", -1);
     
    create_fuel(rules, stock);
    print_list(stock);

    free(rules);
    return 0;
}

