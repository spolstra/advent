#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <assert.h>

#define STR_SIZE 64
#define MAX_RULES 256
#define MAX_INPUTS 16
#define LIST_SIZE 256
#define NOT_FOUND INT_MAX
#define TERMINATOR INT_MIN

#define FACTOR 10000
#define MAX_ORE 1000000000000
//             -1868223614094

long max_amount = 0;

struct pair {
    char name[STR_SIZE];
    long amount;
};

struct rule {
    struct pair output;
    struct pair inputs[MAX_INPUTS];
};

/* list of stuff we have (positive amounts) and stuff we need
   (negative amounts). */
struct pair stock[LIST_SIZE] = {(struct pair) {.amount = TERMINATOR}};

/* amount is multiplied by factor to increase calculation speed */
/* also record maximum amount that is read */
int read_pair(char *buf, struct pair *pair) {
    char *save_input;
    char *tok, *end;
    tok = strtok_r(buf, " ", &save_input);
    pair->amount = strtol(tok, &end, 10); // read amount
    if (pair->amount > max_amount) {
        max_amount = pair->amount;
    }
    if (*end != '\0') {
        printf("error reading amount\n");
        return -1;
    }
    tok = strtok_r(NULL, " ", &save_input); // read name
    if (strlen(tok) > 64 - 1) {
        return -1;
    }
    strcpy(pair->name, tok);
    if (pair->name[strlen(pair->name) - 1] == '\n') {
        pair->name[strlen(pair->name) - 1] = '\0';
    }
    return 0;
}

struct rule *read_rules(void) {
    char *inputs, *output;
    char *inputs_tok;
    char *save_rule, *save_inputs;
    char *buf = NULL;
    size_t size;
    struct rule *rules = calloc(MAX_RULES, sizeof(struct rule));

    size_t cur_rule = 0;
    while (getline(&buf, &size, stdin) != -1) {
        inputs = strtok_r(buf, "=", &save_rule);
        output = strtok_r(NULL, "=", &save_rule); // starts with '>' char
        // read inputs
        size_t cur_input = 0;
        while ( (inputs_tok = strtok_r(inputs, ",", &save_inputs)) ) {
            inputs = NULL;  // strtok already primed with inputs.
            if (read_pair(inputs_tok,
                        &rules[cur_rule].inputs[cur_input]) != 0) {
                printf("Error reading input %lu of rule %lu\n", cur_input, \
                        cur_rule);
                free(rules);
                return NULL;
            }
            if (++cur_input > MAX_INPUTS) {
                printf("Too many inputs (%lu) in rule %lu\n", cur_input, \
                        cur_rule);
                free(rules);
                return NULL;
            }
        }
        // mark end of inputs
        rules[cur_rule].inputs[cur_input].amount = TERMINATOR;

        // read output
        if (output[0] != '>') {
            printf("Error reading output of rule %lu\n", cur_rule);
            free(rules);
            return NULL;
        }
        // skip '>' char
        if (read_pair(output + 1, &rules[cur_rule].output) != 0) {
            printf("Error reading output of rule %lu\n", cur_rule);
            free(rules);
            return NULL;
        }
        cur_rule++;
        if (cur_rule > MAX_RULES) {
            printf("Too many rules %lu\n", cur_rule);
            free(rules);
            return NULL;
        }
    }
    // mark the end of the rule list
    rules[cur_rule].output.amount = TERMINATOR;
    free(buf);
    return rules;
}

void print_pair(struct pair *pair) {
    printf("(%ld, %s)", pair->amount, pair->name);
}

void print_inputs(struct pair *inputs, char sep) {
    for (size_t i = 0; inputs[i].amount != TERMINATOR; i++) {
        print_pair(&inputs[i]);
        if (sep != '\0') {
            putchar(sep);
        }
    }
}

void print_list(struct pair *inputs) {
    print_inputs(inputs, '\n');
    return;
}

void print_rules(struct rule *rules) {
    for (size_t i = 0; rules[i].output.amount != TERMINATOR; i++) {
        print_inputs(rules[i].inputs, ' ');
        printf(" => ");
        print_pair(&rules[i].output);
        putchar('\n');
    }
}

struct pair *find_list(struct pair *l, char *name) {
    size_t i;
    for (i = 0; l[i].amount != TERMINATOR; i++) {
        if (strcmp(l[i].name, name) == 0) {
            break;
        }
    }
    return &l[i];
}

/* return 1 if there is still ore left */
long ore_left(struct pair *stock) {
    struct pair *ore = find_list(stock, "ORE");
    return ore->amount;
}

struct pair *add_pair(struct pair *l, char *name) {
    size_t i;
    for (i = 0; l[i].amount != TERMINATOR; i++) {}
    strcpy(l[i].name, name);
    l[i].amount = 0;
    assert(i + 1 < LIST_SIZE);
    l[i + 1].amount = TERMINATOR; // terminate list!
    return &l[i];
}

long change_amount(struct pair *l, char *name, long amount) {
    struct pair *p = find_list(l, name);
    if (p->amount == TERMINATOR) {
        p = add_pair(l, name);
    }
    p->amount += amount;
    return p->amount;
}

/* return pointer to negative material (not ore) or NULL
 * if no negative is found. */
struct pair *find_neg(struct pair *l) {
    for (size_t i = 0; l[i].amount != TERMINATOR; i++) {
        if (l[i].amount < 0 && strcmp(l[i].name, "ORE") != 0) {
            return &l[i];
        }
    }
    return NULL;
}

/* find rule by output material */
struct rule *find_rule(struct rule *rules, char *output_name) {
    for (size_t i = 0; rules[i].output.amount != TERMINATOR; i++) {
        if (strcmp(rules[i].output.name, output_name) == 0) {
            return &rules[i];
        }
    }
    return NULL;
}

/* apply rule once to generate material */
void generate(struct rule *rule, struct pair *stock, long factor) {
    // consume inputs
    for (size_t i = 0; rule->inputs[i].amount != TERMINATOR; i++) {
        change_amount(stock, rule->inputs[i].name,
                - rule->inputs[i].amount * factor);
    }
    // produce output
    change_amount(stock, rule->output.name, rule->output.amount * factor);
}

void satisfy(struct rule *rules, struct pair *stock, struct pair *need,
        long factor) {
    struct rule *rule = find_rule(rules, need->name);
    // size_t cnt = 0;
    while (need->amount < 0) {
        // tune speedup factor
        if (factor != 1 && labs(need->amount) < (10 * max_amount) * factor) {
            factor = factor / 2 > 0 ? factor / 2 : 1;
        }

        generate(rule, stock, factor);
        /*
        if (++cnt % 1000000 == 0) {
            print_list(stock);
            printf("factor: %ld\n", factor);
            printf("----------------------\n");
        }
        */
    }
}

void create_fuel(struct rule *rules, struct pair *stock, long factor) {
    struct pair *need = NULL;
    while ( (need = find_neg(stock)) ) {
        satisfy(rules, stock, need, factor);
    }
}

int main() {
    struct rule *rules = read_rules();
    print_rules(rules);

    long fuel = -7289275;
    // long fuel = -8289275;
    // long fuel = -4436983;
    // answer is 4436981
    while (1) {
        add_pair(stock, "FUEL");
        change_amount(stock, "FUEL", fuel);
        create_fuel(rules, stock, FACTOR);
        printf("ore left: %ld fuel: %ld\n", ore_left(stock), fuel);
        if (ore_left(stock) < -MAX_ORE * 1.001) {
            fuel = fuel * ((double) MAX_ORE / -ore_left(stock)) - 20;
        } else if (ore_left(stock) < -MAX_ORE) {
            fuel++;
        } else {
            break;
        }
        printf("new fuel estimate: %ld\n", fuel);
        stock[0].amount = TERMINATOR;
    }

    print_list(stock);
    printf("calculated fuel: %ld\n", -fuel);

    free(rules);
    return 0;
}

