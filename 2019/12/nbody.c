#include <stdio.h>
#include <stdlib.h>

#define NPLANETS 4

#define NDIMS 3

#define STEPS 1000
#define PRINT_INTVAL 10

struct pos {
    long x;
    long y;
    long z;
};

struct vel {
    long x;
    long y;
    long z;
};


struct pos *read_pos() {
    struct pos *ppos = calloc(NPLANETS, sizeof(struct pos));
    for (size_t p = 0; p < NPLANETS; p++) {
        int nread = scanf("<x=%ld, y=%ld, z=%ld>\n",
                &ppos[p].x, &ppos[p].y, &ppos[p].z);
        if (nread != 3) {
            printf("Only read %d positions on line %lu\n", nread, p + 1);
        }
    }
    return ppos;
}

void print_state(struct pos *ppos, struct vel *vels) {
    for (size_t p = 0; p < NPLANETS; p++) {
        printf("pos=<x=%ld, y=%ld, z=%ld>, ", ppos[p].x, ppos[p].y, ppos[p].z);
        printf("vel=<x=%ld, y=%ld, z=%ld>\n", vels[p].x, vels[p].y, vels[p].z);
    }
}

long delta_v(long x1, long x2) {
    if (x1 > x2) {
        return -1;
    } else if (x1 < x2) {
        return +1;
    }
    return 0;
}

/* update velocities of the planets positions. */
void update_vels(struct pos *ppos, struct vel *vels) {
    for (size_t p1 = 0; p1 < NPLANETS; p1++) {
        for (size_t p2 = 0; p2 < NPLANETS; p2++) {
            if (p1 == p2) {
                continue;
            }
            vels[p1].x += delta_v(ppos[p1].x, ppos[p2].x);
            vels[p1].y += delta_v(ppos[p1].y, ppos[p2].y);
            vels[p1].z += delta_v(ppos[p1].z, ppos[p2].z);
        }
    }
}

/* apply the velocites to the planets. */
void apply_vels(struct pos *ppos, struct vel *vels) {
    for (size_t p = 0; p < NPLANETS; p++) {
        ppos[p].x += vels[p].x;
        ppos[p].y += vels[p].y;
        ppos[p].z += vels[p].z;
    }
}

long planet_energy(struct pos p, struct vel v) {
    long potential = labs(p.x) + labs(p.y) + labs(p.z);
    long kinetic = labs(v.x) + labs(v.y) + labs(v.z);
    return potential * kinetic;
}

long energy_sum(struct pos *ppos, struct vel *vels) {
    long sum = 0;
    for (size_t p = 0; p < NPLANETS; p++) {
        sum += planet_energy(ppos[p], vels[p]);
    }
    return sum;
}

int main(void) {
    struct pos *ppos = read_pos();
    struct vel *vels = calloc(NPLANETS, sizeof(struct vel)); // vel set to 0

    for (size_t t = 0; t <= STEPS; t++) {
        if (t % PRINT_INTVAL == 0) {
            printf("After %lu steps:\n", t);
            print_state(ppos, vels);
            printf("Sum of total energy: %ld\n", energy_sum(ppos, vels));
            putchar('\n');
        }

        update_vels(ppos, vels);
        apply_vels(ppos, vels);
    }

    free(ppos);
    free(vels);
}
