#include <numeric>
#include <iostream>

using std::cout;
using std::endl;

int main(void) {
    cout << "hello there" << endl;
    cout << std::lcm(15, 6) << endl;
    cout << std::lcm(15, std::lcm(6, 10)) << endl;

    return 0;
}
