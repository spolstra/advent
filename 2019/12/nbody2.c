#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NPLANETS 4

#define NDIMS 3

#define STEPS 1000
#define PRINT_INTVAL 10000000

struct xyz {
    int x;
    int y;
    int z;
};

struct cxyz {
    signed char x;
    signed char y;
    signed char z;
};


struct planet {
    struct xyz pos;
    struct cxyz v;
};

struct system {
    struct planet planet[NPLANETS];
};

struct system *read_system() {
    struct system *system = calloc(1, sizeof(struct system));
    for (size_t p = 0; p < NPLANETS; p++) {
        int nread = scanf("<x=%d, y=%d, z=%d>\n",
                &system->planet[p].pos.x, &system->planet[p].pos.y,
                &system->planet[p].pos.z);
        if (nread != 3) {
            printf("Only read %d positions on line %lu\n", nread, p + 1);
        }
        system->planet[p].v.x = 0;
        system->planet[p].v.y = 0;
        system->planet[p].v.z = 0;
    }
    return system;
}

void print_system(struct system *s) {
    for (size_t p = 0; p < NPLANETS; p++) {
        printf("pos=<x=%d, y=%d, z=%d>, ", s->planet[p].pos.x,
                                              s->planet[p].pos.y,
                                              s->planet[p].pos.z);
        printf("vel=<x=%d, y=%d, z=%d>\n", s->planet[p].v.x,
                                              s->planet[p].v.y,
                                              s->planet[p].v.z);
    }
}

int delta_v(int x1, int x2) {
    if (x1 > x2) {
        return -1;
    } else if (x1 < x2) {
        return 1;
    }
    return 0;
}

/* update velocities of the planets positions. */
void update_vels(struct system *s) {
    for (size_t p1 = 0; p1 < NPLANETS; p1++) {
        for (size_t p2 = 0; p2 < NPLANETS; p2++) {
            if (p1 == p2) {
                continue;
            }
            s->planet[p1].v.x += delta_v(s->planet[p1].pos.x,
                                         s->planet[p2].pos.x);
            s->planet[p1].v.y += delta_v(s->planet[p1].pos.y,
                                         s->planet[p2].pos.y);
            s->planet[p1].v.z += delta_v(s->planet[p1].pos.z,
                                         s->planet[p2].pos.z);
        }
    }
}

/* apply the velocites to the planets. */
void apply_vels(struct system *s) {
    for (size_t p = 0; p < NPLANETS; p++) {
        s->planet[p].pos.x += s->planet[p].v.x;
        s->planet[p].pos.y += s->planet[p].v.y;
        s->planet[p].pos.z += s->planet[p].v.z;
    }
}

long planet_energy(struct planet *p) {
    long potential = labs(p->pos.x) + labs(p->pos.y) + labs(p->pos.z);
    long kinetic = labs(p->v.x) + labs(p->v.y) + labs(p->v.z);
    return potential * kinetic;
}

long energy_sum(struct system *s) {
    long sum = 0;
    for (size_t p = 0; p < NPLANETS; p++) {
        sum += planet_energy(&(s->planet[p])); // () not needed [] stronger than &
    }
    return sum;
}

int xyz_equal(struct xyz *a, struct xyz *b) {
    return ( a->x == b->x && \
             a->y == b->y && \
             a->z == b->z );
}

int vxyz_equal(struct cxyz *a, struct cxyz *b) {
    return ( a->x == b->x && \
             a->y == b->y && \
             a->z == b->z );
}

int planet_equal(struct planet *a, struct planet *b) {
    return xyz_equal(&a->pos, &b->pos) && vxyz_equal(&a->v, &b->v);
}

int planet_equalx(struct planet *a, struct planet *b) {
    return a->pos.x == b->pos.x && a->v.x == b->v.x;
}

int planet_equaly(struct planet *a, struct planet *b) {
    return a->pos.y == b->pos.y && a->v.y == b->v.y;
}

int planet_equalz(struct planet *a, struct planet *b) {
    return a->pos.z == b->pos.z && a->v.z == b->v.z;
}

int system_equalx(struct system *s1, struct system *s2) {
    for (size_t p = 0; p < NPLANETS; p++) {
        if (!planet_equalx(&(s1->planet[p]), &(s2->planet[p]))) {
            return 0;
        }
    }
    return 1;
}

int system_equaly(struct system *s1, struct system *s2) {
    for (size_t p = 0; p < NPLANETS; p++) {
        if (!planet_equaly(&(s1->planet[p]), &(s2->planet[p]))) {
            return 0;
        }
    }
    return 1;
}

int system_equalz(struct system *s1, struct system *s2) {
    for (size_t p = 0; p < NPLANETS; p++) {
        if (!planet_equalz(&(s1->planet[p]), &(s2->planet[p]))) {
            return 0;
        }
    }
    return 1;
}

int main(void) {

    struct system *system = read_system();
    struct system *initial = malloc(sizeof(struct system));
    memcpy(initial, system, sizeof(struct system));

    size_t cycle_x = 0;
    size_t cycle_y = 0;
    size_t cycle_z = 0;
    for (size_t t = 0;; t++) {
        update_vels(system);
        apply_vels(system);
        if (system_equalx(system, initial)) {
            printf("X: start state reached after %lu steps\n", t + 1);
            cycle_x = t + 1;
        }
        if (system_equaly(system, initial)) {
            printf("Y: start state reached after %lu steps\n", t + 1);
            cycle_y = t + 1;
        }
        if (system_equalz(system, initial)) {
            printf("X: start state reached after %lu steps\n", t + 1);
            cycle_z = t + 1;
        }
        if (t % PRINT_INTVAL == 0) {
            printf("steps: %lu\n", t);
        }
        if (cycle_x && cycle_y && cycle_z) {
            break;
        }
    }

    printf("cycles: X: %lu, Y: %lu, Z: %lu\n", cycle_x, cycle_y, cycle_z);
    printf("common: %lu\n", cycle_x * cycle_y * cycle_z);
    free(system);
    free(initial);
}
