#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "list.h"
#include "intcode.h"

int main(void) {

    struct intcode *inst = intcode_init(NULL, 0, NULL, NULL);
    list_push(inst->input, 1); // output 2671328082
    // list_push(inst->input, 2); // output 59095
 
    // intcode_dump(inst);

    intcode_run(inst);
    list_print(inst->output);
    intcode_cleanup(inst);
    return 0;
}
