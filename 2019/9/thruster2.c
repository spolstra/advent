#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "list.h"
#include "intcode.h"

#define NUM_THRUSTERS 5

/* input to the program */
#define INPUT 5

void permutate(struct list *prefix, struct list *l, char *out_buf) {
    char buf[10];
    if (l->size == 1) {
        for (size_t i = 0; i < prefix->size; i++) {
            sprintf(buf, "%ld", prefix->data[i]);
            strcat(out_buf, buf);
        }
        sprintf(buf, "%ld", l->data[0]);
        strcat(out_buf, buf);
        return;
    }
    for (size_t i = 0; i < l->size; i++) {
        struct list *l2 = list_clone(l);
        list_push(prefix, l2->data[i]);
        list_remove(l2, i);
        permutate(prefix, l2, out_buf);
        list_remove(prefix, prefix->size - 1);
        list_cleanup(l2);
    }
}

char *gen_permutations(int num, int offset) {
    struct list *l = list_init();
    struct list *prefix = list_init();
    char *buf = calloc(4096, sizeof(char));
    for (int i = offset; i < num + offset; i++) {
        list_push(l, i);
    }
    permutate(prefix, l, buf);
    list_cleanup(l);
    list_cleanup(prefix);
    return buf;
}

void print_perms(char *perms) {
    size_t index = 0;
    while (perms[index]) {
        printf("%c ", perms[index]);
        index++;
        if (index % 5 == 0) {
            putchar('\n');

        }
    }
}

/* return input index for instance i.
 * Defined as i - 1, module 5, so input of 0 is 4. */
int input_buf(int i) {
    return (i + NUM_THRUSTERS - 1) % NUM_THRUSTERS;
}

int output_buf(int i) {
    return i;
}

struct intcode **create_instances(long *m, int size, struct list **buffers,
        int num) {
    struct intcode **insts = malloc(sizeof(struct intcode*) * num);
    for (int i = 0; i < num; i++) {
        long *mem_copy = malloc(size * sizeof(long));
        memcpy(mem_copy, m, size * sizeof(long));
        insts[i] = intcode_init(mem_copy, size,
                buffers[input_buf(i)], buffers[output_buf(i)]);
    }
    return insts;
}

/* initialize all mems with contents of mem and reset pc */
int reset_insts(long *m, int s, struct intcode **insts, int num) {
    for (int i = 0; i < num; i++) {
        long *tmp = realloc(insts[i]->mem, s * sizeof(long));
        if (!tmp) { return 1; }
        insts[i]->mem = tmp;
        memcpy(insts[i]->mem, m, s * sizeof(long));
        insts[i]->size = s;
        insts[i]->pc = 0;
        insts[i]->base = 0;
    }
    return 0;
}

void cleanup_mems(int **mems, int num) {
    for (int i = 0; i < num; i++) {
        free(mems[i]);
    }
    free(mems);
}

struct list **setup_buffers(int num) {
    struct list **buffers = malloc(sizeof(struct list*) * num);
    for (int i = 0; i < num; i++) {
        buffers[i] = list_init();
    }
    return buffers;
}

void cleanup_buffers(struct list **buffers, int num) {
    for (int i = 0; i < num; i++) {
        list_cleanup(buffers[i]);
    }
    free(buffers);
}

int main(void) {
    int ret_code = 0;
    char *perms = gen_permutations(NUM_THRUSTERS, 5);
    // print_perms(perms);
    // free(perms) ; return 0;

    /* setup instance memories, pc's and buffers */
    struct list **buffers = setup_buffers(NUM_THRUSTERS);
    size_t size;
    long *mem_orig = intcode_load_mem(&size);
    struct intcode **insts = create_instances(mem_orig ,size, buffers,
            NUM_THRUSTERS);

    size_t pi = 0; // index in permutation string
    int max_thrust = 0;
    int max_seq[NUM_THRUSTERS] = {0};
    int thrust = 0;

    while (perms[pi]) {
        printf("pi: %zu\n", pi);
        // int **mems = setup_mems(mem_orig, NUM_THRUSTERS);
        for (int i = 0; i < NUM_THRUSTERS; i++) {
            // add phase setting input to all buffers
            list_push(buffers[input_buf(i)], perms[pi++] - '0');
        }
        list_push(buffers[input_buf(0)], 0); // 0 as initial input to amp A

        /* run until amp E has halted */
        int runs = 0;
        int res[NUM_THRUSTERS] = {0};
        while(1) {
            /* do one run of amps A to E */
            for (int i = 0; i < NUM_THRUSTERS; i++) {
                /* run the interpreter */
                res[i] = intcode_run(insts[i]);
                if (res[i] == -1) {
                    printf("Error while executing program. Exiting..\n");
                    ret_code = 1;
                    goto cleanup;
                }
            }
            if (res[NUM_THRUSTERS - 1] == 0) { // If amp E halted
                thrust = list_remove(buffers[output_buf(NUM_THRUSTERS - 1)], 0);
                /* check if all instances have stopped. */
                for (int i = 0; i < NUM_THRUSTERS; i++) {
                    assert(res[i] == 0);
                }
                /* check if all buffers are empty. */
                for (int i = 0; i < NUM_THRUSTERS; i++) {
                    assert(buffers[i]->size == 0);
                }
                printf("end of %d runs\n", runs);
                break;
            }
            runs++;
        }
        assert(pi % NUM_THRUSTERS == 0);
        if (thrust > max_thrust) {
            max_thrust = thrust;
            /* also record phase sequence that gave the maximum thrust */
            for (int i = 0; i < NUM_THRUSTERS; i++) {
                max_seq[i] = perms[pi - NUM_THRUSTERS + i] - '0';
            }
        }
        if (reset_insts(mem_orig, size, insts, NUM_THRUSTERS) != 0) {
            printf("reset failed\n");
            goto cleanup;
        }
    }

    printf("%d\n", max_thrust);
    for (int i = 0; i < NUM_THRUSTERS; i++) {
        printf("%d ", max_seq[i]);
    }
    putchar('\n');

    /* cleanup */
cleanup:
    cleanup_buffers(buffers, NUM_THRUSTERS);
    for (int i = 0; i < NUM_THRUSTERS; i++) {
        insts[i]->input = insts[i]->output = NULL; // already freed.
        intcode_cleanup(insts[i]);
    }
    free(insts);
    free(mem_orig);
    free(perms);
    return ret_code;
}
