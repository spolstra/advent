#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


#define MAX_POINTS 20000
#define BUFSIZE 1024

#define ASTEROID '#'

struct xy {
    int x;
    int y;
    int rx;
    int ry;
    double phi;
    double r;
};

struct xy *load_map(size_t *size, size_t *nx, size_t *ny) {
    char *buf = malloc(BUFSIZE);
    if (!buf) {
        return NULL;
    }
    struct xy *xys = calloc(MAX_POINTS, sizeof(struct xy));
    if (!xys) {
        free(buf);
        return NULL;
    }

    size_t x = 0;
    size_t y = 0;
    *size = 0;
    while (fgets(buf, BUFSIZE, stdin)) {
        if (buf[strlen(buf) - 1] != '\n') {
            printf("fgets truncation.\n");
            free(buf);
            free(xys);
            return NULL;
        }
        x = 0;
        for (char *cur = buf; *cur; cur++) {
            if (*cur == '\n') {
                break;
            }
            if (*cur == ASTEROID) {
                xys[*size].x = x;
                xys[*size].y = y;
                (*size)++;
            }
            x++;
        }
        y++;
    }
    free(buf);

    *nx = x;
    *ny = y;
    return xys;
}

void print_xys(struct xy *xys, size_t size) {
    for (size_t i = 0; i < size; i++) {
        printf("%lu: (%d,%d) r(%d,%d) phi: %f, r: %f\n", i,
                xys[i].x, xys[i].y,
                xys[i].rx, xys[i].ry,
                xys[i].phi, xys[i].r);
    }
}

/* make xys relative to origin. */
void add_rel(struct xy *xys, size_t size, struct xy origin) {
    for (size_t i = 0; i < size; i++) {
        xys[i].rx = xys[i].x - origin.x;
        xys[i].ry = xys[i].y - origin.y;
    }
}

/* add polar coordinates. negate phi for easy sorting. */
void add_polars_negate(struct xy *xys, size_t size) {
    for (size_t i = 0; i < size; i++) {
        xys[i].phi = -atan2(xys[i].rx, xys[i].ry);
        xys[i].r = sqrt(xys[i].rx * xys[i].rx + xys[i].ry * xys[i].ry);
    }
}

/* find x'th visibile asteroid. return index and set fnd to that
 * asteroid */
int find_xth(struct xy *xys, size_t size, size_t x, struct xy *fnd) {
    size_t count = 0;
    double prev_phi = 10; // out of range.
    size_t i;
    for (i = 0; i < size; i++) {
        if (xys[i].phi == prev_phi) {
            continue; // skip over invisibles
        }
        count++;
        prev_phi = xys[i].phi;
        if (count == x) {
            break;
        }
    }
    *fnd = xys[i]; // count was updated
    return i; // index still points to the one we found
}

int find_visible(struct xy *xys, size_t size) {
    size_t count = 0;
    double prev_phi = 10; // out of range.
    size_t i;
    for (i = 0; i < size; i++) {
        if (xys[i].phi == prev_phi) {
            continue; // skip over invisibles
        }
        count++;
        prev_phi = xys[i].phi;
    }
    return count;
}


/* check if cur is in xys. */
int is_full(struct xy *xys, size_t size, struct xy cur) {
    for (size_t i = 0; i < size; i++) {
        if (xys[i].x == cur.x && xys[i].y == cur.y) {
            return 1;
        }
    }
    return 0;
}

int xy_eq(struct xy a, struct xy b) {
    if (a.x == b.x && a.y == b.y) {
        return 1;
    }
    return 0;
}

/* compare two xy's using phi and then r. */
int xy_comp(const void *first, const void *snd) {
    struct xy *f = (struct xy*) first;
    struct xy *s = (struct xy*) snd;
    if (f->phi < s->phi) {
        return -1;
    } else if (f->phi > s->phi) {
        return 1;
    }
    /* if phi's are the same check r. */
    if (f->r < s->r) {
        return -1;
    } else if (f->r > s->r) {
        return 1;
    }
    return 0;
}

// a: (26,36)
// num_vis: 347

/* 
 * use this example to test shooting:
 *
 * .#..#
 * .....
 * #X###
 * ....#
 * ...##
 */
int main(void) {
    size_t size, x, y;
    // struct xy origin = {.x = 1, .y = 2};

    // real input
    struct xy origin = {.x = 26, .y = 36};

    struct xy *xys = load_map(&size, &x, &y);
    print_xys(xys, size);
    putchar('\n');

    add_rel(xys, size, origin);
    add_polars_negate(xys, size);

    print_xys(xys, size);
    putchar('\n');

    /* sort on phi. */
    qsort(xys, size, sizeof(struct xy), &xy_comp);
    print_xys(xys, size);

    struct xy target;
    int i = find_xth(xys, size, 200, &target);
    printf("%d: (%d,%d) => %d\n", i, target.x, target.y,
            target.x * 100 + target.y);

    printf("%d\n", find_visible(xys, size));

    free(xys);
    return 0;
}
