#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define MAX_POINTS 20000
#define BUFSIZE 1024

#define ASTEROID '#'

struct xy {
    int x;
    int y;
};

struct xy *load_map(size_t *size, size_t *nx, size_t *ny) {
    char *buf = malloc(BUFSIZE);
    if (!buf) {
        return NULL;
    }
    struct xy *xys = calloc(MAX_POINTS, sizeof(struct xy));
    if (!xys) {
        free(buf);
        return NULL;
    }

    size_t x = 0;
    size_t y = 0;
    *size = 0;
    while (fgets(buf, BUFSIZE, stdin)) {
        if (buf[strlen(buf) - 1] != '\n') {
            printf("fgets truncation.\n");
            free(buf);
            free(xys);
            return NULL;
        }
        x = 0;
        for (char *cur = buf; *cur; cur++) {
            if (*cur == '\n') {
                break;
            }
            if (*cur == ASTEROID) {
                xys[*size].x = x;
                xys[*size].y = y;
                (*size)++;
            }
            x++;
        }
        y++;
    }
    free(buf);

    *nx = x;
    *ny = y;
    return xys;
}

void print_xys(struct xy *xys, size_t size) {
    for (size_t i = 0; i < size; i++) {
        printf("(%d,%d)\n", xys[i].x, xys[i].y);
    }
}

/* check if cur is in xys. */
int is_full(struct xy *xys, size_t size, struct xy cur) {
    for (size_t i = 0; i < size; i++) {
        if (xys[i].x == cur.x && xys[i].y == cur.y) {
            return 1;
        }
    }
    return 0;
}

int xy_eq(struct xy a, struct xy b) {
    if (a.x == b.x && a.y == b.y) {
        return 1;
    }
    return 0;
}

/* check if b is visible from a. */
int is_visible(struct xy *xys, size_t size, struct xy a, struct xy b) {
    /* b - a => dv (dx,dy) vector. Find step vector from this dv vector.*/
    struct xy dv = (struct xy){.x = b.x - a.x, .y = b.y - a.y};

    /* record sign of dv and use abs values for the calculation. */
    int sign_x = 1;
    int sign_y = 1;
    if (dv.x < 0) {
        sign_x = -1;
    }
    if (dv.y < 0) {
        sign_y = -1;
    }
    dv.x = abs(dv.x);
    dv.y = abs(dv.y);

    struct xy step = (struct xy){.x = 0, .y = 0};
    if (dv.x != 0 && dv.y != 0) {
        /* we do dv.y/dv.x y-steps per 1 x-step. */
        for (int i = 1; i < abs(dv.x); i++) {
            if ((i * dv.y) % dv.x == 0) { // found smallest step vector
                step.x = i;
                step.y = (i * dv.y) / dv.x;
                break;
            }
        }
    } else if (dv.x == 0) { // vertical line is a special case.
        step.x = 0;
        step.y = 1;
    } else { //horizontal
        step.y = 0;
        step.x = 1;
    }
    /* Now put back the sign we saved in the beginning. */
    if (sign_x < 1) {
        step.x *= -1;
    }
    if (sign_y < 1) {
        step.y *= -1;
    }

    // If did not find a step  there
    // are no intermediate points so b is visible from from a.
    if (xy_eq(step, (struct xy){.x = 0, .y = 0})) {
        return 1;
    }
    // Otherwise we step from a to b to check if there is asteroid
    // between a and b. (only check if x matches, that is enough)
    for (size_t i = 1;; i++) {
        struct xy intermediate = (struct xy){.x =  a.x + (i * step.x),
                                             .y = a.y + (i * step.y)};
        if (xy_eq(intermediate, b)) {
            break;
        }
        if (is_full(xys, size, intermediate)) {
            return 0;
        }
    }
    // Nothing between a and b, so b is visible!
    return 1;
}

/* count how many asteroids are visible from a. */
size_t count_visibles(struct xy *xys, size_t size, struct xy a) {
    size_t count = 0;
    for (size_t i = 0; i < size; i++) {
        if (xy_eq(a, xys[i])) {
            continue;
        }
        if (is_visible(xys, size, a, xys[i])) {
            count++;
        }
    }
    return count;
}

struct xy max_visible(struct xy *xys, size_t size) {
    size_t max_vis = 0;
    size_t max_index = 0;
    for (size_t i = 0; i < size; i++) {
        size_t vis = count_visibles(xys, size, xys[i]);
        if (vis > max_vis) {
            max_vis = vis;
            max_index = i;
        }
    }
    return xys[max_index];
}

int main(void) {
    size_t size, x, y;
    struct xy *xys = load_map(&size, &x, &y);
    print_xys(xys, size);

    struct xy a = max_visible(xys, size);
    printf("a: (%d,%d)\n", a.x, a.y);
    size_t num_vis = count_visibles(xys, size, a);
    printf("num_vis: %lu\n", num_vis);

    free(xys);
    return 0;
}
