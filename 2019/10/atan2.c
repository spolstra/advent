#include <math.h>
#include <stdio.h>

int main(void) {
	// we can use atan2(x,y) to sort points clockwise from pi..-pi
	// so we -atan2(x,y) will sort from -pi..pi which is what we want.

    printf("%f\n", atan2(1, -10));
    printf("%f\n", atan2(2, -2));
    printf("%f\n", atan2(10, 0));
    printf("%f\n", atan2(2, 2));
    printf("%f\n", atan2(0, 10));
    printf("%f\n", atan2(-2, 2));
    printf("%f\n", atan2(-2, 0));
    printf("%f\n", atan2(-1, -5));
    return 0;
}
