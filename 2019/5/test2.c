#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define MEM_SIZE 2048
#define BUF_SIZE 1024
#define MAX_PARAM 3

/* input to the program */
#define INPUT 5

enum instr_t {ADD = 1, MULT = 2, READ = 3, WRITE = 4,
              JMPT = 5, JMPF = 6, SETLT = 7, SETE = 8, HALT = 99};

int *load(int *pos) {
    char buf[BUF_SIZE];
    int *m = calloc(MEM_SIZE, sizeof(int));
    char *end;

    *pos = 0;
    while (fgets(buf, BUF_SIZE, stdin)) {
        char *tok = strtok(buf, ",");
        m[(*pos)++] = strtol(tok, &end, 10);
        assert(*end == '\0' || *end == '\n');
        assert(*pos < MEM_SIZE);

        while ((tok = strtok(NULL, ","))) {
            m[(*pos)++] = strtol(tok, &end, 10);
            assert(*end == '\0' || *end == '\n');
            assert(*pos < MEM_SIZE);
        }
    }
    return m;
}

void dump_memory(int *mem, int size) {
    for (int i = 0; i < size; i++) {
        printf("%d,", mem[i]);
    }
    putchar('\n');
}

void get_params(int pc, int *mem, int *params, int n) {
    int param_base[MAX_PARAM] = {100, 1000, 10000};
    for (int i = 0; i < n; i++) {
        if ((mem[pc] / param_base[i]) % 10 == 1) {
            assert(i != 2); // cannot write to dest parameter as immediate
            params[i] = mem[pc + i + 1];
        } else {
            params[i] = mem[mem[pc + i + 1]];
        }
    }
}


int run(int *mem) {
    int pc = 0;
    int params[MAX_PARAM];

    while ((mem[pc] % 100) != HALT) {
        switch(mem[pc] % 100) {
            case ADD:
                get_params(pc, mem, params, 3);
                mem[mem[pc + 3]] = params[0] + params[1];
                pc += 4;
                break;
            case MULT:
                get_params(pc, mem, params, 3);
                mem[mem[pc + 3]] = params[0] * params[1];
                pc += 4;
                break;
            case READ:
                mem[mem[pc + 1]] = INPUT;
                pc += 2;
                break;
            case WRITE:
                get_params(pc, mem, params, 1);
                printf("%d\n", params[0]);
                pc += 2;
                break;
            case JMPT:
                get_params(pc, mem, params, 2);
                if (params[0] != 0) {
                    pc = params[1];
                } else {
                    pc += 3;
                }
                break;
            case JMPF:
                get_params(pc, mem, params, 2);
                if (params[0] == 0) {
                    pc = params[1];
                } else {
                    pc += 3;
                }
                break;
            case SETLT:
                get_params(pc, mem, params, 3);
                if (params[0] < params[1]) {
                    mem[mem[pc + 3]] = 1; // TODO always position?
                } else {
                    mem[mem[pc + 3]] = 0;
                }
                pc += 4;
                break;
            case SETE:
                get_params(pc, mem, params, 3);
                if (params[0] == params[1]) {
                    mem[mem[pc + 3]] = 1; // TODO always position?
                } else {
                    mem[mem[pc + 3]] = 0;
                }
                pc += 4;
                break;
            default:
                printf("Unknown opcode %d at pc %d\n", mem[pc], pc);
                return 1;
        }
    }
    return 0;
}

int main(void) {
    int size;

    /* load code from stdin */
    int *mem = load(&size);

    /* run the interpreter */
    if (run(mem) != 0) {
        printf("Error while executing program. Exiting..\n");
        free(mem);
        return 1;
    }

    /* cleanup */
    free(mem);
    return 0;
}


