#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#define BUF_SIZE 20000

#define NCOL 25
#define NROW 6 
#define IMG_SIZE (NCOL * NROW)


char *read_image(void) {
    char *buf = malloc(sizeof(char) * BUF_SIZE);
    fgets(buf, BUF_SIZE, stdin);
    size_t l = strlen(buf);
    buf[l - 1] = '\0'; // chop \n
    return buf;
}

int find_layer(char *img) {
    size_t num_img = strlen(img) / (NCOL * NROW); 
    int layer = -1;
    int min_zeros = INT_MAX;

    for (size_t i = 0; i < num_img; i++) {
        int zeros = 0;
        for (size_t pos = 0; pos < NCOL * NROW; pos++) {
            if (img[IMG_SIZE * i + pos] == '0') {
                zeros++;
            }
        }
        if (zeros < min_zeros) {
            printf(" %d layer %zu\n", zeros, i);
            min_zeros = zeros;
            layer = i;
        }
    }
    return layer;
}

size_t count_digit(char *img, int layer, char digit) {
    size_t count = 0;
    for (size_t pos = 0; pos < NCOL * NROW; pos++) {
        if (img[IMG_SIZE * layer + pos] == digit) {
            count++;
        }
    }
    return count;
}

int main(void) {
    char *img = read_image();
    // printf("[%s]\n", img);
    int layer = find_layer(img);
    if (layer == -1) {
        printf("find_layer failed\n");
        free(img);
        return 1;
    }

    printf("%d\n", layer);
    printf("%lu\n", count_digit(img, layer, '1') * \
            count_digit(img, layer, '2'));

    free(img);
    return 0;
}
