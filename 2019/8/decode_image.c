#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#define BUF_SIZE 20000

#define NCOL 25
#define NROW 6 

// example
//#define NCOL 2
//#define NROW 2

#define IMG_SIZE (NCOL * NROW)

enum {BLACK = '0', WHITE = '1', TRANSP = '2'};

char *read_image(void) {
    char *buf = malloc(sizeof(char) * BUF_SIZE);
    if (!buf) {
        return NULL;
    }
    fgets(buf, BUF_SIZE, stdin);
    size_t l = strlen(buf);
    buf[l - 1] = '\0'; // chop \n
    return buf;
}

char pixel_value(char *buf, int pos) {
    size_t num_layer = strlen(buf) / (NCOL * NROW); 

    for (size_t i = 0; i < num_layer; i++) {
        switch (buf[(IMG_SIZE * i) + pos]) {
            case BLACK:
                return BLACK;
            case WHITE:
                return WHITE;
        }
    }
    return TRANSP; // this signals a problem
}

char *decode_img(char *input) {
    char *output = calloc(IMG_SIZE, sizeof(char));
    for (size_t pos = 0; pos < NCOL * NROW; pos++) {
        output[pos] = pixel_value(input, pos);
        if (output[pos] == TRANSP) {
            free(output);
            return NULL;
        }
    }
    return output;
}

int print_img(char *img) {
    for (size_t pos = 0; pos < NCOL * NROW; pos++) {
        switch (img[pos]) {
            case BLACK:
                putchar(' ');
                break;
            case WHITE:
                putchar('*');
                break;
            default:
                return 1;
        }
        if ((pos + 1) % NCOL == 0) {
            putchar('\n');
        }
    }
    return 0;
}

int main(void) {
    int res = 0;

    char *img = read_image();
    if (!img) {
        printf("error reading image\n");
        return 1;
    }

    char *output = decode_img(img);
    if (!output) {
        printf("error decoding image\n");
        free(img);
        return 1;
    }

    res = print_img(output);
    if (res != 0) {
        printf("error printing image\n");
    }

    free(output);
    free(img);
    return res;
}
