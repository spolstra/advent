#include <stdio.h>
#include <stdlib.h>

#define BUFSIZE 255

int main(void) {
    char buf[BUFSIZE];
    int total_fuel = 0;

    while (fgets(buf, BUFSIZE, stdin)) {
        char *end;
        int weight = strtol(buf, &end, 10);
        if (*end != '\n') {
            return 1;
        }
        total_fuel += (weight / 3) - 2;
    }
    printf("%d\n", total_fuel);
    return 0;
}

