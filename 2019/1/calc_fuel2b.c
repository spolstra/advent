#include <stdio.h>
#include <stdlib.h>

#define BUFSIZE 255


#define DEFAULT_SIZE 10

/* simple int list to store the module fuel requirements. */
struct list {
    size_t size;
    size_t capacity;
    int *data;
};

struct list *init_list(void) {
    struct list *l = malloc(sizeof(struct list));
    if (!l) {
        return NULL;
    }
    l->data = malloc(DEFAULT_SIZE * sizeof(int));
    if (!l->data) {
        free(l);
        return NULL;
    }
    l->size = 0;
    l->capacity = DEFAULT_SIZE;
    return l;
}

int push_list(struct list *l, int i) {
    if (l->size == l->capacity) {
        int *tmp_data = realloc(l->data, l->capacity * 2 * sizeof(int));
        if (!tmp_data) {
            return 1;
        }
        l->data = tmp_data;
        l->capacity *= 2;
    }
    l->data[l->size] = i;
    l->size += 1;
    return 0;
}


int main(void) {
    char buf[BUFSIZE];
    int total_fuel = 0;

    struct list *l = init_list();
    if (!l) { return 1; }

    while (fgets(buf, BUFSIZE, stdin)) {
        /* Read module weight */
        char *end;
        int weight = strtol(buf, &end, 10);
        if (*end != '\n') {
            return 1;
        }
        
        /* Calculate fuel needed to launch that weight. repeat until <= 0 */
        int module_fuel = 0;
        do {
            weight = (weight / 3) - 2;
            module_fuel += weight > 0 ? weight : 0;
        } while (weight > 0);
        push_list(l, module_fuel);
    }

    /* Sum module fuel requirements. */
    for (size_t i = 0; i < l->size; i++) {
        total_fuel += l->data[i];
    }

    printf("%d\n", total_fuel);
    return 0;
}

