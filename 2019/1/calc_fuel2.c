#include <stdio.h>
#include <stdlib.h>

#define BUFSIZE 255

int main(void) {
    char buf[BUFSIZE];
    int total_fuel = 0;

    while (fgets(buf, BUFSIZE, stdin)) {
        /* Read module weight */
        char *end;
        int weight = strtol(buf, &end, 10);
        if (*end != '\n') {
            return 1;
        }
        
        /* Calculate fuel needed to launch that weight. repeat until <= 0 */
        do {
            weight = (weight / 3) - 2;
            total_fuel += weight > 0 ? weight : 0;
        } while (weight > 0);
    }

    printf("%d\n", total_fuel);
    return 0;
}

