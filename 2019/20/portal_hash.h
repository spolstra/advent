#include <glib.h>
#include "xys.h"

/* map of portal label to coordinate pair */

#ifndef PORTAL_HASH_H
#define PORTAL_HASH_H

struct xy *portal_lookup(GHashTable *h, char *portal);

int portal_insert(GHashTable *h, char *portal, struct xy loc);

GHashTable *portal_hash_init(void);

void portal_hash_destroy(GHashTable *h);

#endif // PORTAL_HASH_H
