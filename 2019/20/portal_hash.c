#include <glib.h>
#include <gmodule.h>
#include <stdio.h>
#include <stdlib.h>

#include "portal_hash.h"

/* return an array with the two locations of the portal */
struct xy *portal_lookup(GHashTable *h, char *portal) {
    struct xy *xys = g_hash_table_lookup(h, portal);
    return xys;
}

/* insert a location for the portal.
 * return 0 if succesful, 1 if portal already has two locations. */
int portal_insert(GHashTable *h, char *portal, struct xy loc) {
    struct xy *xys = portal_lookup(h, portal);
    if (!xys) {
        /* first location */
        struct xy *xys = calloc(2, sizeof(struct xy));
        xys[0] = loc;
        g_hash_table_insert(h, portal, xys);
        return 0;
    }
    if (xys[1].x == 0 && xys[1].y == 0) {
        /* second location */
        xys[1] = loc;
    }
    return 1; /* error, portal already has two locations */
}

GHashTable *portal_hash_init(void) {
    return g_hash_table_new_full(g_str_hash, g_str_equal, free, free);
}

void portal_hash_destroy(GHashTable *h) {
    g_hash_table_destroy(h);
}

#ifdef TEST_MAIN

// compile with:
// clang -g3 -DTEST_MAIN `pkg-config --cflags glib-2.0`  `pkg-config --libs
glib-2.0` -fsanitize=address statehash.c int main(void) { int val = 10;

    GHashTable *hash = g_hash_table_new_full(state_hash, state_equal,
state_destroy, NULL);

    struct state s = { .keys = 1, .locs = { 2, 2, 2, 2 }, .nlocs = 4 };
    /*
    struct state *s = malloc(sizeof(struct state));
    s->keys = 1;
    for (int i = 0; i < 4; i++) {
        s->locs[i] = 2;
    }
    */
    val = 12;
    printf("insert struct with keys: %d  val: %d\n", s.keys, val);
    state_insert(hash, &s, val);

    printf("lookup of struct with keys %d: %d\n", s.keys, state_lookup(hash, &s));

    // insert second (different) state
    s.keys = 100;
    for (int i = 0; i < 4; i++) {
        s.locs[i] = 2;
    }
    val = 100;

    // insert this second state.
    state_insert(hash, &s, val);

    printf("lookup of second struct with keys %d: %d\n", s.keys, state_lookup(hash, &s));

    // modify struct
    s.locs[0] = 0;

    // lookup modified struct
    printf("lookup of modified struct: %d\n", state_lookup(hash, &s));

    // copy of first struct
    struct state *s2 = malloc(sizeof(struct state));
    s2->keys = 1;
    for (int i = 0; i < 4; i++) {
        s2->locs[i] = 2;
    }
    printf("lookup of original via copy of struct: %d\n", state_lookup(hash, s2));

    *s2 = (struct state){ .keys = 1, .locs = { 2, 2, 2, 2 }, .nlocs = 4 };
    printf("%d\n", state_insert(hash, s2, 13));
    printf("%d\n", state_insert(hash, s2, 13));
    printf("lookup expect 13: %d\n", state_lookup(hash, s2));

    free(s2);

    g_hash_table_destroy(hash);
    return 0;
}
#endif // TEST_MAIN
