/* Simple queue datastructure to store xy position data structure */

/* xy data structures */
struct xy {
    int x;
    int y;
    int dist;
};

struct xys;

/* queue functions */
struct xys *xys_init(void);
void xys_reset(struct xys *xys);
void xys_enque(struct xys *xys, struct xy xy);
struct xy xys_deque(struct xys *xys);
void xys_copy(struct xys *dest, struct xys *src);
int xys_member(struct xys *set, struct xy e);
size_t xys_size(struct xys *xys);
