#include <ctype.h>
#include <glib.h>
#include <stdio.h>
#include <stdlib.h>

#include "xys.h"

enum { WALL = '#', PATH = '.', SPACE = ' ' };

/* map data structure */
#define MAP_SIZE 256
typedef struct map_t {
    int x;
    int y;
    char m[MAP_SIZE][MAP_SIZE];
} map_t;

typedef struct xy portals_t[MAP_SIZE][MAP_SIZE];
/* portal jump coordinates */
portals_t coor2coor;

/* label to coordinate map */
struct xy label2coor['Z' + 1]['Z' + 1];

/* Read map from stdin into map_t */
map_t *readmap(void) {
    map_t *m = malloc(sizeof(map_t));
    if (!m) {
        return NULL;
    }
    m->x = m->y = 0;

    char c = 0;
    int y;
    for (y = 0; c != EOF; y++) {
        int x;
        for (x = 0; (c = getchar()) != '\n' && c != EOF; x++) {
            m->m[x][y] = c;
        }
        m->x = x > m->x ? x : m->x;
    }
    m->y = y - 1;
    return m;
}

/* print map_t m to stdout. */
void printmap(map_t *m) {
    printf("x: %d, y: %d\n", m->x, m->y);
    for (int y = 0; y < m->y; y++) {
        for (int x = 0; x < m->x; x++) {
            putchar(m->m[x][y]);
        }
        putchar('\n');
    }
}

int leftright_label(map_t *m, int x, int y, struct xy *coor, char *label) {
    /* check if there is a letter right of (x,y) */
    if (isupper(m->m[x][y]) && (x + 1 < m->x && isupper(m->m[x + 1][y]))) {
        coor->y = y;
        /* we are on A: no path left? */
        if (x - 1 < 0 || m->m[x - 1][y] != PATH) {
            /* then AB. (set coor to .) */
            coor->x = x + 2;
            label[1] = m->m[x][y];
            label[0] = m->m[x + 1][y];
        } else {
            /* .AB  (set coor to .) */
            coor->x = x - 1;
            label[0] = m->m[x][y];
            label[1] = m->m[x + 1][y];
        }
        return 1;
    }
    return 0;
}

/* return 1 if updown portal is found. */
int updown_label(map_t *m, int x, int y, struct xy *coor, char *label) {
    /* check if there is a letter below (x,y) */
    if (isupper(m->m[x][y]) && (y + 1 < m->y && isupper(m->m[x][y + 1]))) {
        coor->x = x;
        /* we are on A. no path above us? */
        if (y - 1 < 0 || m->m[x][y - 1] != PATH) {
            /* A
             * B
             * .  (set coor to .) */
            coor->y = y + 2;
            label[1] = m->m[x][y];
            label[0] = m->m[x][y + 1];
        } else {
            /* .
             * A
             * B (set coor to . */
            coor->y = y - 1;
            label[0] = m->m[x][y];
            label[1] = m->m[x][y + 1];
        }
        return 1;
    }
    return 0;
}

/* find all portals and store in global portals array */
void locate_portals(map_t *m) {
    struct xy coor = { 0, 0, 0 };
    char label[3] = {'\0'};

    if (!m) {
        return; 
    }
    for (int y = 0; y < m->y; y++) {
        for (int x = 0; x < m->x; x++) {
            if (updown_label(m, x, y, &coor, label)) {
                printf("updown: %s %d %d\n", label, coor.x, coor.y);
            } else if (leftright_label(m, x, y, &coor, label)) {
                printf("leftright: %s %d %d\n", label, coor.x, coor.y);
            }
            if (label2coor[(int)label[0]][(int)label[1]].x == 0) {
                label2coor[(int)label[0]][(int)label[1]] = coor;
            } else {
                label2coor[(int)label[1]][(int)label[0]] = coor;
            }
        }
    }
}

/* print the 2d array that maps labels to coordinates.
 * For label AB, label2coor[A][B] contains the first portal and
 * label2coor[B][A] contains the second coordinate. */
void print_label2coor(void) {
    for (size_t l1 = 'A'; l1 <= 'Z'; l1++) {
        for (size_t l2 = 'A'; l2 <= 'Z'; l2++) {
            if (label2coor[l1][l2].x != 0) {
                printf("%c%c %d %d\n", (char)l1, (char)l2,
                        label2coor[l1][l2].x, label2coor[l1][l2].y);
            }
        }
    }
}

/* fill the coor2coor 2d array that maps portal coordinates to the connected
 * portal. */
void fill_coor2coor(void) {
    for (size_t l1 = 'A'; l1 <= 'Z'; l1++) {
        for (size_t l2 = l1; l2 <= 'Z'; l2++) {
            if (label2coor[l1][l2].x != 0) {
                struct xy p1 = label2coor[l1][l2];
                struct xy p2 = label2coor[l2][l1];
                coor2coor[p1.x][p1.y] = p2;
                coor2coor[p2.x][p2.y] = p1;
            }
        }
    }
}

/* return 1 if p is a valid neighbour, 0 otherwise */
/* valid neigbours are dots (.) */
int is_neighbour(map_t *m, struct xy p) {
    if (m->m[p.x][p.y] == '.') {
        return 1;
    }
    return 0;
}

/* add neighbours of point p to the neighbours list. */
void find_neighbours(map_t *m, struct xy p, struct xys *neighbours) {
    struct xy offset[] = {{.x = 0, .y = -1}, {.x = 0, .y= 1},
                           {.x = -1, .y = 0}, {.x = 1, .y= 0}};
    for (size_t i = 0; i < sizeof(offset) / sizeof(offset[0]); i++) {
        struct xy n = {.x = p.x + offset[i].x,
                       .y = p.y + offset[i].y,
                       .dist = p.dist + 1};
        if (n.x < 0 || n.y < 0) {
            continue;
        }
        if (is_neighbour(m, n)) {
            xys_enque(neighbours, n);
        }
        if (coor2coor[n.x][n.y].x != 0) { // standing on portal
            /* add location of connected portal */
            n = coor2coor[n.x][n.y];
            /* TODO we take two steps here. would be nicer
             * actually step on this coor and jump in the next move. */
            n.dist = p.dist + 2;
            xys_enque(neighbours, n);
        }

    }
}

/* Find shortest path from AA to ZZ.  */
int shortest_path(map_t *m) {
    // locate start pos
    struct xy start = label2coor[(int)'A'][(int)'A'];
    struct xy dest = label2coor[(int)'Z'][(int)'Z'];

    // scratch map nm: because we overwrite visited positions with walls.
    map_t *nm = malloc(sizeof(map_t));
    memcpy(nm, m, sizeof(map_t));

    // front set of our map flood.
    struct xys *front = xys_init();
    struct xys *neighbours = xys_init();
    xys_enque(front, start);

    while (xys_size(front) > 0) {
        // Examine point of the front.
        struct xy cur = xys_deque(front);
        if (cur.x == dest.x && cur.y == dest.y) {
            free(nm);
            free(front);
            free(neighbours);
            return cur.dist; // done: found distance to dest
        }
        nm->m[cur.x][cur.y] = WALL; // So we only visit it once.

        // Add neighbours of cur to front.
        xys_reset(neighbours);
        find_neighbours(nm, cur, neighbours);
        while (xys_size(neighbours) > 0) { // move all neighbours to points
            xys_enque(front, xys_deque(neighbours));
        }
    }
    free(nm);
    free(front);
    free(neighbours);
    return -1;
}

int main(void) {
    map_t *m = readmap();
    printmap(m);

    locate_portals(m);

    // print_label2coor();
    fill_coor2coor();

    int path = shortest_path(m);
    printf("shortest path: %d\n", path);

    free(m);
    return 0;
}
