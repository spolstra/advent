#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdbool.h>

#include "intcode.h"

// status codes
enum {FOUND_WALL = 0, MOVED = 1, OXYGEN = 2};

// movement commands
enum {NO_DIR = 0, NORTH = 1, SOUTH = 2, WEST = 3, EAST = 4};

// map symbols
enum {DROID = 'D', WALL = '#', SPACE = '.', UNKNOWN = ' ', START = 'x',
      OXY_LOC = 'O'};

// search states
enum {WALL_UNKNOWN = 0, WALL_LEFT = 1};

#define MAPSIZE 50
typedef char map_t[MAPSIZE][MAPSIZE];
map_t map;

typedef int map_len_t[MAPSIZE][MAPSIZE];
map_len_t map_len;

/* set coordinates x,y to position that is in direction dir of x, y */
/* return 0 on succes, 1 on failure. */
int dir2pos(int dir, int *x, int *y) {
    switch (dir) {
        case NORTH:
            *y -= 1;
            break;
        case SOUTH:
            *y += 1;
            break;
        case WEST:
            *x -= 1;
            break;
        case EAST:
            *x += 1;
            break;
        default:
            return 1;
    }
    if (*x < 0 || *x > MAPSIZE || *y < 0 || *y > MAPSIZE) {
        return 1;
    }
    return 0;
}

/* starting position and current position are not stored in map.
 * we only show them here. */
void show_map(map_t m, int cur_x, int cur_y) {
    for (int y = 0; y < MAPSIZE; y++) {
        for (int x = 0; x < MAPSIZE; x++) {
            if (x == cur_x && y == cur_y) {
                assert(m[x][y] != WALL);
                putchar(DROID);
            } else if (x == MAPSIZE / 2 && y == MAPSIZE / 2) {
                putchar(START);
            } else {
                putchar(m[x][y]);
            }
        }
        putchar('\n');
    }
    printf("x: %d, y: %d len: %d\n", cur_x, cur_y, map_len[cur_x][cur_y]);
}

/* make sure we don't alter the map_state once it is set. */
int set_map(map_t m, int x, int y, int map_state) {
    if (!(m[x][y] == map_state || m[x][y] == UNKNOWN)) {
        return 1;
    }
    m[x][y] = map_state;
    return 0;
}


/* mark the effect of the status command on the map. */
int mark_map(map_t m, int x, int y, int dir, int status) {
    int r = 0;
    switch (status) {
        case FOUND_WALL:
            if (dir2pos(dir, &x, &y) != 0) {
                return 1;
            }
            r = set_map(m, x, y, WALL);
            break;
        case MOVED:
            r = set_map(m, x, y, SPACE);
            break;
        case OXYGEN:
            r = set_map(m, x, y, OXY_LOC);
            break;
        default:
            return 1;
    }
    return r;
}

/* move in direction dir. This updates x and y in caller. */
int move(int dir, int *x, int *y) {
    int prevx = *x;
    int prevy = *y;
    int r = dir2pos(dir, x, y);
    if (map_len[*x][*y] == 0) { // update path len from origin.
        map_len[*x][*y] = map_len[prevx][prevy] + 1;
    }
    return r;
}

/* return left of current direction. */
int left(int dir) {
    switch (dir) {
        case NORTH:
            return WEST;
        case SOUTH:
            return EAST;
        case WEST:
            return SOUTH;
        case EAST:
            return NORTH;
        default:
            return NO_DIR;
    }
}

/* return right of current direction. */
int right(int dir) {
    switch (dir) {
        case NORTH:
            return EAST;
        case SOUTH:
            return WEST;
        case WEST:
            return NORTH;
        case EAST:
            return SOUTH;
        default:
            return NO_DIR;
    }
}

/* try to move in direction dir. update x, y and map.
 * return status of intcode. */
int try_move(struct intcode *inst, map_t m, int dir, int *x, int *y) {
    list_push(inst->input, dir);
    int r = intcode_run(inst);
    assert(r != INST_HALTED);
    int status = list_pop(inst->output);
    switch (status) {
        case MOVED:
        case OXYGEN:
            if (move(dir, x, y) != 0) { // update pos.
                return -1;
            }
            // fall through to update map...
        case FOUND_WALL:
            if (mark_map(m, *x, *y, dir, status) != 0) {
                return -1;
            }
    }
    return status;
}

/* turn left relative to dir and move one step if we can.
 * return status returned by the intcode program or error code -1. */
int try_left_turn(struct intcode *inst, map_t m, int *dir, int *x, int *y) {
    // try to move left
    int myleft = left(*dir);
    int status = try_move(inst, m, myleft, x, y);
    if (status == MOVED) { // we could move so update dir
        *dir = myleft;
    }
    return status;
}

int find_max_len(void) {
    int max = 0;
    for (int y = 0; y < MAPSIZE; y++) {
        for (int x = 0; x < MAPSIZE; x++) {
            if (map_len[x][y] > max) {
                max = map_len[x][y];
            }
        }
    }
    return max;
}

int main(void) {
    int res = 0;
    struct intcode *inst = intcode_init(NULL, 0, NULL, NULL);
    if (!inst) {
        return 1;
    }

    memset(map, UNKNOWN, sizeof(char) * MAPSIZE * MAPSIZE);
    memset(map_len, 0, sizeof(int) * MAPSIZE * MAPSIZE);

    int status;
    int x = MAPSIZE/2;
    int y = MAPSIZE/2;
    int dir = NORTH;
    int search_state =WALL_UNKNOWN;
    show_map(map, x, y);
    bool oxy_found = false;
    int dist_to_oxygen = 0;
    while (1) {
        switch (search_state) { // use state machine to move
            case WALL_UNKNOWN:
                status = try_left_turn(inst, map, &dir, &x, &y);
                if (status == MOVED) {
                    break; // we stay in WALL_UNKNOWN search state
                } else if (status == FOUND_WALL) {
                    search_state = WALL_LEFT;
                    break;
                }  else if (status == OXYGEN) {
                    if (oxy_found) {
                        goto stop_searching;
                    }
                    oxy_found = true;
                    dist_to_oxygen = map_len[x][y];
                    memset(map_len, 0, sizeof(int) * MAPSIZE * MAPSIZE);
                    break; // continue after oxygen
                } else {
                    printf("unexpected status try_left_turn: %d\n", status);
                    res = 1;
                    goto cleanup;
                }
            case WALL_LEFT: // try to walk in direction dir.
                status = try_move(inst, map, dir, &x, &y);
                if (status == FOUND_WALL) {
                    dir = right(dir); // turn right, still WALL_LEFT
                    break;
                } else if (status == MOVED) {
                    search_state = WALL_UNKNOWN; // moved, check for wall again
                    break;
                } else if (status == OXYGEN) {
                    if (oxy_found) {
                        goto stop_searching;
                    }
                    oxy_found = true;
                    dist_to_oxygen = map_len[x][y];
                    memset(map_len, 0, sizeof(int) * MAPSIZE * MAPSIZE);
                    search_state = WALL_UNKNOWN; // moved, check for wall
                    break; // continue after oxygen
                } else {
                    printf("unknown status from try_move %d\n", status);
                    res = 1;
                    goto cleanup;
                }
            default:
                printf("unknown search state\n");
                res = 1;
                goto cleanup;
        }
        if (status == -1) {
            printf("error in trying to move\n");
            res = 1;
            goto cleanup;
        }
        show_map(map, x, y);
    }

stop_searching:
    printf("distance to oxygen: %d\n", dist_to_oxygen);
    printf("max distance from oxygen: %d\n", find_max_len());
cleanup:
    intcode_cleanup(inst);
    return res;
}
