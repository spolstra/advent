#include "program.h"

#include <iostream>

using namespace std;

int main(void) {
    Program p;

    p.load();
    p.dump();
    Program p2 = p; // try copying
    cout << p.run() << endl;

    // or
    // while(p.exec_instr()) {}
    // cout << p.acc << endl;

    cout << "p2" << endl;
    p2.dump();
    cout << p2.run() << endl;

    return 0;
}
