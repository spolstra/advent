#include <iostream>
#include <map>
#include <stack>

#include "program.h"

using namespace std;

const map<string, Program::Type> Program::str2type = { { "nop", NOP },
                                                       { "jmp", JMP },
                                                       { "acc", ACC } };
const map<Program::Type, string> Program::type2str = { { NOP, "nop" },
                                                       { JMP, "jmp" },
                                                       { ACC, "acc" } };

void Program::load(void) {
    string instr;
    int arg;
    while (cin >> instr) {
        cin >> arg;
        instrs.push_back(Instr(str2type.at(instr), arg));
    }
    loaded = true;
}

void Program::dump(void) {
    for (auto const &i : instrs) {
        cout << i << endl;
    }
}

void Program::reset(void) {
    pc = 0;
    acc = 0;
    for (auto &i : instrs) {
        i.visited = false;
    }
}


ostream &operator<<(ostream &os, const Program::Instr &i) {
    os << Program::type2str.at(i.type) << " " << i.arg << " " << i.visited;
    return os;
}

bool Program::visited(int i) {
    if (i < 0 || i >= (int) instrs.size()) {
        return true; // signal illegal instr as visited to stop program
    }
    return instrs[i].visited;
}

bool Program::exec_instr(void) {
    instrs[pc].visited = true;
    cout << "pc: " << pc << " " << instrs[pc] << endl;
    switch (instrs[pc].type) {
        case NOP:
            if (!last_instr() && !visited(pc + 1)) {
                pc++;
            } else {
                return false;
            }
            break;
        case JMP:
            if (!visited(pc + instrs[pc].arg)) {
                pc += instrs[pc].arg;
            } else {
                return false;
            }
            break;
        case ACC:
            acc += instrs[pc].arg;
            if (!last_instr() && !visited(pc + 1)) {
                pc++;
            } else {
                return false;
            }
            break;
    }
    return true;
}

bool Program::last_instr(void) {
    return pc + 1 == instrs.size();
}

int Program::run(void) {
    if (!loaded) {
        throw runtime_error("program not loaded");
    }

    while (exec_instr()) {}
    return acc;
}

