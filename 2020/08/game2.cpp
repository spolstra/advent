#include "program.h"

#include <iostream>

using namespace std;

int main(void) {
    Program p;
    int acc = 0;

    p.load();
    p.dump();
    cout << endl;

    while (true) {
        if (p.is_nop_jmp()) { // if it's a nop or jmp try the swapped instr
            Program alt = p; // on a copy of the current program state
            alt.fix_instr(); // swap instr
            acc = alt.run();
            if (alt.last_instr()) {
                break; // finished correctly
            }
        }
		// If that failed continue executing the unmodified prog step-by-step
        bool running = p.exec_instr();
        if (!running) {
            if (p.last_instr()) {
                acc = p.acc; // finished correctly
                break;
            } else {
                cout << "NOT finished correctly, acc: " << p.acc << endl;
                return 1;
            }
        }
    }
    cout << "finished correctly, acc: " << acc << endl;
    return 0;
}
