#include <vector>
#include <iostream>
#include <map>

using namespace std;

class Program {
    public:
    int acc;
    size_t pc;
    bool loaded = false;

    enum Type {NOP, JMP, ACC};
	static const map<string, Type> str2type;
	static const map<Type, string> type2str;

    Program(void) : acc(0), pc(0) {};
    void load(void);
    void dump(void);
    int run(void);
    void reset(void);
    bool exec_instr(void);
    bool visited(int i);
    bool last_instr(void);
    bool is_nop_jmp(void) {
        return instrs[pc].type == NOP || instrs[pc].type == JMP;
    }
    void fix_instr() {
        if (instrs[pc].type == NOP) {
            instrs[pc].type = JMP;
        } else if (instrs[pc].type == JMP) {
            instrs[pc].type = NOP;
        } else {
            cout << "cannot fix this instruction!" << endl;
        }
    }

    class Instr {
        public:
        Type type;
        int arg;
        bool visited;

        Instr(void) : type(NOP), arg(0), visited(false) {};
        Instr(Type t, int arg) : type(t), arg(arg), visited(false) {};

        friend ostream &operator<<(ostream &os, const Instr &i);
    };

    private:
    vector<Instr> instrs;
};
