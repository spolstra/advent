#include <cstdlib>
#include <iostream>
#include <vector>

#include "numbers.h"

using namespace std;

int main(int argc, char *argv[]) {
    vector<int> starters;
    for (int arg = 1; arg < argc; arg++) {
        starters.push_back((int) strtol(argv[arg], 0, 10));
    }

    Numbers nums(starters);

    // 15a
    // while (nums.count < 2020) {

    // 15b
    while (nums.count < 30000000) {
        nums.next();
    }
    cout << nums.last << endl;

    return 0;
}
