#include <map>
#include <vector>

using namespace std;

class Numbers {
    public:
    Numbers(vector<int> start_nums) {
        last = start_nums.back();
        start_nums.pop_back();
        for (auto num : start_nums) {
            numbers[num] = count++;
        }
    }

    void next(void) {
        auto found = numbers.find(last);
        int result;
        if (found != numbers.end()) {
            result = count - found->second;
        } else {
            result = 0;
        }
        numbers[last] = count++;
        last = result;
    }

    void print_numbers(void) {
        for (const auto &e : numbers) {
            cout << e.first << " : count " << e.second << endl;
        }
    }

    private:
    map<int, int> numbers;

    public:
    int last = 0;
    int count = 1;
};
