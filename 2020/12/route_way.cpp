#include "ship_way.h"
#include <iostream>

using namespace std;


int main(void) {
    ShipW s;
    char command;
    int value;
    while (cin >> command >> value) {
        cout << command << " " << value << endl;
        switch(command) {
            case 'F':
                s.forward(value);
                break;
            case 'L':
                s.left(value);
                break;
            case 'R':
                s.right(value);
                break;
            case 'N':
            case 'E':
            case 'S':
            case 'W':
                s.move(command, value);
                break;
            default:
                throw std::runtime_error {"Unknown command"};
        }
        s.print_pos();
        cout << endl;
    }

    return 0;
}

