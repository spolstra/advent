#include <iostream>

#include "ship_way.h"

using namespace std;

void ShipW::move(char wind_dir, int dist) {
    int direction = conv_dir[wind_dir];
    move(direction, dist);
}

void ShipW::move(int direction, int dist) {
    switch (direction) {
    case 0:
        way_y -= dist;
        break;
    case 90:
        way_x += dist;
        break;
    case 180:
        way_y += dist;
        break;
    case 270:
        way_x -= dist;
        break;
    default:
        throw std::runtime_error{ "Invalid direction" };
    }
}

void ShipW::left(int deg) {
    if (deg < 0 || deg > 270) {
        throw std::runtime_error{ "Invalid turn" };
    }
    right(360 - deg); // left deg == right (360 - deg)
}

void ShipW::right(int deg) {
    if (deg < 0 || deg > 270) {
        throw std::runtime_error{ "Invalid turn" };
    }

    int tmp_x = way_x;
    switch (deg) {
    case 90:
        way_x = -way_y;
        way_y = tmp_x;
        break;
    case 180:
        way_x = -way_x;
        way_y = -way_y;
        break;
    case 270:
        way_x = way_y;
        way_y = -tmp_x;
        break;
    }
}

void ShipW::forward(int dist) {
    x += way_x * dist;
    y += way_y * dist;
}

pair<int, int> ShipW::pos(void) const {
    return make_pair(x, y);
}

void ShipW::print_pos(void) const {
    auto loc = pos();
    cout << "pos: " << loc.first << " " << loc.second << endl;
    cout << "way: " << way_x << " " << way_y << endl;
    cout << abs(loc.first) + abs(loc.second) << endl;
}
