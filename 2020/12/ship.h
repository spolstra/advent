#include <utility>
#include <map>

class Ship {
    public:
    Ship() = default;
    Ship(char wind_dir) : dir(conv_dir[wind_dir]){ };

    void move(char wind_dir, int dist);
    void move(int direction, int dist);
    void left(int deg);
    void right(int deg);
    void forward(int dist);
    std::pair<int, int> pos(void) const;
    void print_pos(void) const;

    /*         N
     *         0
     *         |
     * W 270 ----- 90 E
     *         |
     *        180
     *         S
     */
    private:
    std::map<char, int> conv_dir = {{'N', 0}, {'E', 90}, {'S', 180}, {'W', 270}};
    int dir = 90;
    int x = 0;
    int y = 0;
};

