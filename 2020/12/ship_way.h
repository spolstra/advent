#include <map>
#include <utility>

/* Ship navigation via a waypoint/vector */
class ShipW {
    public:
    ShipW() = default;

    void move(char wind_dir, int dist);
    void move(int direction, int dist);
    void left(int deg);
    void right(int deg);
    void forward(int dist);
    std::pair<int, int> pos(void) const;
    void print_pos(void) const;

    /*         N
     *         0
     *         |
     * W 270 ----- 90 E
     *         |
     *        180
     *         S
     */
    private:
    std::map<char, int> conv_dir = { { 'N', 0 }, { 'E', 90 }, { 'S', 180 }, { 'W', 270 } };
    int x = 0;
    int y = 0;
    int way_x = 10;
    int way_y = -1;
};

