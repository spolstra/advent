#include <iostream>

#include "ship.h"

using namespace std;

void Ship::move(char wind_dir, int dist) {
    int direction = conv_dir[wind_dir];
    move(direction, dist);
}

void Ship::move(int direction, int dist) {
    switch (direction) {
    case 0:
        y -= dist;
        break;
    case 90:
        x += dist;
        break;
    case 180:
        y += dist;
        break;
    case 270:
        x -= dist;
        break;
    default:
        throw std::runtime_error{ "Invalid direction" };
    }
}

void Ship::left(int deg) {
    if (deg < 0 || deg > 270) {
        throw std::runtime_error{ "Invalid turn" };
    }
    right(360 - deg); // left deg == right (360 - deg)
    if (dir < 0 || dir > 270) {
        throw std::runtime_error{ "Invalid dir" };
    }
}

void Ship::right(int deg) {
    if (deg < 0 || deg > 270) {
        throw std::runtime_error{ "Invalid turn" };
    }
    dir = (dir + deg) % 360;
    if (dir < 0 || dir > 270) {
        throw std::runtime_error{ "Invalid dir" };
    }
}

void Ship::forward(int dist) {
    move(dir, dist);
}

pair<int, int> Ship::pos(void) const {
    return make_pair(x, y);
}

void Ship::print_pos(void) const {
    auto loc = pos();
    cout << loc.first << " " << loc.second << endl;
    cout << abs(loc.first) + abs(loc.second) << endl;
}
