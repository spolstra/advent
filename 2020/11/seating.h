#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Seating {
    public:
    static const char TAKEN = '#';
    static const char FREE = 'L';
    static const char FLOOR = '.';
    static const char EDGE = '@';

    Seating(istream &file);
    size_t occupied(void);
    bool next(void);

    friend ostream &operator<<(ostream &os, const Seating &seating);

    protected:
    vector<vector<char>> seats;
    size_t limit;

    private:
    virtual size_t adjacent(size_t x, size_t y);

    size_t nrows;
    size_t ncols;
};
