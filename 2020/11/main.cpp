#include "seating.h"
#include <iostream>

int main(void) {
    Seating s(cin);

    do {
        cout << s;
        cout << endl;
    } while (s.next());

    cout << s.occupied() << endl;

    return 0;
}

