#include "seating2.h"
#include <iostream>

size_t calc(Seating &s) {
    do {
        cout << s;
        cout << endl;
    } while (s.next());

    return s.occupied();
}


int main(void) {
    Seating2 s(cin);

    cout << calc(s) << endl;

    return 0;
}

