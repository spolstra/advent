#include "seating.h"
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Seating2 : public Seating {
    public:
    Seating2(istream &file) : Seating(file) {
        limit = 5;
    };

    private:
    size_t adjacent(size_t x, size_t y);
    bool check_occ(size_t x, size_t y, int x_offset, int y_offset);
};
