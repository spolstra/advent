#include "seating2.h"
#include <iostream>
#include <utility>
#include <vector>

using namespace std;

size_t Seating2::adjacent(size_t x, size_t y) {
    size_t taken = 0;

    /* order of search:
     *   812  |
     *   7x3  y   ---x-->
     *   654  |
     *        V
     */
    vector<pair<int, int>> offsets{ { 0, -1 }, { 1, -1 }, { 1, 0 },  { 1, 1 },
                                    { 0, 1 },  { -1, 1 }, { -1, 0 }, { -1, -1 } };

    for (auto &offset : offsets) {
        if (check_occ(x, y, offset.first, offset.second)) {
            taken++;
        }
    }
    return taken;
}

bool Seating2::check_occ(size_t x, size_t y, int x_offset, int y_offset) {
    size_t i = x;
    size_t j = y;
    do {
        i = static_cast<size_t>(static_cast<int>(i) + x_offset);
        j = static_cast<size_t>(static_cast<int>(j) + y_offset);
    } while (seats[i][j] == FLOOR);

    return seats[i][j] == TAKEN;
}
