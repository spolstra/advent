#include "seating.h"
#include <iostream>
#include <vector>

using namespace std;

const char Seating::TAKEN;
const char Seating::FREE;
const char Seating::FLOOR;
const char Seating::EDGE;

Seating::Seating(istream &file) : limit(4) {
    string line;
    while (getline(file, line)) {
        vector<char> v;
        v.push_back(EDGE);
        for (const char &c : line) {
            v.push_back(c);
        }
        v.push_back(EDGE);
        seats.push_back(v);
    }

    // add rows at beginning and end for padding
    vector<char> pad_row(seats[1].size(), EDGE);
    seats.insert(seats.begin(), pad_row);
    seats.insert(seats.end(), pad_row);

    nrows = seats.size() - 2;
    ncols = seats[1].size() - 2;
}

size_t Seating::occupied(void) {
    size_t taken = 0;
    for (size_t row = 1; row <= nrows; row++) {
        for (size_t col = 1; col <= ncols; col++) {
            if (seats[row][col] == TAKEN) {
                taken++;
            }
        }
    }
    return taken;
}

size_t Seating::adjacent(size_t x, size_t y) {
    size_t taken = 0;
    for (size_t nx = x - 1; nx <= x + 1; nx++) {
        for (size_t ny = y - 1; ny <= y + 1; ny++) {
            if (nx == x && ny == y) {
                continue;
            }
            if (seats[nx][ny] == TAKEN) {
                taken++;
            }
        }
    }
    return taken;
}


bool Seating::next(void) {
    vector<vector<char>> tmp = seats;
    bool changed = false;

    for (size_t row = 1; row <= nrows; row++) {
        for (size_t col = 1; col <= ncols; col++) {
            switch (seats[row][col]) {
            case TAKEN:
                if (adjacent(row, col) >= limit) {
                    tmp[row][col] = FREE;
                    changed = true;
                }
                break;
            case FREE:
                if (adjacent(row, col) == 0) {
                    tmp[row][col] = TAKEN;
                    changed = true;
                }
                break;
            }
        }
    }
    seats = tmp;
    return changed;
}

ostream &operator<<(ostream &os, const Seating &seating) {
    for (size_t row = 1; row <= seating.nrows; row++) {
        for (size_t col = 1; col <= seating.ncols; col++) {
            cout << seating.seats[row][col];
        }
        cout << endl;
    }
    return os;
}

