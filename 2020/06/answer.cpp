#include <iostream>
#include <set>

#include "answer.h"

using namespace std;

void Answer::add(string s) {
    for (const auto &c : s) {
        data.insert(c);
    }
}

void Answer::intersect(string s) {
    if (first) {
        add(s);
		first = false;
        return;
    }

	set<char> result;
	for (const char &c : s) {
		if (data.find(c) != data.end()) {
			result.insert(c);
		}
	}

	data.swap(result);
}

ostream &operator<<(ostream &os, Answer a) {
    for (const char &c : a.data) {
        os << c;
    }
    return os;
}
