#include <set>

using namespace std;

class Answer {
	public:
	Answer(void) : first(true) { }
	void add(string s);
	void intersect(string s);
    int count(void) { return data.size(); }

	friend ostream &operator<<(ostream &os, Answer a);

	private:
	bool first;
	set<char> data;
};
