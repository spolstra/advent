#include <iostream>
#include <string>

#include "answer.h"

using namespace std;

int main(void) {
	string line; 
	int total = 0;

	while (!cin.eof()) {
		Answer a;
		while (getline(cin, line)) {
			if (line.length() == 0) {
				break;
			}
			a.add(line);
		}
		total += a.count();
	}
	cout << "total: " << total << endl;
}	
