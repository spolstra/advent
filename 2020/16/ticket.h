#include <regex>
#include <string>
#include <vector>

#pragma once

using namespace std;

class Ticket {
    public:
    Ticket(string s) {
        string pattern("[[:digit:]]+");
        regex r(pattern);
        for (sregex_iterator it(s.begin(), s.end(), r), end_it; it != end_it;
                it++) {
            numbers.push_back(stoi(it->str()));
        }
    }

    friend ostream &operator<<(ostream &os, const Ticket &t);

    vector<int> numbers;
};

ostream &operator<<(ostream &os, const Ticket &t) {
    for (auto i : t.numbers) {
        os << i << ",";
    }
    os << endl;
    return os;
}
