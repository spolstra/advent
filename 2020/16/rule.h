#include <iostream>
#include <regex>
#include <sstream>
#include <string>
#include <vector>

#include "ticket.h"

#pragma once

using namespace std;

class Rule {
    public:
    Rule(string s) {
        string pattern("[[:digit:]]+");
        regex r(pattern);
        for (sregex_iterator it(s.begin(), s.end(), r), end_it; it != end_it; it++) {
            limits.push_back(stoi(it->str()));
        }
        if (limits.size() != 4) {
            throw runtime_error{ "Failed to read rule" };
        }
    }

    bool valid(int val) const {
        if ((val >= limits[0] && val <= limits[1]) ||
                (val >= limits[2] && val <= limits[3])) {
            return true;
        }
        return false;
    }

    friend ostream &operator<<(ostream &os, const Rule &r);

    private:
    vector<int> limits;
};

ostream &operator<<(ostream &os, const Rule &r) {
    for (auto i : r.limits) {
        os << i << " ";
    }
    os << endl;
    return os;
}
