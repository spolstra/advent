#include <iostream>
#include <string>
#include <vector>

#include "rule.h"
#include "ticket.h"

using namespace std;

vector<Rule> read_rules(void) {
    string line;
    vector<Rule> rules;

    while (true) {
        getline(cin, line);
        if (line.empty()) {
            break;
        }
        rules.push_back(Rule(line));
    }
    return rules;
}

Ticket read_my_ticket(void) {
    string s;
    while (true) {
        getline(cin, s);
        if (s.find("your ticket") != string::npos) {
            break;
        }
    }
    getline(cin, s);
    return Ticket(s);
}

vector<Ticket> read_tickets(void) {
    string s;
    vector<Ticket> tickets;

    while (true) {
        getline(cin, s);
        if (s.find("nearby tickets") != string::npos) {
            break;
        }
    }

    while (true) {
        getline(cin, s);
        if (s.empty()) {
            break;
        }
        tickets.push_back(Ticket(s));
    }

    return tickets;
}

bool check_num(int num, vector<Rule> rules) {
    for (const auto &r : rules) {
        if (r.valid(num)) {
            return true;
        }
    }
    return false;
}

int main(void) {
    vector<Rule> rules = read_rules();

    // debug
    /*
    cout << "printing rules" << endl;
    for (const auto &r : rules) {
        cout << r;
    }
    */

    Ticket my_ticket = read_my_ticket();

    // debug
    /*
    cout << "my ticket" << endl;
    cout << my_ticket;
    */

    vector<Ticket> tickets = read_tickets();

    /*
    cout << "nearby tickets" << endl;
    for (const auto &t : tickets) {
        cout << t;
    }
    */

    int error_rate = 0;
    for (const auto &t : tickets) {
        for (int num : t.numbers) {
            if (check_num(num, rules) == false) {
                error_rate += num;
            }
        }
    }
    cout << error_rate << endl;

    return 0;
}
