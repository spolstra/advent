#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

using namespace std;

vector<int> read_data(void) {
    vector<int> data;
    for (int num; cin >> num;) {
        data.push_back(num);
    }
    return data;
}

/* simple naive and less efficient solution */
pair<int, int> find_sum2(vector<int> data, int x) {
    int n_ops = 0;
    for (auto first = data.begin(); first != data.end(); first++) {
        for (auto second = first + 1; second != data.end(); second++) {
            n_ops++;
            if (*first + *second == x) {
                cout << "number of iterations " << n_ops << endl;
                return make_pair(*first, *second);
            }
        }
    }
    throw std::runtime_error{"No pair found!"};
}

/* sort first so we search more efficiently.
 * compare first with last, last - 1, .. until < 2020 or found
 * then repeat with first + 1 */
pair<int, int> find_sum2_improved(vector<int> data, int x) {
    sort(data.begin(), data.end());
    int n_ops = 0;
    for (auto first = data.begin(); first != data.end(); first++) {
        for (auto second = data.end() - 1; second != first + 1; second--) {
            n_ops++;
            if (*first + *second < x) {
                break;
            } else if (*first + *second == x) {
                cout << "number of iterations " << n_ops << endl;
                return make_pair(*first, *second);
            }
        }
    }
    throw std::runtime_error{"No pair found!"};
}


void print_data(vector<int> data) {
    for (const auto &num: data) {
        cout << num << endl;
    }
}

/* solution: 321 * 1699 = 545379 */
int main(void) {
    /* read data */
    auto data = read_data();

    try {
        /* find two numbers with sum 2020 */
        // auto nums = find_sum2(data, 2020);
        auto nums = find_sum2_improved(data, 2020);
        /* print result */
        cout << nums.first << " * " << nums.second << " = " << nums.first * nums.second << endl;
    } catch (exception &e) {
        cerr << e.what() << endl;
    }

}
