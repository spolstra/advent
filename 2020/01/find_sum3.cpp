#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <tuple>

using namespace std;

vector<int> read_data(void) {
    vector<int> data;
    for (int num; cin >> num;) {
        data.push_back(num);
    }
    return data;
}

/* simple naive solution */
tuple<int, int, int> find_sum3(vector<int> data, int x) {
    int n_ops = 0;
    for (auto first = data.begin(); first != data.end(); first++) {
        for (auto second = first + 1; second != data.end(); second++) {
            for (auto third = second + 1; third != data.end(); third++) {
                n_ops++;
                if (*first + *second + *third == x) {
                    cout << "number of iterations " << n_ops << endl;
                    return make_tuple(*first, *second, *third);
                }
            }
        }
    }
    throw std::runtime_error{"No pair found!"};
}

/* sort first so we search more efficiently.
 * compare first with last, last - 1, .. until < 2020 or found
 * then repeat with first + 1 */
pair<int, int> find_sum2_improved(vector<int> data, int x) {
    sort(data.begin(), data.end());
    int n_ops = 0;
    for (auto first = data.begin(); first != data.end(); first++) {
        for (auto second = data.end() - 1; second != first + 1; second--) {
            n_ops++;
            if (*first + *second < x) {
                break;
            } else if (*first + *second == x) {
                cout << "number of iterations " << n_ops << endl;
                return make_pair(*first, *second);
            }
        }
    }
    throw std::runtime_error{"No pair found!"};
}


void print_data(vector<int> data) {
    for (const auto &num: data) {
        cout << num << endl;
    }
}

/* solution part a: 321 * 1699 = 545379 */
int main(void) {
    /* read data */
    auto data = read_data();

    try {
        /* find three numbers with sum 2020 */
        auto nums = find_sum3(data, 2020);
        /* print result */
        cout << get<0>(nums) << " * " << get<1>(nums) << " * " << \
            get<2>(nums) << " = " << get<0>(nums) * get<1>(nums) * get<2>(nums)  << endl;
    } catch (exception &e) {
        cerr << e.what() << endl;
    }

}
