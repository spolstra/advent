#include <iostream>
#include <map>
#include <set>

#include "bag.h"
#include "colormap.h"
#include "graph.h"

using namespace std;

// Read bag info from stdin. Fill Colormap cmap and Bags bags.
void read_bag_info(Colormap &cmap, vector<Bag> &bags) {
    string line;
    while (getline(cin, line)) {
        Bag b(line);
        cmap.add(b.outerbag);
        bags.push_back(b);
    }
}

// Create a graph from the bags information.
// Edges: src is contained in dest
Graph convert_bags_to_graph(const Colormap &cmap, const vector<Bag> &bags) {
    vector<Edge> edges;
    // Convert bag info to edge vector.
    for (const auto &b : bags) {
        // cout << b;
        int dest = cmap.find(b.outerbag);
        for (const auto &inner : b.innerbags) {
            int src = cmap.find(inner.second);
            edges.push_back({ src, dest, inner.first });
        }
    }
    Graph g(edges, cmap.size());
    return g;
}

// Return a set with all bags that can contain a golden bag.
set<int> find_bags_containing_gold_bag(const Colormap &cmap, const Graph &g) {

    // To start initialize can_contain_gold to gold. Remove it later.
    set<int> can_contain_gold;
    can_contain_gold.insert(cmap.find("shiny gold"));

    size_t prev_size;
    do {
        prev_size = can_contain_gold.size();
        // For all elements in set add all destinations to set.
        for (const auto &src : can_contain_gold) {
            vector<Pair> dests = g.adj[src];
            for (const auto &[dest, w] : dests) {
                can_contain_gold.insert(dest);
            }
        }
    } while (prev_size < can_contain_gold.size()); // Stop if none added.

    can_contain_gold.erase(cmap.find("shiny gold"));
    return can_contain_gold;
}

int main(void) {
    vector<Bag> bags;
    Colormap cmap;

    // Read bag data from stdin
    read_bag_info(cmap, bags);

    // Use graph to record: src is contained in dest
    Graph g = convert_bags_to_graph(cmap, bags);

    // Find bags that can contain a gold bag
    set<int> bags_containing_gold_bag = find_bags_containing_gold_bag(cmap, g);

    // Print bags that can contain gold and its size.
    for (const auto &b : bags_containing_gold_bag) {
        cout << cmap.find(b) << endl;
    }
    cout << endl
         << "Number of bags with gold " << bags_containing_gold_bag.size() << endl;
}
