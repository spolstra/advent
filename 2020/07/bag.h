#include <iostream>
#include <regex>
#include <utility>

using namespace std;

class Bag {
    private:
    static const regex split_inners;
    static const regex split_contain;
    static const regex split_inner;

    public:
    string outerbag;
    vector<pair<int, string>> innerbags;

    Bag(string s);

    friend ostream &operator<<(ostream &os, const Bag &b);
};
