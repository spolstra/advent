#include <cassert>
#include <deque>
#include <iostream>
#include <map>

#include "bag.h"
#include "colormap.h"
#include "graph.h"

using namespace std;

// Read bag info from stdin. Fill Colormap cmap and Bags bags.
void read_bag_info(Colormap &cmap, vector<Bag> &bags) {
    string line;
    while (getline(cin, line)) {
        Bag b(line);
        cmap.add(b.outerbag);
        bags.push_back(b);
    }
}

// Create a graph from the bags information.
// Edges: src bag contains dest bag
Graph convert_bags_to_graph(const Colormap &cmap, const vector<Bag> &bags) {
    vector<Edge> edges;
    // Convert bag info to edge vector.
    for (const auto &b : bags) {
        // cout << b;
        int src = cmap.find(b.outerbag);
        for (const auto &inner : b.innerbags) {
            int dest = cmap.find(inner.second);
            edges.push_back({ src, dest, inner.first });
        }
    }
    Graph g(edges, cmap.size());
    return g;
}

// Return the number of bags that must be inside a golden bag.
size_t find_bags_in_gold_bag(const Colormap &cmap, const Graph &g) {
    size_t total_bags = 0;
    // Initialize queue with golden bag to get started.
    deque<int> q;
    q.push_back(cmap.find("shiny gold"));
    do {
        // remove bag at the front to process it.
        int current = q.front();
        q.pop_front();
        total_bags++; // count it!

        // Count and add all bags that must be in current bag
        vector<Pair> dests = g.adj[current];
        for (const auto &[dest, w] : dests) {
            assert(w > 0);
            // Add the dest bag w times.
            for (int i = 0; i < w; i++) {
                q.push_back(dest);
            }
        }
    } while (!q.empty());

    return total_bags - 1; // we don't count the golden bag
}

int main(void) {
    vector<Bag> bags;
    Colormap cmap;

    // Read bag data from stdin
    read_bag_info(cmap, bags);

    // Use graph to record: src bag contains dest bag
    Graph g = convert_bags_to_graph(cmap, bags);

    // Find all bags that must be inside a golden bag.
    size_t bags_in_gold_bag = find_bags_in_gold_bag(cmap, g);

    cout << "Number of bags in gold bag " << bags_in_gold_bag << endl;
}
