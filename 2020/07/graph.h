#include <vector>

/* Graph implementation with weights:
   https://www.techiedelight.com/graph-implementation-using-stl/
*/

using namespace std;

struct Edge {
    int src;
    int dest;
    int weight;
};

typedef pair<int, int> Pair;

class Graph {
    public:
    vector<vector<Pair>> adj;

    Graph(vector<Edge> const &edges, int N) {
        adj.resize(N);

        for (const Edge &e : edges) {
            adj[e.src].push_back(make_pair(e.dest, e.weight));
        }
    }
};
