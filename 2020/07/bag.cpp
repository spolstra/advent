#include "bag.h"

const regex Bag::split_inners{ R"([\w ]+[,. ]+)" };
const regex Bag::split_contain{ R"((.+) bags contain (.+))" };
const regex Bag::split_inner{ R"((\d+)\s([\w ]+) bag(s)?[., ]+)" };

using namespace std;

Bag::Bag(string s) {
    // Split on: outerbag "contain" innerbags
    smatch matches;
    if (!regex_match(s, matches, split_contain)) {
        throw runtime_error{ "Bag parse error!" };
    }
    outerbag = matches[1];

    // split innerbags
    string rest = matches[2];
    sregex_token_iterator end{};
    for (sregex_token_iterator p{ rest.begin(), rest.end(), split_inners }; p != end; ++p) {
        smatch inner_matches;
        string s = *p;
        if (s.find("no other bags.") != string::npos) {
            return;
        }

        // split innerbag into count and name
        if (!regex_match(s, inner_matches, split_inner)) {
            throw runtime_error{ "Innerbag parse error!" };
        }
        int count = stoi(inner_matches[1]);
        string outerbag = inner_matches[2];
        innerbags.push_back(make_pair(count, outerbag));
    }
}

ostream &operator<<(ostream &os, const Bag &b) {
    os << b.outerbag << endl;
    for (const auto &ib : b.innerbags) {
        os << ib.first << " : " << ib.second << endl;
    }
    return os;
}
