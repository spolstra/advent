using namespace std;

class Colormap {
    private:
    map<string, int> str2int;
    map<int, string> int2str;
    int next;

    public:
    Colormap() : next(0){};
    void add(string c) {
        if (!str2int.contains(c)) {
            int2str[next] = c;
            str2int[c] = next++;
        }
    }
    int find(string c) const {
        return str2int.at(c);
    }

    string find(int i) const {
        return int2str.at(i);
    }

    int size(void) const {
        return next;
    }

    friend ostream &operator<<(ostream &os, const Colormap &m);
};

ostream &operator<<(ostream &os, const Colormap &m) {
    for (const auto &[key, value] : m.str2int) {
        os << key << " : " << value << endl;
    }

    /* alternative
    for (auto i = m.str2int.begin(); i != m.data.end(); i++) {
        os << i->first << " : " << i->second << endl;
    }
    */
    return os;
}
