#include <iostream>
#include <regex>
#include <utility>
#include <vector>

using namespace std;

regex pat(R"((\d+)-(\d+) (\w): (\w+))");

/* part 1 */
class PassEntry {
    protected:
    pair<int, int> range;
    char letter;
    string passwd;
    bool is_valid;

    public:
    PassEntry(void){};
    PassEntry(pair<int, int> r, char l, string e)
    : range{ r }, letter{ l }, passwd{ e } {
    }
    PassEntry(int first, int second, char l, string e)
    : PassEntry(make_pair(first, second), l, e) {
    }
    virtual ~PassEntry() = default;

    bool parse_ok(void) const;
    virtual bool passwd_ok(void) const;

    friend ostream &operator<<(std::ostream &os, const PassEntry &e);
    friend istream &operator>>(std::istream &is, PassEntry &e);
};

bool PassEntry::parse_ok(void) const {
    return is_valid;
}

bool PassEntry::passwd_ok(void) const {
    if (!is_valid) {
        return false;
    }
    int count = std::count(passwd.begin(), passwd.end(), letter);
    return count >= range.first && count <= range.second;
}

ostream &operator<<(ostream &os, const PassEntry &e) {
    os << e.range.first << "-" << e.range.second << " " << e.letter << ": "
       << e.passwd << endl;
    return os;
}

istream &operator>>(std::istream &is, PassEntry &e) {
    string line;
    getline(is, line);

    smatch m;
    regex_match(line, m, pat);
    if (m.size() != 5) {
        e.is_valid = false;
        return is;
    }
    e.range = make_pair(stoi(m[1]), stoi(m[2]));
    e.letter = m[3].str()[0];
    e.passwd = m[4].str();
    e.is_valid = true;
    return is;
}

/* part 2 */
class PassEntry2 : public PassEntry {
    public:
    virtual bool passwd_ok(void) const override;
};

bool PassEntry2::passwd_ok(void) const {
    if (!is_valid) {
        return false;
    }
    return (passwd[range.first - 1] == letter && passwd[range.second - 1] != letter) ||
           (passwd[range.second - 1] == letter && passwd[range.first - 1] != letter);
}


// example input:
// 15-16 m: mhmjmzrmmlmmmmmm
int main(void) {

    PassEntry2 entry;
    int correct_count = 0;
    while (cin >> entry && entry.parse_ok()) {
        cout << entry;
        if (entry.passwd_ok()) {
            correct_count++;
        }
    }

    cout << "Number of correct passwords: " << correct_count << endl;
}
