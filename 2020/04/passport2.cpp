#include <iostream>
#include <map>
#include <regex>
#include <string>

#include "passport2.h"

using namespace std;

bool Passport2::valid(void) {
	// Do basic checking of Passport first
	if (Passport::valid() == false) {
		return false;
	}

	// Now do data validation

	// check byr
	int byr = stoi(data["byr"]);
	if (byr < 1920 || byr > 2002) {
		return false;
	}

	// check iyr
	int iyr = stoi(data["iyr"]);
	if (iyr < 2010 || iyr > 2020) {
		return false;
	}

	// check eyr
	int eyr = stoi(data["eyr"]);
	if (eyr < 2020 || eyr > 2030) {
		return false;
	}

	// check hgt
	string hgt = data["hgt"];
	if (hgt.find("in") != string::npos) {
		hgt.erase(hgt.end() - 2, hgt.end());
		int hgt_i = stoi(hgt);
		if (hgt_i < 59 || hgt_i > 76) {
			return false;
		}
	} else if (hgt.find("cm") != string::npos) {
		hgt.erase(hgt.end() - 2, hgt.end());
		int hgt_i = stoi(hgt);
		if (hgt_i < 150 || hgt_i > 193) {
			return false;
		}
	} else {
		return false;
	}

	if (!valid_hcl()) {
		return false;
	}

	if (!valid_ecl()) {
		return false;
	}

	if (!valid_pid()) {
		return false;
	}

    return true;
}

bool Passport2::valid_hcl() {
	return regex_match(data["hcl"], hcl_pat);
}

bool Passport2::valid_ecl() {
	if (eye_colors.find(data["ecl"]) != eye_colors.end()) {
		return true;
	}
	return false;
}

bool Passport2::valid_pid() {
	return regex_match(data["pid"], pid_pat);
}
