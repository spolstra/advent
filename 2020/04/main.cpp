#include <iostream>

#include "passport.h"

using namespace std;

int main() {
    int count = 0;

    while (cin) {
        Passport p(cin);
        // cout << p;
        if (p.valid()) {
            count++;
        }
    }

    cout << count << endl;
}
