#include <iostream>

#include "passport2.h"

using namespace std;

int main() {
    int count = 0;

    while (cin) {
        Passport2 p(cin);
        // cout << p;
        if (p.valid()) {
            count++;
        }
    }

    cout << count << endl;
}
