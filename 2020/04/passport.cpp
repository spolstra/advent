#include <iostream>
#include <map>
#include <regex>
#include <string>

#include "passport.h"

using namespace std;

Passport::Passport(istream &file) {
    string line;
    while (getline(file, line)) {
        if (line.length() == 0) {
            break;
        }
        parse_and_add(line);
    }
}

ostream &operator<<(ostream &os, const Passport &p) {
    for (const auto &[key, value] : p.data) {
        os << key << ":" << value << endl;
    }
    return os;
}

void Passport::parse_and_add(string &l) {
    regex pat{ R"(([#\w]+))" };
    // regex pat{ R"((\S+))" }; // does not work?
    for (sregex_iterator p(l.begin(), l.end(), pat);
            p != sregex_iterator{};
            p++) {
        auto key = (*p++)[1];
        data[key] = (*p)[1];
    }
}

bool Passport::valid(void) {
    if (data.size() == 8) {
        return true;
    } else if (data.size() == 7 && !data.contains("cid")) {
        return true; // ok if we only miss cid
    }
    return false;
}

