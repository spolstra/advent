#include <iostream>
#include <map>
#include <set>
#include <regex>
#include <string>

#include "passport.h"

using namespace std;

class Passport2 : public Passport {
    const regex hcl_pat{ "#[0-9a-z]{6}" };
    const regex pid_pat{ R"(\d{9})" };
    const set<string> eye_colors{ "amb", "blu", "brn", "gry", "grn",
		"hzl", "oth" };
    public:
    Passport2(istream &file) : Passport(file) {
    }
    virtual ~Passport2() = default;
    bool valid_hcl();
    bool valid_ecl();
    bool valid_pid();
    virtual bool valid(void) override;
};

