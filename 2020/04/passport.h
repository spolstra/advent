#include <iostream>
#include <map>
#include <string>

using namespace std;

class Passport {
    public:
    Passport(istream &file);
    virtual bool valid(void);
	virtual ~Passport() = default;

    protected:
    map<string, string> data;
    void parse_and_add(string &line);

    friend ostream &operator<<(ostream &os, const Passport &p);
};


