#include <iostream>
#include <vector>

using namespace std;

class Numbers {
    public:
    Numbers(vector<int> input, int n_) : nums(input), n(n_), start(0), end(n) {
        if (n >= nums.size()) {
            throw runtime_error("preamble size larger than input");
        }
    }

    bool check(void);
    find_range(int target);

    bool next(void) {
        if (end + 1 >= nums.size()) { // end already last valid number
            return false;
        }
        start++;
        end++;
        return true;
    }

    size_t value(void) {
        return nums[end];
    }

    friend ostream &operator<<(ostream &os, const Numbers &nums);

    private:
    vector<int> nums;
    size_t n;
    size_t start;
    size_t end;
};
