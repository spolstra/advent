#include <iostream>
#include <limits>

#include "numbers.h"

using namespace std;

bool Numbers::check(void) {
    if (end >= nums.size()) {
        return false;
    }
    for (size_t i = start; i < end; i++) {
        for (size_t j = i + 1; j < end; j++) {
            if (nums[i] + nums[j] == nums[end]) {
                return true;
            }
        }
    }
    return false;
}

int Numbers::find_range(int target) {
    int sum, min_val, max_val;
    for (size_t i = 0; i < end; i++) {
        sum = 0;
        min_val = numeric_limits<int>::max();
        max_val = numeric_limits<int>::min();
        for (size_t j = i; j < end; j++) {
            sum += nums[j];
            min_val = min(min_val, nums[j]);
            max_val = max(max_val, nums[j]);
            if (sum > target) {
                break;
            }
            if (sum == target) {
                return min_val + max_val;
            }
        }
    }
    return false;
}

ostream &operator<<(ostream &os, const Numbers &ns) {
    os << "nums: ";
    for (auto const &n : ns.nums) {
        cout << n << " ";
    }
    cout << endl;
    os << "n: " << ns.n << " start: " << ns.start << " end: " << ns.end << endl;
    return os;
}
