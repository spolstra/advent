#include <iostream>
#include <vector>

#include "numbers.h"

using namespace std;

int main(int argc, char **argv) {
    vector<int> input;
    int input_num;
    int range = 25;
    if (argc == 2) {
        range = atoi(argv[1]);
    }

    while (cin >> input_num) {
        input.push_back(input_num);
    }

    Numbers nums(input, range);

    do {
        if (!nums.check()) {
            cout << "checksum error: " << nums.value() << endl;
            break;
        }
    } while (nums.next());

    cout << "min + max: " << nums.find_range(nums.value()) << endl;

    return 0;
}

