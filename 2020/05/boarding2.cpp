#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

#include "seat.h"

using namespace std;

int main(void) {
    string line;
    vector<Seat> seats;

    while (getline(cin, line)) {
        seats.push_back(Seat(line));
    }

    sort(seats.begin(), seats.end());

    int prev_id = -1;
    for (auto const &s : seats) {
        if (prev_id > 0 && s.id() != prev_id + 1) {
            cout << "seat id is: " << s.id() - 1 << endl;
            break;
        }
        prev_id = s.id();
    }
}

