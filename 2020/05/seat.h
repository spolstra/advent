#include <string>

class Seat {
    public:
    Seat(std::string s);
	bool operator<(const Seat &other);
	int tid() const { return seatid; }	

	private:
	std::string s;
    int row;
    int column;
    int seatid;

	friend std::ostream &operator<<(std::ostream &os, const Seat &s);
};


