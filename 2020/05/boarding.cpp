#include <iostream>
#include <string>

#include "seat.h"

using namespace std;

int main(void) {
    string line;
    int max_seatid = 0;
    while (getline(cin, line)) {
        Seat s(line);
        if (s.id() > max_seatid) {
            max_seatid = s.id();
        }
    }
    cout << max_seatid << endl;
}

