#include <iostream>
#include <string>

#include "seat.h"

using namespace std;

Seat::Seat(string s_) : s(s_), row(0), column(0) {
    int v = 1;
    for (auto c = s.crbegin() + 3; c != s.crend(); ++c) {
        if (*c == 'B') {
            row += v;
        }
        v *= 2;
    }

    v = 1;
    for (auto c = s.crbegin(); c != s.crbegin() + 3; ++c) {
        if (*c == 'R') {
            column += v;
        }
        v *= 2;
    }

    seatid = row * 8 + column;
}

ostream &operator<<(ostream &os, const Seat &s) {
    os << s.seatid << endl;
    return os;
}

bool Seat::operator<(const Seat &other) {
    return seatid < other.id();
}
