#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Map {
    vector<string> map;
    int x_size;
    int y_size;
    public:
    Map(istream &file);
    int x() const { return x_size; }
    int y() const { return y_size; }
	int trees(int step_x, int step_y) const;
    char at(int x, int y) const {
        return map[y].at(x);
    }

    friend ostream &operator<<(ostream &os, const Map &m);
};


