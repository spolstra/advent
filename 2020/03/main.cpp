#include <iostream>
#include <utility>

#include "map.h"

using namespace std;

int main(void) {
    Map m(cin);

    // cout << m;
    cout << "x: " << m.x() << " y: " << m.y() << endl;


    /*  Right 1, down 1.
        Right 3, down 1. (This is the slope you already checked.)
        Right 5, down 1.
        Right 7, down 1.
        Right 1, down 2.
        */

    vector<pair<int, int>> slopes = { { 1, 1 }, { 3, 1 }, { 5, 1 }, { 7, 1 }, { 1, 2 } };

    long int trees = 1;
    for (const auto &slope : slopes) {
        int t = m.trees(slope.first, slope.second);
        cout << slope.first << " " << slope.second << " " << t << endl;
        trees = trees * t;
    }
    cout << "trees: " << trees << endl;
}

