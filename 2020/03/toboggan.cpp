#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Map {
    vector<string> map;
    int x_size;
    int y_size;
    public:
    Map(istream &file);
    int x() const { return x_size; }
    int y() const { return y_size; }
    char at(int x, int y) const {
        return map[y].at(x);
    }

    friend ostream &operator<<(ostream &os, const Map &m);
};

Map::Map(istream &file) {
    string line;
    y_size = 0;
    while (getline(file, line)) {
        map.push_back(line);
        ++y_size;
        x_size = line.length();
    }
}
    
ostream &operator<<(ostream &os, const Map &m) {
    for (const auto &line: m.map) {
        os << line << endl;
    }
    return os;
}


int main(void) {
    Map m(cin);

    // cout << m;
    cout << "x: " << m.x() << " y: " << m.y() << endl;

    int x {0};
    int step {3};
    int tree_count {0};
    for (int y = 0; y < m.y(); ++y) {
        char loc = m.at(x, y);
        if (loc == '#') {
            ++tree_count;
        }
        cout << x << " " << y << " " << loc << endl;
        x = (x + step) % m.x();
    }
    cout << "trees: " << tree_count << endl;
}

