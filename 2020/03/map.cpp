#include <iostream>
#include <vector>
#include <string>

#include "map.h"

Map::Map(istream &file) {
    string line;
    y_size = 0;
    while (getline(file, line)) {
        map.push_back(line);
        ++y_size;
        x_size = line.length();
    }
}
    
int Map::trees(int step_x, int step_y) const {
    int x {0};
    int tree_count {0};
    for (int y = 0; y < y_size; y += step_y) {
        char loc = at(x, y);
        if (loc == '#') {
            ++tree_count;
        }
        x = (x + step_x) % x_size;
    }
	return tree_count;
}

ostream &operator<<(ostream &os, const Map &m) {
    for (const auto &line: m.map) {
        os << line << endl;
    }
    return os;
}


