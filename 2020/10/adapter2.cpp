#include <algorithm>
#include <iostream>
#include <map>
#include <utility>
#include <vector>

using namespace std;

map<long, long> cache;

vector<long> read_jolts(void) {
    long jolt;

    vector<long> jolts;

    jolts.push_back(0); // start at 0 (outlet)
    while (cin >> jolt) {
        jolts.push_back(jolt);
    }
    return jolts;
}

long choices(vector<long> &jolts, vector<long>::const_iterator cur) {
    if (cur == jolts.cend()) {
        return 0;
    }

    // Check if we already calculated the number of choices for *cur
    if (cache.contains(*cur)) {
        return cache[*cur];
    }

    long n_choices = 0;
    for (auto succs = cur + 1; succs != jolts.cend() && *succs - *cur <= 3; ++succs) {
        n_choices += choices(jolts, succs);
    }
    if (n_choices == 0) {
        return 1; // for last element
    }

    cache[*cur] = n_choices; // store our calculated n_choices in the cache
    return n_choices;
}

void print(vector<long> v) {
    cout << "------------------------------------------------------" << endl;
    for (auto &i : v) {
        cout << i << endl;
    }
    cout << "------------------------------------------------------" << endl;
}

long choices_dyn_prog(const vector<long> &jolts) {
    // Need a vector to store the number of choices for every element in jolts.
    vector<long> choices;
    choices.resize(jolts.size());
    fill(choices.begin(), choices.end(), 0);
    choices[jolts.size() - 1] = 1;  // Last jolt has only one choice.

    // Calculate choices from the last jolt to the first.
    for (size_t i = jolts.size() - 2; i-- > 0; ) { // note: i ranges from [size - 2, ... , 0]
        long choice_i = 0;
        for (size_t suc = i + 1;
                suc < jolts.size() && jolts[suc] - jolts[i] <= 3; suc++) {
             // the number of choices of i is the sum of the choices of its successors.
            choice_i += choices[suc];
        }
        choices[i] = choice_i;
        // print(choices);
    }

    return choices[0];
}

int main(void) {
    // read
    vector<long> jolts = read_jolts();

    // sort
    sort(jolts.begin(), jolts.end());

    //long n_choices = choices(jolts, jolts.cbegin());
    long n_choices = choices_dyn_prog(jolts);

    cout << "number of choices " << n_choices << endl;

    return 0;
}
