#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

using namespace std;

vector<int> read_jolts(void) {
    int jolt;

    vector<int> jolts;

    while (cin >> jolt) {
        jolts.push_back(jolt);
    }
    return jolts;
}

pair<int, int> count_steps(vector<int> jolts) {
    int prev = 0; // outlet starts at 0 jolts
    int ones = 0;
    int threes = 1; // jolt jump to device is always 3
    for (auto &j : jolts) {
        if (j - prev == 1) {
            ones++;
        } else if (j - prev == 3) {
            threes++;
        }
        prev = j;
    }
    return make_pair(ones, threes);
}

void print_jolts(vector<int> jolts) {
    for (auto &j : jolts) {
        cout << j << endl;
    }
}

int main(void) {
    // read
    vector<int> jolts = read_jolts();

    // sort
    sort(jolts.begin(), jolts.end());

    // count one and three steps
    auto p = count_steps(jolts);

    cout << "ones: " << p.first << ", threes: " << p.second << endl;

    cout << p.first * p.second << endl;
    
    return 0;
}
