#include <cmath>
#include <iostream>
#include <sstream>
#include <utility>
#include <vector>
#include <algorithm>

using namespace std;

/* read arrival time and busline information from input stream is.
 * Return a pair of (arrival_time, businfo vector)
 */
pair<int, vector<int>> read_bus_info(istream &is) {
    int arrival;
    vector<int> busses;
    string line;

    // read arrival time
    getline(is, line);
    arrival = stoi(line);

    // read bus info
    getline(cin, line);
    for (auto &c : line) {
        if (c == ',') {
            c = ' ';
        }
    }
    stringstream ss(line);
    string word;
    while (ss >> word) {
        if (word != "x") {
            int bus = stoi(word);
            busses.push_back(bus);
        }
    }
    return make_pair(arrival, busses);
}

/* return a pair of the bus line and the time to departure of the earliest bus line */
pair<int, int> calc_earliest(int arrival, vector<int> busses) {
    vector<int> times;
    float arrival_f = static_cast<float>(arrival);
    for (const auto &bus : busses) {
        times.push_back(
        static_cast<int>(ceil(arrival_f / static_cast<float>(bus))) * bus - arrival);
    }

    /* for (const auto &t : times) {
        cout << t << endl;
    }*/
    std::vector<int>::iterator result = min_element(times.begin(), times.end());
    // cout << *result << endl;
    auto index = static_cast<vector<int>::size_type>(distance(times.begin(), result));
    return make_pair(busses[index], *result);
}

int main(void) {
    auto input = read_bus_info(cin);
    cout << input.first << endl;
    for (const auto &bus : input.second) {
        cout << bus << endl;
    }

    auto result = calc_earliest(input.first, input.second);
    cout << endl << result.first << " " << result.second << endl;
    cout << result.first * result.second << endl;
    return 0;
}
