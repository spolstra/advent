#include <algorithm>
#include <cmath>
#include <iostream>
#include <sstream>
#include <utility>
#include <vector>

using namespace std;

/* return a vector of busline, pos pairs. */
vector<pair<int, int>> read_bus_info(istream &is) {
    vector<pair<int, int>> busses;
    string line;

    // read and ignore arrival time
    getline(is, line);

    // read bus info
    getline(cin, line);
    for (auto &c : line) {
        if (c == ',') {
            c = ' ';
        }
    }
    stringstream ss(line);
    string word;
    int count = 0;
    while (ss >> word) {
        if (word != "x") {
            int bus = stoi(word);
            busses.push_back(make_pair(bus, count));
        }
        count++;
    }
    return busses;
}

/* naive version to find the departure time that meets the constraints. */
long long find_timestamp(vector<pair<int, int>> busses) {
    int step = busses[0].first;
    busses.erase(busses.begin());

    bool found = false;
    long long t = 0;
    while (!found) {
        found = true;
        for (const auto &bus : busses) {
            if ((t + bus.second) % bus.first != 0) {
                found = false;
                break;
            }
        }
        if (!found) {
            t += step;
        }
    }
    return t;
}

static bool pair_compare(pair<int, int> a, pair<int, int> b) {
    return a.first < b.first;
}

/* naive version number 2. Find the largest bus line and use that as step. */
long long find_timestamp2(vector<pair<int, int>> &busses) {
    // Find the largest bus line.
    vector<pair<int, int>>::iterator maxpair;
    maxpair = max_element(busses.begin(), busses.end(), pair_compare);
    int step = (*maxpair).first;
    int offset = (*maxpair).second;
    // Adjust timing offset of the other busses.
    for (auto &bus : busses) {
        bus.second -= offset;
    }
    busses.erase(maxpair);

    // Now search with that largest bus line.
    bool found = false;
    long long t = 0;
    while (!found) {
        found = true;
        for (const auto &bus : busses) {
            if ((t + bus.second) % bus.first != 0) {
                found = false;
                break;
            }
        }
        if (!found) {
            t += step;
        }
    }

    return t - offset;
}

/* start at n0 * n1 ... * n_m and count down */
long long find_timestamp3(vector<pair<int, int>> busses) {
    // Find starting value
    long long t = 1;
    for (const auto &b : busses) {
        t *= b.first;
    }
    cout << "starting at: " << t << endl;

    // Find the largest bus line.
    vector<pair<int, int>>::iterator maxpair;
    maxpair = max_element(busses.begin(), busses.end(), pair_compare);
    int step = (*maxpair).first;
    int offset = (*maxpair).second;
    // Adjust timing offset of the other busses.
    for (auto &bus : busses) {
        bus.second -= offset;
    }
    busses.erase(maxpair);


    bool found = false;
    while (!found) {
        found = true;
        for (const auto &bus : busses) {
            if ((t + bus.second) % bus.first != 0) {
                found = false;
                break;
            }
        }
        if (!found) {
            t -= step;
        }
    }
    return t - offset;
}

/* smarter version: increase the step size when a subset of the inputs match the constraints. */
long long find_timestamp4(vector<pair<int, int>> busses) {
    long long t = 0;
    long long start = busses[0].second; // 0 for the first bus
    long long step = busses[0].first; // initial stepsize is busline
    busses.erase(busses.begin());

    while (busses.size() != 0) {
        // find period for next bus
        cout << "start search: " << start << endl;
        cout << "step: " << step << endl;
        cout << "search for current bus: " << busses[0].first << endl << endl;
        for (t = start; (t + busses[0].second) % busses[0].first != 0; t += step) { }
        cout << "found match at " << t << endl;
        step *= busses[0].first; // set new step size
        busses.erase(busses.begin()); // and move to next bus
        start = t;
    }
    return t;
}


int main(void) {
    auto busses = read_bus_info(cin);

    for (const auto &bus : busses) {
        cout << bus.first << " " << bus.second << endl;
    }

    long long t = find_timestamp4(busses);

    for (const auto &bus : busses) {
        cout << bus.first << " " << bus.second << endl;
    }

    cout << t << endl;
    return 0;
}
