#include <iostream>
#include <vector>
#include <memory>
#include <string>

#include "instr.h"

using namespace std;

vector<shared_ptr<InstrFloat>> instrs;

shared_ptr<InstrFloat> read_instr(string &line) {
    shared_ptr<InstrFloat> p;
     if (line.find("mask") != string::npos) {
         p = make_shared<MaskFloat>(line);
     } else if (line.find("mem") != string::npos) {
         p = make_shared<MemFloat>(line);
     } else {
         throw runtime_error{"Unknown instruction"};
     }
    return p;
}

int main(void) {
    string input;
    while (getline(cin, input)) {
        instrs.push_back(read_instr(input));
    }

    State2 s;
    for (auto const &i : instrs) {
        i->execute(s);
    }

    cout << "Sum of values " << s.sum() << endl;

    return 0;
}
