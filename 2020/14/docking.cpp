#include <iostream>
#include <vector>
#include <memory>
#include <string>

#include "instr.h"

using namespace std;

vector<shared_ptr<Instr>> instrs;

shared_ptr<Instr> read_instr(string &line) {
    shared_ptr<Instr> p;
     if (line.find("mask") != string::npos) {
         p = make_shared<Mask>(line);
     } else if (line.find("mem") != string::npos) {
         p = make_shared<Mem>(line);
     } else {
         throw runtime_error{"Unknown instruction"};
     }
    return p;
}

int main(void) {
    string input;
    while (getline(cin, input)) {
        instrs.push_back(read_instr(input));
    }

    State s;
    for (auto const &i : instrs) {
        i->execute(s);
    }

    cout << "Sum of values " << s.sum() << endl;

    return 0;
}
