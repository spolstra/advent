#include <map>
#include <sstream>
#include <string>
#include <bitset>

using namespace std;

/* Holds the current state of the memory and mask */
class State {
   public:
    void store(long long addr, long long value) {
        memory[addr] = (value & set_zero_mask) | set_one_mask;
    }

    void update_mask(long long zm, long long om) {
        set_zero_mask = zm;
        set_one_mask = om;
    }

    long long sum(void) {
        long long total = 0;
        for (auto &p : memory) {
            total += p.second;
        }
        return total;
    }

   private:
    map<long long, long long> memory;
    long long set_zero_mask = 0;
    long long set_one_mask = 0;
};

/* Holds the current state of the memory and mask for 14b (floating X) */
class State2 {
   public:
    void store(long long addr, long long value) { set_value(mask, addr, value); }

    void update_mask(string m) { mask = m; }

    long long sum(void) {
        long long total = 0;
        for (auto &p : memory) {
            total += p.second;
        }
        return total;
    }

   private:
    void handle_xs(string m, long long addr, long long val) {
        size_t index = m.find("X");
        if (index == string::npos) {  // no X's left, set val
            cerr << m << " = " << (stol(m, 0, 2) | addr) << endl;
            long long long_mask = stol(m, 0, 2);
            memory[addr | long_mask] = val;
            return;
        }
        m[index] = '0';
        handle_xs(m, addr, val);
        m[index] = '1';
        handle_xs(m, addr, val);
    }

    void set_value(string m, long long addr, long long val) {
        string mask_copy = m;
        /* We first need to set all 'ones' in the mask in val. */
        for (char &c : mask_copy) {
            if (c == 'X') {
                c = '0';
            }
        }
        long long set_one = stol(mask_copy, 0, 2);
        addr |= set_one; // turn on 'one' bits in mask in addr

        mask_copy = m;
        for (char &c : mask_copy) {
            if (c == 'X') {
                c = '0';
            } else {
                c = '1';
            }
        }
        long long set_zero = stol(mask_copy, 0, 2);
        addr &= set_zero; // turn off X bits in mask in addr

        cerr << "->" << std::bitset<36>((unsigned long long)addr) << " = " << addr << endl;
        // std::format not support by g++ or clang++
        // cerr << "->" << std::format("{:b}", addr) << " = " << addr << endl;

        /* Then we deal with the floating X's in the mask. */
        handle_xs(m, addr, val);
    }

    map<long long, long long> memory;
    string mask;
};

/* Base class for Instr */
class Instr {
   public:
    Instr() = default;
    virtual ~Instr() {}
    virtual void execute(State &s) = 0;
};

/* Represents the mask instruction */
class Mask : public Instr {
   public:
    Mask(string line) : Instr() {
        string mask, mask_copy;
        string _mask, _assign;
        istringstream in_str(line);
        in_str >> _mask >> _assign >> mask;
        mask_copy = mask;
        for (char &c : mask_copy) {
            if (c == 'X') {
                c = '0';
            }
        }
        set_to_one = stol(mask_copy, 0, 2);

        for (char &c : mask) {
            if (c == 'X') {
                c = '1';
            }
        }
        set_to_zero = stol(mask, 0, 2);
    }

    virtual ~Mask() {}

    virtual void execute(State &s) {
        cout << "Mask: " << set_to_one << " " << set_to_zero << endl;
        s.update_mask(set_to_zero, set_to_one);
    }

   private:
    long long set_to_one = 0;
    long long set_to_zero = 0;
};

/* Represents the memory instruction. */
class Mem : public Instr {
   public:
    Mem(string line) : Instr() {
        string addr_str, val_str, _dummy;
        istringstream in_str(line);
        in_str >> addr_str;
        addr_str.erase(0, 4);

        address = stol(addr_str);
        in_str >> _dummy >> value;  // ignore "="
    }

    virtual ~Mem() {}

    virtual void execute(State &s) {
        cout << "Mem: " << address << " " << value << endl;
        s.store(address, value);
    }

   private:
    long long address = 0;
    long long value = 0;
};

/* Base class for Instr Float version */
class InstrFloat {
   public:
    InstrFloat() = default;
    virtual ~InstrFloat() {}
    virtual void execute(State2 &s) = 0;
};

/* Represents the mask instruction with floating X */
class MaskFloat : public InstrFloat {
   public:
    MaskFloat(string line) : InstrFloat() {
        string _mask, _assign;
        istringstream in_str(line);
        in_str >> _mask >> _assign >> mask;
    }

    virtual ~MaskFloat() {}

    virtual void execute(State2 &s) {
        cout << "MaskFloat: " << mask << endl;
        s.update_mask(mask);
    }

   private:
    string mask;
};

/* Represents the memory instruction with floating X. */
class MemFloat : public InstrFloat {
   public:
    MemFloat(string line) : InstrFloat() {
        string addr_str, val_str, _dummy;
        istringstream in_str(line);
        in_str >> addr_str;
        addr_str.erase(0, 4);

        address = stol(addr_str);
        in_str >> _dummy >> value;  // ignore "="
    }

    virtual ~MemFloat() {}

    virtual void execute(State2 &s) {
        cout << "MemFloat: " << address << " " << value << endl;
        s.store(address, value);
    }

   private:
    long long address = 0;
    long long value = 0;
};

