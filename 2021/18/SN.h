#include <iostream>
#include <memory>
#include <utility>

struct SnailNum : std::enable_shared_from_this<SnailNum> {
    virtual ~SnailNum() = default;
    virtual void print(void) const = 0;
    virtual std::pair<int, int> explode(int depth, bool &changed) = 0;
    virtual std::shared_ptr<SnailNum> split(bool &changed) = 0;
    virtual int magnitude(void) = 0;
    virtual std::shared_ptr<SnailNum> add(std::shared_ptr<SnailNum> o) = 0;
    virtual int val(void) const = 0;
    virtual void add_left(int n) = 0;
    virtual void add_right(int n) = 0;
    virtual std::shared_ptr<SnailNum> copy(void) = 0;
};

struct Num : public SnailNum {
    Num(int n) : num(n) {}
    ~Num() { /* std::cout << "~Num" << std::endl; */
    }

    void print(void) const override { std::cout << num; }
    std::pair<int, int> explode(int depth, bool &changed) override;
    std::shared_ptr<SnailNum> split(bool &changed) override;
    int magnitude(void) override { return num; }
    std::shared_ptr<SnailNum> add(std::shared_ptr<SnailNum> o) override;
    int val(void) const override { return num; }
    void add_left(int n) override { num += n; }
    void add_right(int n) override { num += n; }

    std::shared_ptr<SnailNum> copy(void) override {
        return std::make_shared<Num>(num);
    }

    int num;
};

struct NumPair : public SnailNum {
    NumPair(std::shared_ptr<SnailNum> f, std::shared_ptr<SnailNum> s)
        : nums(make_pair(f, s)) {}
    ~NumPair() { /* std::cout << "~NumPair" << std::endl; */
    }

    void print(void) const override {
        std::cout << "[";
        nums.first->print();
        std::cout << ",";
        nums.second->print();
        std::cout << "]";
    }
    std::pair<int, int> explode(int depth, bool &changed) override;
    std::shared_ptr<SnailNum> split(bool &changed) override;
    int magnitude(void) override;
    std::shared_ptr<SnailNum> add(std::shared_ptr<SnailNum> o) override;
    int val(void) const override { return -1; }  // numpair has not real value
    void add_left(int n) override;
    void add_right(int n) override;

    std::shared_ptr<SnailNum> copy(void) override {
        std::shared_ptr<SnailNum> f = nums.first->copy();
        std::shared_ptr<SnailNum> s = nums.second->copy();
        return make_shared<NumPair>(f, s);
    }

    std::pair<std::shared_ptr<SnailNum>, std::shared_ptr<SnailNum>> nums;
};
