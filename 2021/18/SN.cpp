#include <memory>

#include "SN.h"

using namespace std;

pair<int, int> Num::explode(int depth, [[maybe_unused]] bool &changed) {
    if (depth > 5) {
        throw runtime_error("Depth too large in Num");
    }
    return make_pair(-1, -1); // no explosion
}

pair<int, int> NumPair::explode(int depth, bool &changed) {
    if (depth > 4) {
        return make_pair(nums.first->val(), nums.second->val());
    }

    // Recurse lhs
    auto r = nums.first->explode(depth + 1, changed);
    if (r.first != -1 && r.second != -1) {
        // debug
        // cout << "Exploded: ";
        // print();
        // cout << endl;

        nums.first = make_shared<Num>(0);
        changed = true;
    }
    if (r.second != -1) {
        // lhs exploded so can add second with add_left to rhs now!
        nums.second->add_left(r.second);
        r.second = -1; // done with second
        return r;
    }
    if (r.first != -1) { // explosion in lhs subtree, handle it higher up.
        return r;
    }

    if (changed) { // If we exploded in lhs don't continue in rhs.
        return r;
    }

    // Recurse rhs
    r = nums.second->explode(depth + 1, changed);
    if (r.first != -1 && r.second != -1) {
        // debug
        // cout << "Exploded: ";
        // print();
        // cout << endl;

        nums.second = make_shared<Num>(0);
        changed = true;
    }
    if (r.first != -1) {
        // rhs exploded so can add first with add_right to lhs now!
        nums.first->add_right(r.first);
        r.first = -1; // done with first
        return r;
    }
    if (r.second != -1) {// explosion in rhs subtree, handle it higher up.
        return r;
    }

    return make_pair(-1, -1); // No explosion.
}

void NumPair::add_left(int n) {
    nums.first->add_left(n);
}

void NumPair::add_right(int n) {
    nums.second->add_right(n);
}

shared_ptr<SnailNum> Num::split(bool &changed) {
    if (num > 9) {
        // debug
        //cout << "Split: ";
        //print();
        //cout << endl;

        changed = true;
        return make_shared<NumPair>(make_shared<Num>(num / 2),
                make_shared<Num>((num + 1) / 2));
    }
    return shared_from_this();
}

shared_ptr<SnailNum> NumPair::split(bool &changed) {
    auto r = nums.first->split(changed);
    if (r != nums.first) {
        nums.first = r;
        return shared_from_this();
    }

    if (changed) { // Only split once.
        return shared_from_this();
    }

    r = nums.second->split(changed);
    if (r != nums.second) {
        nums.second = r;
        return shared_from_this();
    }
    return shared_from_this();
}

int NumPair::magnitude(void) {
    return nums.first->magnitude() * 3 + nums.second->magnitude() * 2;
}

shared_ptr<SnailNum> Num::add([[maybe_unused]] std::shared_ptr<SnailNum> o) {
    throw runtime_error("Cannot add to a Num");
}

shared_ptr<SnailNum> NumPair::add(std::shared_ptr<SnailNum> o) {
    return make_shared<NumPair>(shared_from_this(), o);
}
