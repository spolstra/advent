#include <iostream>
#include <memory>
#include <string>
#include <utility>

#include "SN.h"

using namespace std;

shared_ptr<SnailNum> parse(void) {
    char c;
    cin >> c;

    if (c == '[') {
        // parse a Snail pair
        shared_ptr<SnailNum> first = parse();

        cin >> c;
        if (c != ',') {
            throw runtime_error("parse error: expected ','");
        }
        shared_ptr<SnailNum> second = parse();

        cin >> c;
        if (c != ']') {
            throw runtime_error("parse error: expected ']'");
        }
        return make_shared<NumPair>(first, second);
    }
    if (isdigit(c)) {
        string num;
        num.push_back(c);
        char next = cin.peek();
        if (isdigit(next)) {  // input are 1 digit, this is to test split
            cin >> c;
            num.push_back(c);
        }
        return make_shared<Num>(stoi(num));
    }
    return 0;  // signal eof or parse error
}

/* Reduce snailnum sn to its canonical form. */
void reduce(shared_ptr<SnailNum> sn) {
    bool changed = true;
    int iteration = 0;
    while (changed) {
        cout << "Current: " << iteration++ << " ";
        sn->print();
        cout << endl;

        changed = false;
        sn->explode(1, changed);
        if (changed) {
            continue;
        }
        sn = sn->split(changed);
    }
}

int main(void) {
    shared_ptr<SnailNum> sn = parse();

    while (!cin.eof()) {
        shared_ptr<SnailNum> sn2 = parse();
        if (!sn2) {
            break;
        }
        sn2->print();
        cout << endl;
        sn = sn->add(sn2);
        reduce(sn);
    }

    cout << endl;
    sn->print();
    cout << endl;
    cout << sn->magnitude() << endl;
}
