#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <utility>

#include "SN.h"

using namespace std;

shared_ptr<SnailNum> parse(void) {
    char c;
    cin >> c;

    if (c == '[') {
        // parse a Snail pair
        shared_ptr<SnailNum> first = parse();

        cin >> c;
        if (c != ',') {
            throw runtime_error("parse error: expected ','");
        }
        shared_ptr<SnailNum> second = parse();

        cin >> c;
        if (c != ']') {
            throw runtime_error("parse error: expected ']'");
        }
        return make_shared<NumPair>(first, second);
    }
    if (isdigit(c)) {
        string num;
        num.push_back(c);
        char next = cin.peek();
        if (isdigit(next)) {  // input are 1 digit, this is to test split
            cin >> c;
            num.push_back(c);
        }
        return make_shared<Num>(stoi(num));
    }
    return 0;  // signal eof or parse error
}

/* Reduce snailnum sn to its canonical form. */
void reduce(shared_ptr<SnailNum> sn) {
    bool changed = true;
    // int iteration = 0;
    while (changed) {
        // debug
        // cout << "Current: " << iteration++ << " ";
        // sn->print();
        // cout << endl;

        changed = false;
        sn->explode(1, changed);
        if (changed) {
            continue;
        }
        sn = sn->split(changed);
    }
}

int main(void) {
    /* Read all nums. */
    vector<shared_ptr<SnailNum>> nums;
    while (!cin.eof()) {
        shared_ptr<SnailNum> num = parse();
        if (!num) {
            break;
        }
        nums.push_back(num);
    }

    int max = 0;
    /* try all combinations of additions */
    for (auto n1 = nums.cbegin(); n1 != nums.cend(); ++n1) {
        for (auto n2 = n1 + 1; n2 != nums.cend(); ++n2) {
            /* Check n1 + n2 */
            shared_ptr<SnailNum> sum = (*n1)->copy()->add((*n2)->copy());
            reduce(sum);
            int magnitude = sum->magnitude();
            if (magnitude > max) {
                max = magnitude;
            }

            /* Check n2 + n1 */
            sum = (*n2)->copy()->add((*n1)->copy());
            reduce(sum);
            magnitude = sum->magnitude();
            if (magnitude > max) {
                max = magnitude;
            }
        }
    }
    cout << "Solution: " << max << endl;
}
