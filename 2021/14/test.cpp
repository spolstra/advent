#include <iostream>
#include <map>

using namespace std;

map<char, int> create_map(void) {
    return map<char, int> {{'a', 1}, {'b', 2}};
}

int main(void) {
    char c1 = 'a';
    char c2 = 'b';

    string s({c1, c2});

    cout << s << endl;

    map<char, int> counts;

    cout << counts[c1]++ << endl;
    cout << counts[c1] << endl;

    map<char, int> m1 = create_map();

    map<char, int> m2 {{'a', 10}, {'b', 20}};


    for (auto &[k, v] : m1) {
        m2[k] += v;
    }

    cout << "m1" << endl;
    for (auto &[k, v] : m1) {
        cout << k << " : " << v << endl;
    }

    cout << "m2" << endl;
    for (auto &[k, v] : m2) {
        cout << k << " : " << v << endl;
    }

    return 0;
}
