#include <algorithm>
#include <cassert>
#include <iostream>
#include <list>
#include <map>
#include <sstream>
#include <string>
#include <utility>

using namespace std;

/* Global result store for memoization. */
map<pair<string, int>, map<char, long>> stored_results;

/* Read input. */
pair<string, map<string, char>> read_input(void) {
    string input, line;
    map<string, char> templates;

    getline(cin, input);
    getline(cin, line);  // Consume empty line.

    while (getline(cin, line)) {
        stringstream line_stream(line);
        string pattern;
        string result;
        line_stream >> pattern;
        line_stream >> result;  // Consume arrow.
        line_stream >> result;
        templates[pattern] = result[0];
    }

    return make_pair(input, templates);
}

/* Debug printing. */
void check_data(const string &input, const map<string, char> &templates) {
    cout << input << endl;
    list<char> l = list(input.cbegin(), input.cend());
    cout << l.front() << endl;
    string s(l.cbegin(), l.cend());
    cout << s << endl;

    for (const auto &[p, res] : templates) {
        cout << p << " --> " << res << endl;
    }
}

map<char, long> expand(string input, int level,
                       const map<string, char> &templates) {
    if (level == 0) { /* at leaf, return map with first char. */
        return map<char, long>{{input[0], 1}};
    }

    /* Check if we have already calculated this before. */
    if (stored_results.contains(make_pair(input, level))) {
        return stored_results[make_pair(input, level)];
    }

    /* Recursively expand input. */
    assert(input.length() == 2);
    char c = templates.at(input);
    map<char, long> l = expand(string({input[0], c}), level - 1, templates);
    map<char, long> r = expand(string({c, input[1]}), level - 1, templates);

    /* combine maps. */
    for (auto &[k, v] : r) {
        l[k] += v;
    }

    /* memoize the calculated result. */
    stored_results[make_pair(input, level)] = l;

    return l;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        return 1;
    }
    int level = atoi(argv[1]);

    string input;
    map<string, char> templates;

    tie(input, templates) = read_input();

    map<char, long> counts;
    /* We only count the first char when recursion finishes, so the last char
     * of the input is not counted. That is why we count it now. */
    counts[input.back()]++;

    /* Call expand all substrings of length two. */
    auto start = input.begin();
    auto end = input.begin();
    end++;
    end++;
    while (true) {
        map<char, long> sub_counts =
            expand(string(start, end), level, templates);
        /* Add to final counts map. */
        for (auto &[k, v] : sub_counts) {
            counts[k] += v;
        }

        if (end == input.end()) {
            break;
        }
        start++;
        end++;
    }

    vector<long> values;
    for (auto &[k, v] : counts) {
        cout << k << " : " << v << endl;
        values.push_back(v);
    }
    sort(values.begin(), values.end());
    cout << "Answer: " << values.back() - values.front() << endl;

    return 0;
}
