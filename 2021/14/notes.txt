
x1:~/projects/advent/2021/14 [master]$ time ./polymer2 26 < example.txt
Answer: 130175202

real	0m7.094s
user	0m7.094s
sys	0m0.000s

>>> (7*2**(40-26))/(60*60.0)
31.857777777777777

32 uur dus for example.txt

input.txt:
x1:~/projects/advent/2021/14 [master]$ time ./polymer2 23 < input.txt
Answer: 22657249

real	0m9.097s
user	0m9.096s
sys	0m0.001s

>>> (10*2**(40-23))/(60*60.0)
364.0888888888889

>>> (10*2**(40-23))/(60*60.0*24)
15.170370370370371

15 dagen voor input.txt met versie 1b7cc60

A constant factor of improvement will not help. Let's say we can improve the runtime to 1 second for (23, input)

It would still take 1.5 days.

Most of the time is spend looking up the template (53%)

Check duplicated work:
cout << level << " : " << input << endl;
x1:~/projects/advent/2021/14 [memoize_14]$ time ./polymer2 18 < input.txt  > log
x1:~/projects/advent/2021/14 [memoize_14]$ wc -l log.sorted log.uniq
4980728 log.sorted
1314 log.uniq

>>> 1314/4980728.0
0.000263816855688566

