#include <iostream>
#include <list>
#include <map>
#include <sstream>
#include <string>
#include <utility>
#include <algorithm>

using namespace std;

pair<list<char>, map<string, char>> read_input(void) {
    string input, line;
    map<string, char> templates;

    getline(cin, input);
    getline(cin, line);  // Consume empty line.

    while (getline(cin, line)) {
        stringstream line_stream(line);
        string pattern;
        string result;
        line_stream >> pattern;
        line_stream >> result;  // Consume arrow.
        line_stream >> result;
        templates[pattern] = result[0];
    }

    list<char> l = list(input.cbegin(), input.cend());
    return make_pair(l, templates);
}

void check_data(const string &input, const map<string, char> &templates) {
    cout << input << endl;
    list<char> l = list(input.cbegin(), input.cend());
    cout << l.front() << endl;
    string s(l.cbegin(), l.cend());
    cout << s << endl;

    for (const auto &[p, res] : templates) {
        cout << p << " --> " << res << endl;
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        return 1;
    }
    int level = atoi(argv[1]);


    list<char> input;
    map<string, char> templates;

    tie(input, templates) = read_input();

    for (int i = 0; i < level; i++) {
        /* Setup iterators to get substring of length 2. */
        auto start = input.begin();
        auto end = input.begin();
        end++;
        end++;

        while (true) {
            // cout << string(start, end) << endl;
            char c = templates[string(start, end)];

            start++;
            input.insert(start, c);

            if (end == input.end()) {
                break;
            }
            end++;
        }
        // cout << string(input.cbegin(), input.cend()) << endl;
    }

    /* Get list of unique characters in input. */
    vector<char> uniq = vector(input.begin(), input.end());
    sort(uniq.begin(), uniq.end());
    auto last = unique(uniq.begin(), uniq.end());
    uniq.erase(last, uniq.end());
    cout << "uniq: " << string(uniq.cbegin(), uniq.cend()) << endl;

    vector<int> counts;
    for (char c : uniq) {
        int cnt = std::count(input.cbegin(), input.cend(), c);
        cout << c << " : " << cnt << endl;
        counts.push_back(cnt);
    }
    sort(counts.begin(), counts.end());
    cout << counts.back() - counts.front() << endl;

    return 0;
}
