#include <iostream>
#include <map>
#include <set>
#include <stack>
#include <string>
#include <vector>

using namespace std;

/* Return a vector with all strings read from cin. */
vector<string> read_input(void) {
    vector<string> result;
    string s;

    while (!cin.eof()) {
        getline(cin, s);
        if (!s.empty()) {
            result.push_back(s);
        }
    }
    return result;
}

/* Calculate the completion score. */
long calc_score(stack<char> &s) {
    map<char, long> points = { { '(', 1 }, { '[', 2 }, { '{', 3 }, { '<', 4 } };

    long score = 0;
    while (!s.empty()) {
        score = score * 5 + points[s.top()];
        s.pop();
    }
    return score;
}

/* Find incomplete chunks and then return the completion score.
 * If the chunk is corrupted or ok return -1 .*/
long complete_chunk(const string &str) {
    stack<char> s;
    set<char> open = { '(', '[', '{', '<' };
    set<char> close = { ')', ']', '}', '>' };
    map<char, char> match = { { '(', ')' }, { '[', ']' }, { '{', '}' }, { '<', '>' } };

    for (char c : str) {
        if (open.contains(c)) {
            s.push(c);
        } else if (close.contains(c)) {
            if (s.empty()) {
                return -1; // signal corrupt
            }
            char expected = match[s.top()];
            if (c != expected) {
                return -1; // signal corrupt;
            }
            s.pop(); // Paren. matches, just pop and continue
        } else {
            throw runtime_error("Unexpected char found");
        }
    }
    if (!s.empty()) { // Found incomplete.
        return calc_score(s);
    }
    return -1; // Everything was fine!
}

int main(void) {
    set<char> close = { ')', ']', '}', '>' };

    /* read input strings. */
    vector<string> input = read_input();

    /* Find the incomplete chunks and store their score. */
    vector<long> scores;
    for (const string &s : input) {
        long score = complete_chunk(s);
        if (score > 0) {
            scores.push_back(score);
        }
    }

    /* debug */
    for (long score : scores) {
        cout << score << endl;
    }

    /* Return median score. */
    sort(scores.begin(), scores.end());
    cout << "Middle score: " << scores[scores.size() / 2] << endl;
    return 0;
}

