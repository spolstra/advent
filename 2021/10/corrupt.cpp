#include <iostream>
#include <stack>
#include <string>
#include <vector>
#include <set>
#include <map>

using namespace std;

/* Return a vector with all strings read from cin. */
vector<string> read_input(void) {
    vector<string> result;
    string s;

    while (!cin.eof()) {
        getline(cin, s);
        if (!s.empty()) {
            result.push_back(s);
        }
    }
    return result;
}

/* Match chunks (parentheses pair).
 * If the wrong closing paren is found return it.
 * Return the closing paren if the stack is empty (no opening paren found)
 * Return 'I' if the string is incomplete.
 * Return 'O' (ok) if all parens are correctly balanced.
 */
char find_illegal(const string &str) {
    stack<char> s;
    set<char> open = {'(', '[', '{', '<'};
    set<char> close = {')', ']', '}', '>'};
    map<char, char> match = {{'(', ')'}, {'[', ']'}, {'{', '}'}, {'<', '>'}};

    for (char c : str) {
        if (open.contains(c)) {
            s.push(c);
        } else if (close.contains(c)) {
            if (s.empty()) {
                return c;
            }
            char expected = match[s.top()];
            if (c != expected) {
                return c;
            }
            s.pop(); // Paren. matches, just pop and continue
        } else {
            throw runtime_error("Unexpected char found");
        }
    }
    if (!s.empty()) { // Not all opened parens are closed.
        return 'I';
    }
    return 'O'; // Everything was fine!
}

int main(void) {
    set<char> close = {')', ']', '}', '>'};
    map<char, int> points = {{')', 3}, {']', 57}, {'}', 1197}, {'>', 25137}};

    /* read input strings. */
    vector<string> input = read_input();

    /* Find illegal closing parens and calculate total score. */
    int total = 0;
    for (const string &s : input) {
        char c = find_illegal(s);
        if (close.contains(c)) {
            total += points[c];
            cout << c << endl;
        }
    }

    cout << "Total: " << total << endl;
    return 0;
}

