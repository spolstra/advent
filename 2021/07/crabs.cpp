#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cstdint>

using namespace std;

vector<int> read_nums(void) {
    string s;
    int n;
    char sep;

    getline(cin, s);
    stringstream ss = stringstream(s);
    vector<int> nums;
    while(ss >> n) {
        nums.push_back(n);
        ss >> sep;
    }
    return nums;
}

void print(vector<int> nums) { 
    for (int n : nums) {
        cout << n << " ";
    }
    cout << endl;
}

// Cost function for 7a is just the identity function.
/*
int cost(int n) {
    return n;
}
*/

// Cost function for 7b S_n: Sum of sequence 1...n => (n + n^2)/2
int cost(int n) {
    return (n + n * n) / 2;
}

/* Implementation of algorithm v2 from boox notes. */
int main(void) {
    vector<int> crabs = read_nums();

    sort(crabs.begin(), crabs.end());

    // subtract current position.
    int best = INT32_MAX;
    for (int pos = crabs.front(); pos <= crabs.back(); ++pos) {
        // subtract current positions from all crab positions.
        vector<int> new_crabs;
        transform(crabs.begin(), crabs.end(), back_inserter(new_crabs),
                [&](int n) -> int {return n - pos;});

        // calculate total fuel
        int total = accumulate(new_crabs.begin(), new_crabs.end(), 0,
                [](int tot, int n) { return tot + cost(abs(n));});
        cout << total << endl;

        // Check if we improved, if not we are done.
        if (total < best) {
            best = total;
        } else {
            cout << "total is: " << best << endl;
            cout << "position is: " << pos << endl;
            break;
        }
    }
    return 0;
}

