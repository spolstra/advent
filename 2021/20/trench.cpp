#include <iostream>
#include <set>
#include <utility>
#include <vector>
#include <tuple>

using namespace std;

vector<bool> get_lookup_table(const string &s) {
    vector<bool> table;

    for (char c : s) {
        if (c == '#') {
            table.push_back(true);
        }
        if (c == '.') {
            table.push_back(false);
        }
    }
    return table;
}

void print_table(const vector<bool> &t) {
    for (bool b : t) {
        if (b) {
            cout << "#";
        } else {
            cout << ".";
        }
    }
    cout << endl;
}

set<pair<int, int>> read_image(void) {
    set<pair<int, int>> s;
    string line;
    int y = 0;

    while (getline(cin, line)) {
        int x = 0;
        for (char c : line) {
            if (c == '#') {
                s.insert(make_pair(x, y));
            }
            ++x;
        }
        ++y;
    }
    return s;
}

void print_image(const set<pair<int, int>> &image) {
    for (const auto &p : image) {
        cout << p.first << "," << p.second << endl;
    }
}

/* Compute index from neighbouring pixels. */
int get_index(const pair<int, int> &p, const set<pair<int, int>> &image) {
    int index = 0;
    for (int y_off = -1; y_off < 2; ++y_off) {
        for (int x_off = -1; x_off < 2; ++x_off) {
            index <<= 1;
            pair<int, int> neighbour = make_pair(p.first + x_off, p.second + y_off);
            if (image.contains(neighbour)) {
                index |= 1;
            }
        }
    }
    return index;
}

/* notes:
 * We only store lit pixels, but we need to examine all pixels in the
 * "bounding box". So we need to calculate the bounding box to handle all
 * points that might have a lit neighbour.
 * Bounding box is (min_x - 1, min_y * - 1) to (max_x + 1, max_y + 1)
 * Or just increase the bounding box with every iteration? That is
 * conservative and could even be quicker. 
 */

/* Get bounding box for an image.
 * Return a bounding box with an empty edge of one
 * as a pair of (upper_left, bottom_right)
 */
pair<pair<int, int>, pair<int, int>> get_bounding_box(const set<pair<int, int>> &image) {
    // debug
    return make_pair(make_pair(-200, -200), make_pair(200, 200));

    pair<int, int> upper_left = *image.cbegin();
    pair<int, int> bottom_right = *image.cbegin();
    for (const auto &p : image) {
        /* Check upper left. */
        if (p.first < upper_left.first) {
            upper_left.first = p.first;
        }
        if (p.second < upper_left.second) {
            upper_left.second = p.second;
        }

        /* Check bottom right. */
        if (p.first > bottom_right.first) {
            bottom_right.first = p.first;
        }
        if (p.second > bottom_right.second) {
            bottom_right.second = p.second;
        }
    }

    /* Add a border of one. */
    --upper_left.first;
    --upper_left.second;
    ++bottom_right.first;
    ++bottom_right.second;
    
    return make_pair(upper_left, bottom_right);
}

/* Enhance image using the algorithm table.
 * Return the enhanced image. */
set<pair<int, int>> enhance(const set<pair<int, int>> &image, const vector<bool> &table,
        pair<pair<int, int>, pair<int, int>> boundingbox) {
    set<pair<int, int>> enhanced;
    pair<int, int> upper_left;
    pair<int, int> bottom_right;
    tie(upper_left, bottom_right) = boundingbox;

    /* Go over all pixels in the bounding box. */
    for (int y = upper_left.second; y <= bottom_right.second; ++y) {
        for (int x = upper_left.first; x <= bottom_right.first; ++x) {
            pair<int, int> p = make_pair(x, y);
            int index = get_index(p, image);
            if (table[index]) {
                enhanced.insert(p);
            }
        }
    }
    return enhanced;
}

/* Print the image as a map. */
void print_image_map(const set<pair<int, int>> &image) {
    pair<int, int> upper_left;
    pair<int, int> bottom_right;
    tie(upper_left, bottom_right) = get_bounding_box(image);


    /* Go over all pixels in the bounding box. */
    for (int y = upper_left.second; y <= bottom_right.second; ++y) {
        for (int x = upper_left.first; x <= bottom_right.first; ++x) {
            pair<int, int> p = make_pair(x, y);
            if (image.contains(p)) {
                cout << "#";
            } else {
                cout << ".";
            }
        }
        cout << endl;
    }
}

int main(void) {
    /* Read lookup table. */
    string line;
    getline(cin, line);
    vector<bool> table = get_lookup_table(line);

    getline(cin, line);  // skip empty line

    /* Read images as a set of (x,y) pairs. */
    set<pair<int, int>> image = read_image();

    /* Debug: check index calc.
    int index = get_index(make_pair(1,1), image);
    cout << index << endl;
    cout << table[index] << endl;
    return 0;
    */

    print_table(table);
    cout << endl;
    print_image_map(image);

    /* Enhance once. */
    image = enhance(image, table, make_pair(make_pair(-200, -200), make_pair(200, 200)));
    print_image_map(image);
    cout << "lit: " << image.size() << endl;

    /* Enhance again. */
    image = enhance(image, table, make_pair(make_pair(-199, -199), make_pair(199, 199)));
    print_image_map(image);
    cout << "lit: " << image.size() << endl;

    return 0;
}
