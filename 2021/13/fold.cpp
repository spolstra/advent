#include <iostream>
#include <sstream>
#include <set>
#include <vector>
#include <string>
#include <regex>

using namespace std;

set<pair<int, int>> read_dots(void) {
    int x;
    int y;
    char comma;
    string line;
    set <pair<int, int>> dots;
    while (true) {
        getline(cin, line);
        if (line.find(",") == string::npos) { // No more dots.
            break;
        }
        stringstream strline(line);
        strline >> x;
        strline >> comma;
        strline >> y;
        dots.insert(make_pair(x, y));
    }
    return dots;
}

vector<pair<char, int>> read_actions(void) {
    vector<pair<char, int>> actions;
    string line;

    /* Grep the x,y and line/column number. */
    regex pattern("fold along ([xy])=([[:digit:]]+)");

    while (getline(cin, line)) {
        smatch results;
        if (regex_search(line, results, pattern)) {
            actions.push_back(make_pair(results.str(1)[0], stoi(results.str(2))));
        }
    }
    return actions;
}

set<pair<int, int>> fold(set<pair<int, int>> dots,
        const vector<pair<char, int>> &actions) {
    set<pair<int, int>> new_dots;
    for (const auto &[axis, n] : actions) {
        new_dots.clear();
        for (auto [x, y] : dots) {
            if (axis == 'x') {
                if (x > n) {
                    x = n - (x - n);
                }
            } else { // axis = y
                if (y > n) {
                    y = n - (y - n);
                }
            }
            new_dots.insert(make_pair(x, y));
        }
        dots = new_dots;
    }
    return new_dots;
}

pair<int, int> max_vals(const set<pair<int, int>> &dots) {
    pair<int, int> max = make_pair(0, 0);

    for (auto d : dots) {
        if ( d.first > max.first) {
            max.first = d.first;
        }
        if (d.second > max.second) {
            max.second = d.second;
        }
    }
    return max;
}

void display_dots(const set<pair<int, int>> &dots) {
    auto max = max_vals(dots);

    for (int y = 0; y <= max.second; y++) {
        for (int x = 0; x <= max.first; x++) {
            if (dots.contains(make_pair(x, y))) {
                cout << "#";
            } else {
                cout << " ";
            }
        }
        cout << endl;
    }
}

int main(void) {
    auto dots = read_dots();
    auto actions = read_actions();

    for (const auto &d : dots) {
        cout << d.first << ", " << d.second << endl;
    }
    cout << endl;

    for (const auto &a : actions) {
        cout << a.first << ", " << a.second << endl;
    }
    cout << endl;

    cout << "Dots: " << dots.size() << endl;
    dots = fold(dots, actions);
    cout << "Dots: " << dots.size() << endl;

    display_dots(dots); /* ARHZPCUH */

    return 0;
}
