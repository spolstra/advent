#include <iostream>

#include "map.h"

using namespace std;

int main(void) {
    Map m(cin);

    for (size_t i = 0; i < 100; ++i) {
        m.step();
    }

    cout << m << endl;
    cout << "flash count: " << m.flash_count << endl;

    return 0;
}
