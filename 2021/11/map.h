#include <iostream>
#include <vector>

class Map {
    int size; // grid is square

    std::vector<std::vector<int>> data;
    void increase_all(void);
    void increase_neighbours(int r, int c);
    bool try_to_flash(void);
    void reset_flashed(void);
    void flash(int r, int c);

    public:
    Map(std::istream &is);
    bool step(void);
    size_t flash_count;

    friend std::ostream &operator<<(std::ostream &os, Map m);
};
