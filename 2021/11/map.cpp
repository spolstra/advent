#include <string>
#include <limits>

#include "map.h"

using namespace std;

Map::Map(istream &is) : size(0), flash_count(0) {
    string s;

    while(!is.eof()) {
        vector<int> row;
        getline(is, s);
        if (s.empty()) {
            break;
        }
        for (char c : s) {
            row.push_back(c - '0');
        }

        data.push_back(row);
        ++size;
    }
}

/* Increase all neighbours of data[r][c] by 1. */
void Map::increase_neighbours(int r, int c) {
    /* Loop over row and column offsets dr and dc. */
    for (int dr = -1; dr < 2; ++dr) {
        for (int dc = -1; dc < 2; ++dc) {
            if (r + dr >= 0 && r + dr < size && \
                    c + dc >= 0 && c + dc < size && \
                    !(dr == 0 && dc == 0)) {
                ++data[r + dr][c + dc];
            }
        }
    }
}

/* Increase level every octopus on the map by 1. */
void Map::increase_all(void) {
    for (vector<int> &row : data) {
        for (int &e : row) {
            ++e;
        }
    }
}

void Map::flash(int r, int c) {
    increase_neighbours(r, c);
    data[r][c] = numeric_limits<int>::min();
    ++flash_count;
}

/* Find first octopus to flash and flash it.  Reset flashed octopus to max
 * negative values so it not flashed again in this step. 
 *
 * Return true if one is found, false otherwise. */
bool Map::try_to_flash(void) {
    for (int r = 0; r < size; ++r) {
        for (int c = 0; c < size; ++c) {
            if (data[r][c] > 9) {
                flash(r, c);
                return true;
            }
        }
    }
    return false;
}

/* Reset all flashed octopuses to 0. */
void Map::reset_flashed(void) {
    for (vector<int> &row : data) {
        for (int &e : row) {
            if (e < 0) {
                e = 0;
            }
        }
    }
}

/* A step consists of:
 * - Increment all
 * - flash until nobody left to flash
 *
 * Return true if all octopuses flashed during the step, otherwise
 * return false.
 */
bool Map::step(void) {
    increase_all();

    size_t prev_flash_count = flash_count;
    bool flashed;
    do {
        flashed = try_to_flash();
    } while (flashed);

    reset_flashed();

    return flash_count == prev_flash_count + size * size;
}

ostream &operator<<(ostream &os, Map m) {
    for (const vector<int> &row : m.data) {
        for (int e : row) {
            os << e;
        }
        os << endl;
    }
    return os;
}
