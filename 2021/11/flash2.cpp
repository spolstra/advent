#include <iostream>

#include "map.h"

using namespace std;

int main(void) {
    Map m(cin);

    size_t steps = 0;
    while (true) {
        ++steps;
        if (m.step()) {
            break;
        }
    }

    cout << "step count: " << steps << endl;

    return 0;
}
