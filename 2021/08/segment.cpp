#include "segment.h"

Seg::Seg(string s) {
    for (char c : s) {
        data.insert(c);
    }
}

Seg::Seg(set<char> s) {
    data = s;
}


/* Use string s to map chars
 * "deafgbc" maps d->a, e->b */
Seg Seg::transform(const string &s) const {
    set<char> new_data;
    for (auto c : data) {
        auto it = find(s.cbegin(), s.cend(), c);
        new_data.insert('a' + (it - s.cbegin()));
    }
    return Seg(new_data);
}

ostream &operator<<(ostream &os, Seg s) {
    for (char c : s.data) {
        os << c;
    }
    return os;
}


bool operator==(const Seg &a, const Seg &b) {
    return a.data == b.data;
}

bool compare(const Seg &a, const Seg &b) {
    return a.data < b.data;
}


