#include <iostream>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include "segment.h"

using namespace std;

/* Initialize numbers with all valid segment configurations */
vector<string> number_strings = {
    "abcefg", // 0
    "cf", // 1
    "acdeg", // 2
    "acdfg", // 3
    "bcdf", // 4
    "abdfg", // 5
    "abdefg", // 6
    "acf", // 7
    "abcdefg", // 8
    "abcdfg" // 9
};

/* Read all lines of segments from stdin.
 * A line consists of 10 patterns and 4 output digits. */
vector<vector<Seg>> read_input(void) {
    vector<vector<Seg>> segment_lines;
    /* For every line.. */
    while (!cin.eof()) {
        /* Read line */
        string s;
        getline(cin, s);
        if (s.length() == 0) { /* just before eof() case */
            break;
        }
        stringstream ss = stringstream(s);
        string elem;

        /* Store segment values in segment_line. */
        vector<Seg> segment_line;
        while (ss >> elem) {
            if (elem == "|") { /* skip separator */
                continue;
            }
            segment_line.push_back(Seg(elem));
        }
        /* And append to segment_lines. */
        segment_lines.push_back(segment_line);
    }
    return segment_lines;
}

/* Find the permutation that 'fixes' the segs into correct numbers.
 * For every permutation:
 *   Test if every segment in segments is a valid number.
 *   If it is that permutation is the correct one.
 */
string find_permutation(vector<Seg> segs, const vector<Seg> &numbers) {
    std::string permutation = "abcdefg";
    /* For every permutation */
    do {
        bool valid = true;
        /* Check every segment. */
        for (const Seg &seg : segs) {
            Seg transformed = seg.transform(permutation);
            /* Check if its a valid number. */
            auto it = find(numbers.begin(), numbers.end(), transformed);
            if (it == numbers.end()) {
                valid = false; /* Stop if it not a valid number. */
                break;
            }
        }
        if (valid) { /* permutation is valid for all segments! */
            return permutation;
        }
    } while (std::next_permutation(permutation.begin(), permutation.end()));

    throw runtime_error("No permutation found");
}

int get_output(vector<Seg> segments, string permutation, const vector<Seg> &numbers) {
    int number = 0;
    /* Convert the last 4 digits to an integer. */
    for (int i = 10; i < 14; ++i) {
        Seg seg = segments[i].transform(permutation);
        auto it = find(numbers.cbegin(), numbers.cend(), seg);
        int digit = distance(numbers.cbegin(), it);
        number = number * 10 + digit;
    }
    return number;
}

int main(void) {
    /* Create a segments vector with all valid numbers.
     * We can also use it to convert seg to digit. */
    vector<Seg> numbers;
    for (auto &num : number_strings) {
        numbers.push_back(Seg(num));
    }

    /* Get input from stdin */
    vector<vector<Seg>> segment_lines = read_input();

    /* Get output numbers and sum them */
    int sum = 0;
    for (auto &segment_line : segment_lines) {
        string permutation = find_permutation(segment_line, numbers);
        cout << permutation << endl;
        int output = get_output(segment_line, permutation, numbers);
        cout << output << endl;
        sum += output;
    }
    cout << "Sum is: " << sum << endl;

    return 0;
}
