#include <iostream>
#include <string>
#include <set>

using namespace std;

class Seg {
    set<char> data;
    public:
    Seg(string s);
    Seg(set<char> s);
    size_t size(void) { return data.size(); }
    Seg transform(const string &s) const;

    friend ostream &operator<<(ostream &os, Seg s);
    friend bool operator==(const Seg &a, const Seg &b);
    friend bool compare(const Seg &a, const Seg &b);
};

extern bool compare(const Seg &a, const Seg &b);
