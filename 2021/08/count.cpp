#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "segment.h"

using namespace std;

int main(void) {
    vector<vector<Seg>> outputs;
    /* For every line.. */
    while (!cin.eof()) {
        /* Read line */
        string s;
        getline(cin, s);
        if (s.length() == 0) { /* just before eof() case */
            break;
        }
        stringstream ss = stringstream(s);
        string elem;

        /* Skip patterns. */
        while (ss >> elem) {
            if (elem == "|") {
                break;
            }
        }

        /* Store output values in output. */
        vector<Seg> output;
        while (ss >> elem) {
            output.push_back(Seg(elem));
        }
        /* And append to outputs. */
        outputs.push_back(output);
    }

    /* Count outputs with numbers: 1, 4, 7, 8
     *                   segments: 2, 4, 3, 7 */
    size_t total = 0;
    set<int> target_segments = {2, 4, 3, 7};
    for (auto &output : outputs) {
        for (auto &segment : output) {
            if (target_segments.contains(segment.size())) {
                ++total;
            }
        }
    }

    cout << "total: " << total << endl;
    return 0;
}
