#include <algorithm>
#include <string>
#include <vector>
#include <iostream>

// simple permute test 
int main()
{
    std::string s = "abcdefg";
    std::sort(s.begin(), s.end());
    do {
        std::cout << s << '\n';
    } while(std::next_permutation(s.begin(), s.end()));
}
