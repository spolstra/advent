#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <algorithm>

using namespace std;

vector<vector<string>>
findpaths(const string &start, vector<string> visited, const map<string, vector<string>>& neighbours) {
    if (start == "end") {
        return vector<vector<string>> {{"end"}};
    }

    vector<string> connected = neighbours.at(start);
    /* Remove nodes we have already visited from connected. */
    for (const string& v : visited) {
        auto it = find(begin(connected), end(connected), v);
        if (it != end(connected)) {
            connected.erase(it);
        }
    }

    if (islower(start[0])) {
        visited.push_back(start);
    }

    vector<vector<string>> paths;
    for (const string& c : connected) {
        auto rest = findpaths(c, visited, neighbours);
        for (auto &p : rest) {
            p.insert(p.begin(), start);
        }
        paths.insert(paths.begin(), rest.begin(), rest.end());
    }

    return paths;
}

void print_paths(const vector<vector<string>> &paths) {
    for (const auto &p : paths) {
        bool first = true;
        for (const string &node : p) {
            if (first) {
                first = false;
            } else {
                cout << "-";
            }
            cout << node;
        }
        cout << endl;
    }
}

int main(void) {
    map<string, vector<string>> neighbours;

    /* Create connection map (node -> neighbours) from input. */
    string line;
    while (getline(cin, line)) {
        stringstream strdata(line);
        string first, second;
        getline(strdata, first, '-');
        getline(strdata, second, '-');
        /* Connect in both directions. */
        neighbours[first].push_back(second);
        neighbours[second].push_back(first);
    }

    // debug print graph connections.
    for (const auto& [src, dests] : neighbours) {
        cout << src << " :";
        for (const auto &dest : dests) {
            cout << " " << dest;
        }
        cout << endl;
    }

    vector<string> visited;
    vector<vector<string>> paths = findpaths("start", visited, neighbours);

    print_paths(paths);
    cout << "Number of paths found: " << paths.size() << endl;

    return 0;
}
