class Die {
    public:
    Die() : rolls(0) { }
    virtual ~Die() { }
    virtual unsigned int roll(void) = 0;

    unsigned int rolls;
};
