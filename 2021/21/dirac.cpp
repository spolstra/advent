#include <iostream>
#include <vector>

#include "determi_die.h"
#include "player.h"

using namespace std;

int main(void) {
    Determ_Die d(10);
    vector<Player> players{ Player(8, 10), Player(4, 10) };

    while (true) {
        int i = 0;
        for (Player &p : players) {
            p.move(d.roll() + d.roll() + d.roll());
            cout << i++ << " p: " << p.position << " s: " << p.score << endl;
            if (p.score >= 1000) {
                goto done;
            }
        }
    }

done:
    unsigned int answer = min(players[0].score, players[1].score) * d.rolls; 
    cout << "answer: " << answer << endl;
    return 0;
}
