#include "die.h"

class Determ_Die : public Die {
    public:
    Determ_Die(unsigned int sides) : sides(sides), current(1) { }
    unsigned int roll(void) {
        unsigned int rolled = current;
        current = (current % sides) + 1;
        ++rolls;
        return rolled;
    }

    private:
    unsigned int sides;
    unsigned int current;
};
