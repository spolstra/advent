class Player {
    public:
    Player(unsigned int start, unsigned int board_size)
    : position(start), board_size(board_size), score(0) {
    }

    unsigned int move(unsigned int steps) {
        position = ((position + steps - 1) % board_size) + 1;
        score += position;
        return score;
    }

    unsigned int position;
    unsigned int board_size;
    unsigned int score;
};
