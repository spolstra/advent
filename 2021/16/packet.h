#include <memory>
#include <vector>

class Packet {
   public:
    Packet(size_t v, size_t t) : version(v), type(t) {};
    virtual ~Packet() = default;
    virtual void print(void) const;
    virtual size_t version_count(void) const = 0;
    virtual size_t eval(void) const = 0;

    size_t version;
    size_t type;
};

class Literal : public Packet {
   public:
    Literal(size_t v, size_t t, size_t l) : Packet(v, t), literal(l){};
    void print(void) const override;
    size_t version_count(void) const override;
    virtual size_t eval(void) const override;

    size_t literal;
};

class Operator : public Packet {
   public:
    Operator(size_t v, size_t t,
             std::vector<std::shared_ptr<Packet>> packets)
        : Packet(v, t), subpackets(packets){};
    std::vector<std::shared_ptr<Packet>> subpackets;
    void print(void) const override;
    size_t version_count(void) const override;
    virtual size_t eval(void) const override;
};
