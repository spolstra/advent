#include <iostream>
#include <sstream>
#include <deque>
#include <memory>

#include "packet.h"

using namespace std;

/* Read hex string into bit queue. */
deque<bool> read_input(void) {
    deque<bool> result;
    string line;
    size_t n;

    getline(cin, line);

    for (char c : line) {
        istringstream(string(1, c)) >> std::hex >> n;
        for (int mask = 8; mask > 0; mask /= 2) {
            result.push_back(n & mask);
        }
    }
    return result;
}

/* Return the integer value of the next n bits. */
size_t get_bits(size_t n, deque<bool> &bits) {
    size_t result = 0;
    while (n > 0) {
        result *= 2;
        if (bits.front()) {
            ++result;
        }

        bits.pop_front();
        --n;
    }
    return result;
}

size_t parse_literal(deque<bool> &bits) {
    size_t prefix;
    size_t literal = 0;

    do {
        prefix = get_bits(1, bits);
        size_t partial = get_bits(4, bits);
        literal = literal * 16 + partial;
    } while (prefix == 1);

    return literal;
}

shared_ptr<Packet> parse(deque<bool> &bits) {
    /* Parse header. */
    size_t version = get_bits(3, bits);
    size_t type = get_bits(3, bits);

    if (type == 4) {
        /* Parse literal. */
        size_t literal = parse_literal(bits);
        return make_shared<Literal>(version, type, literal);
    }

    size_t length_type = get_bits(1, bits);
    vector<shared_ptr<Packet>> packets;

    if (length_type == 0) {
        /* Length in bits. */
        size_t length = get_bits(15, bits);
        size_t sub_start = bits.size();
        while (bits.size() + length > sub_start) {
            packets.push_back(parse(bits));
        }
    } else if (length_type == 1) {
        /* Length in packets. */
        size_t length = get_bits(11, bits);
        for (size_t count = 0; count < length; ++count) {
            packets.push_back(parse(bits));
        }
    } else {
        throw runtime_error("Unknown length type");
    }

    return make_shared<Operator>(version, type, packets);
}

int main(void) {
    deque<bool> bits = read_input();

    shared_ptr<Packet> root = parse(bits);

    root->print();
    cout << endl;

    cout << "version count: " << root->version_count() << endl;
    cout << "value: " << root->eval() << endl;

}
