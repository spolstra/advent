#include "packet.h"

#include <iostream>
#include <limits>

using namespace std;

void Packet::print(void) const { cout << "v: " << version << " t: " << type; }

/* Literal methods. */
void Literal::print(void) const {
    cout << "[ ";
    Packet::print();
    cout << " literal: " << literal;
    cout << " ]";
}

size_t Literal::version_count(void) const { return version; }

size_t Literal::eval(void) const { return literal; }

/* Operator methods. */
void Operator::print(void) const {
    cout << "[ ";
    Packet::print();
    cout << " [ ";
    for (const auto &p : subpackets) {
        p->print();
    }
    cout << "]]";
}

size_t Operator::version_count(void) const {
    size_t sum = version;
    for (const auto &p : subpackets) {
        sum += p->version_count();
    }
    return sum;
}

size_t Operator::eval(void) const {
    switch (type) {
        case 0: {
            size_t sum = 0;
            for (const auto &p : subpackets) {
                sum += p->eval();
            }
            return sum;
        }
        case 1: {
            size_t product = 1;
            for (const auto &p : subpackets) {
                product *= p->eval();
            }
            return product;
        }
        case 2: {
            size_t min = numeric_limits<size_t>::max();
            for (const auto &p : subpackets) {
                size_t val = p->eval();
                min = val < min ? val : min;
            }
            return min;
        }
        case 3: {
            size_t max = 0;
            for (const auto &p : subpackets) {
                size_t val = p->eval();
                max = val > max ? val : max;
            }
            return max;
        }
        case 5:
            return subpackets[0]->eval() > subpackets[1]->eval();
        case 6:
            return subpackets[0]->eval() < subpackets[1]->eval();
        case 7:
            return subpackets[0]->eval() == subpackets[1]->eval();
    }

    throw runtime_error("Evalution of unknown packet type");
}
