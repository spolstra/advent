#include <array>
#include <iostream>
#include <string>

using namespace std;

int main(void) {
    string s;
    int num;
    size_t len = 0, total = 0;
    array<size_t, 32> bit_count = { 0 };

    // Read numbers and count bits in bit_count.
    while (cin >> s) {
        len = s.length(); // Because we need the number of bits later on.
        num = stoi(s, NULL, 2);
        cout << num << endl;

        size_t bit = 0;
        // Record bit count starting at LSB (right side).
        while (num > 0) {
            if (num % 2 == 1) {
                ++bit_count[bit];
            }
            num /= 2;
            ++bit;
        }
        ++total;
    }

    // Calculate gamma
    // Add power of two's starting at LSB
    size_t gamma = 0;
    size_t power_two = 1;
    for (size_t bit = 0; bit < len; ++bit) {
        // cout << bit << ": " << bit_count[bit] << endl;
        if (bit_count[bit] > total / 2) {
            gamma += power_two;
            cout << "1";
        } else {
            cout << "0";
        }
        power_two *= 2;
    }
    cout << endl << endl << gamma << endl;

    // Calculate epsilon by negating gamma all 'len' bits
    size_t epsilon = ~gamma & ((1 << len) - 1); // bitwise negate with mask
    cout << epsilon << endl;

    cout << "power: " << gamma * epsilon << endl;
    return 0;
}
