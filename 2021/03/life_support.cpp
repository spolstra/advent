#include <iostream>
#include <set>
#include <string>
#include <algorithm>
#include <utility>
#include <array>
#include <bitset>

const int num_bits = 5;

using namespace std;

/* Print int set in decimal and binary */
void print_set(const set<int> &s) {
    for (auto e : s) {
        cout << e << "\t";
        cout << bitset<num_bits>((unsigned long long)e) << endl;
    }
}

/* Print elements [0..n> of array 'a' */
void print_array(const array<size_t, 32> &a, size_t n) {
    for (size_t i = 0; i < n; ++i) {
        cout << a[i] << " ";
    }
    cout << endl;
}

/* Read numbers from input.
   Return them as a set together with the bit length.
*/
pair<set<int>, size_t> read_nums() {
    string s;
    size_t len;
    set<int> nums;
    while (cin >> s) {
        len = s.length();
        nums.insert(stoi(s, NULL, 2));
    }
    return make_pair(nums, len);
}

/* Calculate bit count for a set of numbers */
array<size_t, 32> get_bit_counts(const set<int> &nums) {
    array<size_t, 32> bit_count = { 0 };
    // Record bit count in all positions of the set nums in bit_count.
    for (auto num : nums) {
        size_t bit = 0;
        while (num > 0) {
            if (num % 2 == 1) {
                ++bit_count[bit];
            }
            num /= 2;
            ++bit;
        }
    }
    return bit_count;
}

/* Abstract class. Contains a function that will return the reference bits
 * from a set of numbers. */
class GetRefBits {
    public:
    virtual array<size_t, 32> operator() (const set<int> &nums) = 0;
};

/* Input: set of integers 
 * Output: array where the bits are set to the most common bit.
 * If a position has the same number of 0's and 1's to bit is set to 1.
 */
class GetMajority : public GetRefBits {
    public:
    array<size_t, 32> operator() (const set<int> &nums) {
        array<size_t, 32> bit_count = get_bit_counts(nums);
        /* calculate bits from the calculated bit counts. */
        transform(bit_count.begin(), bit_count.end(), bit_count.begin(),
                [&nums](size_t n) -> size_t { return n * 2 >= nums.size(); });
        return bit_count;
    }
};

/* Input: set of integers 
 * Output: array where the bits are set to the least common bit.
 * If a position has the same number of 0's and 1's to bit is set to 0.
 */
class GetMinority : public GetRefBits {
    public:
    array<size_t, 32> operator() (const set<int> &nums) {
        array<size_t, 32> bit_count = get_bit_counts(nums);
        /* calculate bits from the calculated bit counts. */
        transform(bit_count.begin(), bit_count.end(), bit_count.begin(),
                [&nums](size_t n) -> size_t { return n * 2 < nums.size(); });
        return bit_count;
    }
};

/* Find oxygen or co2 rating: the number is left after filtering down the
 * input to the number that matches the bit_count with the ref bits.
 */
int get_rating(set<int> nums, int nbits, GetRefBits &select) {

    set<int> filtered;
    /* Start checking bits from the MSB. */
    for (int nb = nbits - 1; nb >= 0; --nb) {
        /* Calculate the bit counts of nums. */
        array<size_t, 32> bit_count = select(nums);

        /* Select the current ref_bit to consider for this iteration. */
        int ref_bit = bit_count[nb];
        /* Filter: copy all nums with matching ref_bit to filtered set. */
        std::copy_if(nums.begin(), nums.end(),
                std::inserter(filtered, filtered.end()),
                [&](int n) { return ((n >> nb) & 0x1) == ref_bit; });

        /* If there is one element left, we are done. */
        if (filtered.size() == 1) {
            return *filtered.begin();
        }
        nums = filtered;
        filtered.clear();
    }
    throw runtime_error("Filtering failed");
}

int main(void) {
    auto [nums, nbits] = read_nums();

    GetMajority majority = GetMajority();
    int oxygen_rating = get_rating(nums, (int) nbits, majority);
    cout << "oxygen rating " << oxygen_rating << endl;

    GetMinority minority = GetMinority();
    int co2_rating = get_rating(nums, (int) nbits, minority);
    cout << "co2 rating " << co2_rating << endl;

    cout << "solution " << oxygen_rating * co2_rating << endl;

    return 0;
}
