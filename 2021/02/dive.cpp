#include <iostream>

using namespace std;

int main(void) {
    int x = 0, depth = 0;
    string command;
    int operand;

    while(cin >> command) {
        cin >> operand;
        if (command == "forward") {
            x += operand;
        } else if (command == "down") {
            depth += operand;
        } else if (command == "up") {
            depth -= operand;
        } else {
            throw runtime_error("Unknown command");
        }
    }

    cout << "depth: " << depth << ", x: " << x << endl;
    cout << "answer: " << depth * x << endl;

    return 0;
}
