#include <iostream>

using namespace std;

int main(void) {
    int x = 0, depth = 0, aim = 0;
    string command;
    int operand;

    while(cin >> command) {
        cin >> operand;
        if (command == "forward") {
            x += operand;
            depth += aim * operand;
        } else if (command == "down") {
            aim += operand;
        } else if (command == "up") {
            aim -= operand;
        } else {
            throw runtime_error("Unknown command");
        }
    }

    cout << "depth: " << depth << ", x: " << x << endl;
    cout << "answer: " << depth * x << endl;

    return 0;
}
