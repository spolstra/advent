#include <array>
#include <vector>
#include <iostream>
#include <memory>
#include <string>
#include <sstream>

#include "board.h"

using namespace std;

vector<int> read_nums(string s) {
    vector<int> nums;
    string item;
    istringstream iss(s);
    while (getline(iss, item, ',')) {
        nums.push_back(stoi(item));
    }
    return nums;
}

int main(void) {
    string s;
    cin >> s;
    vector<int> nums = read_nums(s);

    // debug
    for (auto n : nums) {
        cout << n << " ";
    }
    cout << endl;

    vector<shared_ptr<Board>> boards;
    while(true) {
        shared_ptr<Board> b = make_shared<Board>(cin);
        if (cin.eof()) {
            break;
        }
        boards.push_back(b);
    }

    for (const auto &b : boards) {
        cout << *b;
        cout << endl;
    }

    // Check all numbers on all remaining boards until we get a bingo.
    for (int n : nums) {
        // Iterate over all boards that are left
        auto it = boards.begin();
        while (it != boards.end()) {
            (*it)->mark(n);
            int r = (*it)->is_bingo(n);
            if (r != -1) {
                if (boards.size() == 1) {
                    // Last bingo, report score.
                    cout << "bingo with score: " << r << endl;
                    return 0;
                } else {
                    // Remove board from boards
                    it = boards.erase(it);
                }
            } else { // Check next board
                ++it;
            }
        }
    }
    return 0;
}
