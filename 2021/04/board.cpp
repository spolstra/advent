#include <array>
#include <vector>
#include <iostream>
#include <memory>
#include <string>
#include <sstream>

#include "board.h"

using namespace std;

Board::Board(istream &is) {
    for (size_t r = 0; r < size; ++r) {
        for (size_t c = 0; c < size; ++c) {
            int n;
            is >> n;
            nums[r][c] = n;
        }
    }
}

void Board::mark(int num) {
    for (size_t r = 0; r < size; ++r) {
        for (size_t c = 0; c < size; ++c) {
            if (nums[r][c] == num) {
                hits[r][c] = true;
            }
        }
    }
}
 
/* Return the score if the board has bingo, otherwise return -1. */
int Board::is_bingo(int num) {
    // check rows
    for (size_t r = 0; r < size; ++r) {
        bool bingo = true;
        for (size_t c = 0; c < size; ++c) {
            if (hits[r][c] == false) {
                bingo = false;
                break;
            }
        }
        if (bingo) {
            return unmarked_sum() * num;
        }
    }

    // check colums
    for (size_t c = 0; c < size; ++c) {
        bool bingo = true;
        for (size_t r = 0; r < size; ++r) {
            if (hits[r][c] == false) {
                bingo = false;
                break;
            }
        }
        if (bingo) {
            return unmarked_sum() * num;
        }
    }
    return -1;
}

int Board::unmarked_sum(void) {
    // Count unmarked numbers
    int sum = 0;
    for (size_t r = 0; r < size; ++r) {
        for (size_t c = 0; c < size; ++c) {
            if (!hits[r][c]) {
                sum += nums[r][c];
            }
        }
    }
    return sum;
}



ostream &operator<<(ostream &os, const Board &b) {
    for (size_t r = 0; r < Board::size; ++r) {
        for (size_t c = 0; c < Board::size; ++c) {
            os << b.nums[r][c] << " ";
        }
        os << endl;
    }
    return os;
}


