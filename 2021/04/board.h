class Board {
    static constexpr size_t size = 5;
    int nums[size][size] = { { 0 } };
    bool hits[size][size] = { { false } };

    public:
    Board(std::istream &stream);
    ~Board() { }
    int is_bingo(int num);
    int unmarked_sum(void);
    void mark(int num);

    friend std::ostream &operator<<(std::ostream &os, const Board &b);
};

