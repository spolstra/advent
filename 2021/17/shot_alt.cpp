#include <iostream>
#include <regex>

/* Naive solution. 30 times slower than shot.cpp */

using namespace std;

struct Box {
    int x_min;
    int x_max;
    int y_min;
    int y_max;
};

struct Pos {
    int x;
    int y;
};

struct V {
    int x;
    int y;
};

struct Box read_input(void) {
    string line;
    regex pattern(
        "target area: x=(-?[[:digit:]]+)..(-?[[:digit:]]+), "
        "y=(-?[[:digit:]]+)..(-?[[:digit:]]+)");
    smatch results;

    getline(cin, line);
    if (regex_search(line, results, pattern)) {
        Box t{0, 0, 0, 0};
        t.x_min = stoi(results.str(1));
        t.x_max = stoi(results.str(2));
        t.y_min = stoi(results.str(3));
        t.y_max = stoi(results.str(4));
        return t;
    }
    throw runtime_error("Cannot parse input");
}

bool in_box(const Pos &p, const Box &box) {
    if (p.x >= box.x_min && p.x <= box.x_max && p.y <= box.y_max &&
        p.y >= box.y_min) {
        return true;
    }
    return false;
}

/* Shoot with vector V until we hit the target box or pass it.
 * set max_height to maximum height recorded.
 * Return true if we hit the box, false otherwise. */
bool shoot(V v, const Box &box, int &max_height) {
    Pos pos{};  // start at pos 0,0
    int height = 0;

    while (pos.x <= box.x_max && pos.y >= box.y_min) {
        pos.x += v.x;
        pos.y += v.y;

        if (v.x > 0) {
            --v.x;
        }
        --v.y;

        if (pos.y > height) {
            height = pos.y;
        }

        if (in_box(pos, box)) {
            max_height = height;
            return true;
        }
    }
    return false;
}

/* Find maximum height that hits box.
 * Try all initial speed vectors in the range [(0,-min_v)..(max_v, max_v)]
 * and record the max height if the speed vector hits the box.
 *
 * Returns the maximum height found.
 */
int solve(const Box &box, int min_v, int max_v) {
    V v{};
    int height = 0, max_height = 0;

    for (v.x = 0; v.x <= max_v; ++v.x) {
        for (v.y = min_v; v.y <= max_v; ++v.y) {
            if (shoot(v, box, height)) {
                if (height > max_height) {
                    cout << v.x << "," << v.y << endl;
                    cout << "height: " << height << endl;
                    max_height = height;
                }
            }
        }
    }
    return max_height;
}

int main(void) {
    Box box = read_input();

    // Find height using brute force.
    int height = solve(box, -10000, 10000);

    cout << "Solution: " << height << endl;
}
