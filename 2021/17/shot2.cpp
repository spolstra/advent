#include <iostream>
#include <regex>
#include <vector>
#include <utility>
#include <set>

using namespace std;

struct Target {
    int xmin;
    int xmax;
    int ymin;
    int ymax;
};

struct Target read_input(void) {
    string line;
    regex pattern("target area: x=(-?[[:digit:]]+)..(-?[[:digit:]]+), y=(-?[[:digit:]]+)..(-?[[:digit:]]+)");
    smatch results;

    getline(cin, line);
    if (regex_search(line, results, pattern)) {
        Target t {0, 0, 0, 0};
        t.xmin = stoi(results.str(1));
        t.xmax = stoi(results.str(2));
        t.ymin = stoi(results.str(3));
        t.ymax = stoi(results.str(4));
        return t;
    }
    throw runtime_error("Cannot parse input");
}

/* Shoot probe in x direction with initial speed v.
 * If pos is in the range [min..max] after nsteps return true.
 * Otherwise return false.
 */
bool shoot_x(int v, int min, int max, int nsteps) {
    int pos = 0;
    int steps = 0;
    
    while (pos <= max && steps < nsteps) {
        pos += v;
        if (v > 0) {
            --v;
        }
        ++steps;
        if (pos >= min && pos <= max && steps == nsteps) {
            return true;
        }
    }
    return false;
}

/* Shoot probe in y direction with initial speed v.
 * If probe y pos in range [min..max] and steps to the result.
 * Return all valid steps.
 */
vector<int> shoot_y(int v, int min, int max) {
    int pos = 0;
    int steps = 0;
    vector<int> all_steps;

    while (pos >= min) {
        pos += v;
        --v;
        ++steps;
        if (pos >= min && pos <= max) {
            all_steps.push_back(steps);
        }
    }
    return all_steps;
}

/* Solve vx and vy of initial vector (vx, vy) independently.
 */
set<pair<int, int>> solve(const Target &box, int min_v, int max_v) {
    set<pair<int, int>> solutions;
    int vx, vy;

    for (vy = max_v; vy >= min_v; --vy) {
        vector<int> valid_steps = shoot_y(vy, box.ymin, box.ymax);
        if (valid_steps.size() != 0) {
            /* vy hits the box in nsteps. Now find vx. */
            for (const int nsteps : valid_steps) {
                for (vx = min_v; vx < max_v; ++vx) {
                    if (shoot_x(vx, box.xmin, box.xmax, nsteps)) {
                        solutions.insert(make_pair(vx, vy));
                    }
                }
            }
        }
    }
    return solutions;
}

int main(void) {
    Target box = read_input();

    // Try brute force first
    set<pair<int, int>> solutions = solve(box, -10000, 10000);
    cout << "Number of solutions: " << solutions.size() << endl;
}
