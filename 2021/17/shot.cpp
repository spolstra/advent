#include <iostream>
#include <regex>

using namespace std;

struct Target {
    int xmin;
    int xmax;
    int ymin;
    int ymax;
};

struct Target read_input(void) {
    string line;
    regex pattern("target area: x=(-?[[:digit:]]+)..(-?[[:digit:]]+), y=(-?[[:digit:]]+)..(-?[[:digit:]]+)");
    smatch results;

    getline(cin, line);
    if (regex_search(line, results, pattern)) {
        Target t {0, 0, 0, 0};
        t.xmin = stoi(results.str(1));
        t.xmax = stoi(results.str(2));
        t.ymin = stoi(results.str(3));
        t.ymax = stoi(results.str(4));
        return t;
    }
    throw runtime_error("Cannot parse input");
}

/* Shoot probe in x direction with initial speed v.
 * If pos is in the range [min..max] after nsteps return true.
 * Otherwise return false.
 */
bool shoot_x(int v, int min, int max, int nsteps) {
    int pos = 0;
    int steps = 0;
    
    while (pos <= max && steps < nsteps) {
        pos += v;
        if (v > 0) {
            --v;
        }
        ++steps;
        if (pos >= min && pos <= max && steps == nsteps) {
            return true;
        }
    }
    return false;
}

/* Shoot probe in y direction with initial speed v.
 * If probe y pos in range [min..max] return the number of steps taken.
 * Otherwise return 0.
 * Also set height to maximum height reached.
 */
int shoot_y(int v, int min, int max, int &height) {
    int pos = 0;
    int steps = 0;
    height = 0;

    while (pos >= max) {
        pos += v;
        --v;
        ++steps;
        if (pos > height) {
            height = pos;
        }
        if (pos >= min && pos <= max) {
            return steps;
        }
    }
    return 0;
}

/* Find maximum height that hits box.
 * Try to solve vx and vy of initial vector (vx, vy) independently.
 *
 * First launch probe straight up and find the maximum height that still goes
 * through the box. Store the step count when it hits the box.
 *
 * Then try to find the corresponding vx that hits the x-range in the box with
 * that step count.
 *
 * If there is no solution for that step count, try with the second largest vy
 * and repeat until we find a corresponding vx.
 */
int solve(const Target &box, int min_v, int max_v) {
    int vx, vy, nsteps, height, max_height = -1;

    for (vy = max_v; vy >= min_v; --vy) {
        nsteps = shoot_y(vy, box.ymin, box.ymax, height);
        if (nsteps != 0) {
            /* vy hits the box in nsteps. Now find vx. */
            for (vx = min_v; vx < max_v; ++vx) {
                if (shoot_x(vx, box.xmin, box.xmax, nsteps)) {
                    cout << vx << ", " << vy << endl;
                    if (height > max_height) {
                        max_height = height;
                    }
                }
            }
        }
    }
    return max_height;
}

int main(void) {
    Target box = read_input();

    // Try brute force first
    int height = solve(box, -10000, 10000);

    cout << "Solution: " << height << endl;
}
