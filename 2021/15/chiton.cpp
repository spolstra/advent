#include <algorithm>
#include <iostream>
#include <limits>
#include <queue>
#include <set>
#include <string>
#include <tuple>
#include <vector>

#include "graph.h"

using namespace std;

/* Source: https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
function Dijkstra(Graph, source):
 2
 3      for each vertex v in Graph.Vertices:
 4          dist[v] ← INFINITY
 5          prev[v] ← UNDEFINED
 6          add v to Q
 7      dist[source] ← 0
 8
 9      while Q is not empty:
10          u ← vertex in Q with min dist[u]
11          remove u from Q
12
13          for each neighbor v of u still in Q:
14              alt ← dist[u] + Graph.Edges(u, v)
15              if alt < dist[v] and dist[u] is not INFINITY:
16                  dist[v] ← alt
17                  prev[v] ← u
18
19      return dist[], prev[]
*/
int shortest_path(const Graph &g) {
    vector<int> distance(g.adj.size(), numeric_limits<int>::max());
    set<int> visited;
    vector<int> prev(g.adj.size(), -1);  // prev is undefined for all nodes.

    // (dist,node), distance first so we get min-priority queue on distance.
    priority_queue<pair<int, int>, vector<pair<int, int>>,
                   greater<pair<int, int>>>
        q;

    distance[0] = 0;
    for (unsigned int i = 0; i < distance.size(); ++i) {
        q.push(make_pair(distance[i], i));
    }

    while (!q.empty()) {
        auto u = q.top();
        q.pop();
        visited.insert(u.second);
        /* u.first == distance from start, u.second == node number. */

        /* Check if this is an outdated copy of the node.
         * This can happen because when we update the distance of a node.
         * we just push the same node with a smaller distance and leave the
         * old one in the priority queue. */
        if (u.first > distance[u.second]) {
            continue;
        }

        /* For all neighbours v of u that are still in q. */
        for (const NodeWeight &v : g.adj[u.second]) {
            if (visited.contains(v.node)) {
                continue;  // in visited means not in q.
            }

            int dist = distance[u.second] + v.weight;

            // TODO why need the max check here? Seems unnecessary.
            if (dist < distance[v.node] &&
                distance[u.second] != numeric_limits<int>::max()) {
                distance[v.node] = dist;
                prev[v.node] = u.second;
                q.push(make_pair(dist, v.node));  // new dist, reinsert in q!
            }
        }
    }

    /* For debugging, get path from start finish. */
    int node = g.adj.size() - 1;  // destination node id is the last node.
    vector<int> path;
    prev[0] = 0;  // prev of first is first.
    while (node != 0) {
        path.push_back(node);
        node = prev[node];
    }

    for_each(path.rbegin(), path.rend(), [](const int n) { cout << n << " "; });
    cout << endl;

    return distance[g.adj.size() - 1];
}

bool valid_neighbour(int src, int dst, int width, int size) {
    if (dst < 0) return false;
    if (dst >= size) return false;
    if (dst == src + 1 && dst % width == 0) return false;
    if (dst == src - 1 && src % width == 0) return false;
    return true;
}

pair<vector<int>, int> read_map_data(void) {
    vector<int> nodes;
    string line;
    getline(cin, line);
    int width = line.length();

    /* Read map into a vector of nodes. */
    do {
        for (char d : line) {
            nodes.push_back(d - '0');
        }
    } while (getline(cin, line));
    return make_pair(nodes, width);
}

Graph convert_to_graph(vector<int> nodes, int width) {
    int size = nodes.size();
    vector<Edge> edges;

    /* Convert vector of nodes to edge list. */
    vector<int> offsets = {-1, 1, -width, width};
    for (int n = 0; n < size; ++n) {
        for (int offset : offsets) {
            if (valid_neighbour(n, n + offset, width, size)) {
                edges.push_back(Edge(n, n + offset, nodes[n + offset]));
            }
        }
    }

    return Graph(edges, size);
}

vector<int> extend_row(vector<int> nodes, int width, int offset, int n) {
    vector<int> src(nodes.cbegin() + offset, nodes.cbegin() + offset + width);
    vector<int> dst = src;
    for (int start = 0, end = width, i = 0; i < n - 1;
         start += width, end += width, ++i) {
        transform(src.cbegin() + start, src.cbegin() + end, back_inserter(dst),
                  [](int n) { return n + 1 < 10 ? n + 1 : 1; });
        src = dst;
    }
    return dst;
}

/* Extend map 'n' times. */
pair<vector<int>, int> extend_map(vector<int> nodes, int width, int n) {
    vector<int> result;

    /* Extend in x-direction. */
    for (int i = 0; i < static_cast<int>(nodes.size()) / width; ++i) {
        vector<int> dst = extend_row(nodes, width, width * i, n);
        result.insert(result.end(), dst.begin(), dst.end());
    }

    /* Extend in y-direction. */
    result = extend_row(result, result.size(), 0, n);

    return make_pair(result, width * n);
}

Graph read_map(int extend) {
    vector<int> nodes;
    int width;

    tie(nodes, width) = read_map_data();
    tie(nodes, width) = extend_map(nodes, width, extend);
    return convert_to_graph(nodes, width);
}

int main(void) {
    // 15a
    // Graph g = read_map(1);

    // 15b
    Graph g = read_map(5);

    // cout << g;
    int distance = shortest_path(g);
    cout << "Distance: " << distance << endl;
}
