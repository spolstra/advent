#include <vector>
#include <iostream>
#include <utility>

/* Graph implementation with weights:
   https://www.techiedelight.com/graph-implementation-using-stl/
*/

using namespace std;

struct Edge {
    Edge(int s, int d, int w) : src(s), dest(d), weight(w) {}
    int src;
    int dest;
    int weight;
};

struct NodeWeight {
    NodeWeight(int n, int w) : node(n), weight(w) {}
    int node;
    int weight;
};

class Graph {
    public:
    vector<vector<NodeWeight>> adj;

    Graph(vector<Edge> const &edges, int N) {
        adj.resize(N);

        for (const Edge &e : edges) {
            adj[e.src].push_back(NodeWeight(e.dest, e.weight));
        }
    }

    friend ostream &operator<<(ostream &os, const Graph &g);
};


