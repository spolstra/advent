#include <iostream>
#include "graph.h"

using namespace std;

ostream &operator<<(ostream &os, const Graph &g) {
    for (auto it = g.adj.cbegin(); it != g.adj.cend(); ++it) {
        os << distance(g.adj.cbegin(), it) << " : ";
        for (const NodeWeight &d : *it) {
            os << "(" << d.node << " " << d.weight << ") ";
        }
        os << endl;
    }
    return os;
}
