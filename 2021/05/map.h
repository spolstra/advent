#include <iostream>
#include <regex>
#include <utility>

using namespace std;

class Map {
    regex coord_pat {R"((\d+))"};
    static constexpr int size = 1000;
    int data[size][size] = { { 0 } };
    public:
    void add_line(string s, bool diagonal = false);
    void add_points(pair<int, int> start, pair<int, int> end, bool diag=false);
    void add_points_diagonal(pair<int, int> start, pair<int, int> end);
    size_t overlap(void) const;
};
