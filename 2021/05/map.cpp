#include <iostream>
#include <utility>

#include "map.h"

using namespace std;

void Map::add_line(string s, bool diagonal) {
    for (sregex_iterator p(s.begin(), s.end(), coord_pat);
            p != sregex_iterator{};
            p++) {
        auto start = make_pair(stoi((*p)[1]), stoi((*(++p))[1]));
        auto end = make_pair(stoi((*(++p))[1]), stoi((*(++p))[1]));
        add_points(start, end, diagonal);
    }
}

void Map::add_points(pair<int, int> start, pair<int, int> end, bool diag) {
    //cout << start.first << "," << start.second << " -> " \
    //   << end.first << "," << end.second << endl;

    if (start.first == end.first) { // horizontal line
        if (start.second > end.second) { // reorder if needed.
            start.swap(end);
        }
        while (start.second <= end.second) { // add points on line
            ++data[start.first][start.second];
            ++start.second;
        }
    } else if (start.second == end.second) { // vertical line
        if (start.first > end.first) { // reorder if needed.
            start.swap(end);
        }
        while (start.first <= end.first) { // add points on line
            ++data[start.first][start.second];
            ++start.first;
        }
    } else if (diag) {
        add_points_diagonal(start, end);
    }
}

/* Mark diagonal points in data map. */
void Map::add_points_diagonal(pair<int, int> start, pair<int, int> end) {
    // Set step directions.
    int x_step = 1, y_step = 1;
    if (start.first > end.first) {
        x_step = -1;
    }
    if (start.second > end.second) {
        y_step = -1;
    }

    // Mark all points.
    while (start.first != end.first) {
        ++data[start.first][start.second];
        start.first += x_step;
        start.second += y_step;
    }
    ++data[start.first][start.second]; // mark end point as well.
    return;
}

size_t Map::overlap(void) const {
    size_t count = 0;
    for (size_t x = 0; x < size; ++x) {
        for (size_t y = 0; y < size; ++y) {
            if (data[x][y] >= 2) {
                ++count;
            }
        }
    }
    return count;
}

