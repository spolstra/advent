#include <iostream>
#include <string>

#include "map.h"

using namespace std;

int main(void) {
    string s;
    Map m;
    while (!cin.eof()) {
        getline(cin, s);
        // m.add_line(s);  // 5a
        m.add_line(s, true); // 5b
    }

    cout << "Overlap count " << m.overlap() << endl;

    return 0;
}
