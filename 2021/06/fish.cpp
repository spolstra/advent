#include <iostream>
#include <numeric>
#include <vector>
#include <regex>

using namespace std;

class Fish {
    /* q is a vector of 9 positions to store the fish.
     * Fish give birth in postion 0.
     * Born fish start in position 8.
     * Parent fish then go back to position 6.
     * Q is shifted left for each time step.
     */
    vector<size_t> q;
    public:
    Fish(vector<int> init);
    void age(void);
    size_t total(void);
    friend ostream &operator<<(ostream &os, const Fish &f);
};

/* Debug: print the queue contents. */
Fish::Fish(vector<int> init) {
    q.insert(q.begin(), 9, 0);
    for (auto n : init) {
        ++q[n];
    }
}

/* Age all fish in the queue. */
void Fish::age(void) {
    /* Shift all fish to the left. */
    /* Record fish that will create children and remove them */
    size_t birth = q[0];
    q.erase(q.begin());
    q.push_back(birth); // Add new fish.
    q[6] += birth; // And reintroduce the old fish.
}


/* Return the total number of Fish in the queue */
size_t Fish::total(void) {
    return accumulate(q.begin(), q.end(), 0L);
}

ostream &operator<<(ostream &os, const Fish &f) {
    for (size_t n : f.q) {
        cout << n << " ";
    }
    cout << endl;
    return os;
}

/* Read list of numbers from input stream and return as vector. */
vector <int> read_nums(istream &is) {
    regex num_pat {R"((\d+))"};
    vector<int> nums;
    string s;
    getline(is, s);
    for (sregex_iterator p(s.begin(), s.end(), num_pat);
            p != sregex_iterator{}; ++p) {
        nums.push_back(stoi((*p)[1]));
    }
    return nums;
}

int main(void) {
    vector<int> nums = read_nums(cin);
    Fish f(nums);

    for (int t = 0; t <= 256; ++t) {
        // cout << t << " : " << f;
        cout << t << ": " << f.total() << endl;
        f.age();
    }

    return 0;
}
