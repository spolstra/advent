#include <iostream>

using namespace std;

int main(void) {
    unsigned int prev, cur, increased = 0;

    cin >> prev;
    while (cin >> cur) {
        if (cur > prev) {
            ++increased;
        }
        prev = cur;
    }

    cout << increased << endl;
    return 0;
}
