#include <deque>
#include <iostream>
#include <numeric>

using namespace std;

struct Nums {
    deque<int> nums;

    Nums(void) {
        for (int i = 0; i < 3; ++i) {
            read();
        }
    }

    bool read(void) {
        int num;
        if (!(cin >> num)) {
            return false;
        }
        nums.push_front(num);
        if (nums.size() > 3) {
            nums.pop_back();
        }
        return true;
    }

    int total(void) const {
        return accumulate(nums.begin(), nums.end(), 0);
    }
};

int main(void) {
    unsigned int prev, cur, increased = 0;

    Nums nums;
    prev = nums.total();

    while (nums.read()) {
        cur = nums.total();
        if (cur > prev) {
            ++increased;
        }
        prev = cur;
    }

    cout << increased << endl;
    return 0;
}
