#include <Eigen/Dense>
#include <iostream>
#include <set>

using namespace Eigen;
using namespace std;

int main() {
    // Matrix2d mat;
    // mat << 1, 2, 3, 4;
    //
    Vector3d v(1, 2, 3);
    Matrix3d rotx;
    Matrix3d tmp;
    rotx << 1, 0, 0,
            0 , 0, -1,
            0, 1, 0;

    std::cout << "Here is mat:\n" << rotx << endl;
    cout << "v: " << v << endl;

    for (int i = 0; i < 6; i++) {
        // cout << v << endl; endl;
        v = rotx * v;
        cout << v << endl << endl;
    }

    /*
    std::cout << "Here is mat*u:\n" << mat * u << std::endl;
    std::cout << "Here is u^T*mat:\n" << u.transpose() * mat << std::endl;
    std::cout << "Here is u^T*v:\n" << u.transpose() * v << std::endl;
    std::cout << "Here is u*v^T:\n" << u * v.transpose() << std::endl;
    std::cout << "Let's multiply mat by itself" << std::endl;
    mat = mat * mat;
    std::cout << "Now mat is mat:\n" << mat << std::endl;
    */
}

