#include <Eigen/Dense>
#include <iostream>
#include <set>
#include <vector>

using namespace Eigen;
using namespace std;

/* Returns a vector of all 24 rotation matrices.
 * Uses the formula:
 * (rot_x^p)*(rot_y^q)*(rot_x^r)*(rot_y^s)
 * with p,q,r,s = {0, 1, 2, 3}
 *
 * ref: https://stackoverflow.com/questions/16452383/how-to-get-all-24-rotations-of-a-3-dimensional-array
 *
 * More general information:
 * https://en.wikipedia.org/wiki/Rotation_matrix#In_three_dimensions
 */
vector<Matrix3d> calc_rotations(void) {
    Matrix3d res;
    Matrix3d rot_x;
    Matrix3d rot_y;

    rot_x << 1, 0, 0, 0, 0, -1, 0, 1, 0;
    rot_y << 0, 0, 1, 0, 1, 0, -1, 0, 0;

    vector<Matrix3d> rotations;
    vector<Matrix3d> rotations_uniq;

    // Store all possible rotations in 'rotations' (with duplicates)
    for (int p = 0; p < 4; p++) {
        // Reset res to identity matrix.
        res << 1, 0, 0, 0, 1, 0, 0, 0, 1;
        // Do p number of x rotations.
        for (int i = 0; i < p; i++) {
            res = res * rot_x;
        }

        for (int q = 0; q < 4; q++) {
            // And then do q number of y rotations.
            for (int i = 0; i < q; i++) {
                res = res * rot_y;
            }
            for (int r = 0; r < 4; r++) {
                // And then do r number of x rotations.
                for (int i = 0; i < r; i++) {
                    res = res * rot_x;
                }
                for (int s = 0; s < 4; s++) {
                    // And then do r number of x rotations.
                    for (int i = 0; i < s; i++) {
                        res = res * rot_y;
                    }
                    rotations.push_back(res);
                    // cout << res << endl << endl;
                }
            }
        }
    }

    // Remove duplicates.
    for (auto p = rotations.cbegin(); p != rotations.cend(); ++p) {
        bool uniq = true;
        for (auto q = p + 1; q != rotations.cend(); ++q) {
            if (*p == *q) {
                uniq = false;
                break;
            }
        }
        if (uniq) {
            rotations_uniq.push_back(*p);
        }
    }

    return rotations_uniq;
}

int main() {
    vector<Matrix3d> rotations = calc_rotations();

    cout << rotations.size() << endl;
    /*
    for (const Matrix3d &m : rotations) {
        cout << m << endl << endl;
    }
    */
}
