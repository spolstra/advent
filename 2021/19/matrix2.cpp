#include <Eigen/Dense>
#include <iostream>
#include <set>
#include <vector>

using namespace Eigen;
using namespace std;

int main() {
    Matrix3d rotx;
    Matrix3d res;
    // set<Matrix3d> all_rotx_set;
    vector<Matrix3d> all_rotx;
    rotx << 1, 0, 0,
            0 , 0, -1,
            0, 1, 0;
    Matrix3d rotcopy;
    rotcopy << 1, 0, 0,
            0 , 0, -1,
            0, 1, 1;


    res = rotx;
    if (rotcopy == rotx) {
        cout << "same" << endl;
    } else {
        cout << "different" << endl;
    }

    for (int i = 0; i < 6; i++) {
        cout << res << endl << endl;
        res = res * rotx;
        // all_rotx_set.insert(res);
        all_rotx.push_back(res);
    }

    cout << all_rotx.size() << endl;
    // cout << all_rotx_set.size() << endl;

}

