#include <Eigen/Dense>
#include <iostream>
#include <set>
#include <string>
#include <vector>
#include <limits>

using namespace Eigen;
using namespace std;

/* Returns a vector of all 24 rotation matrices.
 * Uses the formula:
 * (rot_x^p)*(rot_y^q)*(rot_x^r)*(rot_y^s)
 * with p,q,r,s = {0, 1, 2, 3}
 *
 * ref:
 * https://stackoverflow.com/questions/16452383/how-to-get-all-24-rotations-of-a-3-dimensional-array
 *
 * More general information:
 * https://en.wikipedia.org/wiki/Rotation_matrix#In_three_dimensions
 */
vector<Matrix3d> calc_rotations(void) {
    Matrix3d res;
    Matrix3d rot_x;
    Matrix3d rot_y;

    rot_x << 1, 0, 0, 0, 0, -1, 0, 1, 0;
    rot_y << 0, 0, 1, 0, 1, 0, -1, 0, 0;

    vector<Matrix3d> rotations;
    vector<Matrix3d> rotations_uniq;

    // Store all possible rotations in 'rotations' (with duplicates)
    for (int p = 0; p < 4; p++) {
        // Reset res to identity matrix.
        res << 1, 0, 0, 0, 1, 0, 0, 0, 1;
        // Do p number of x rotations.
        for (int i = 0; i < p; i++) {
            res = res * rot_x;
        }

        for (int q = 0; q < 4; q++) {
            // And then do q number of y rotations.
            for (int i = 0; i < q; i++) {
                res = res * rot_y;
            }
            for (int r = 0; r < 4; r++) {
                // And then do r number of x rotations.
                for (int i = 0; i < r; i++) {
                    res = res * rot_x;
                }
                for (int s = 0; s < 4; s++) {
                    // And then do r number of x rotations.
                    for (int i = 0; i < s; i++) {
                        res = res * rot_y;
                    }
                    rotations.push_back(res);
                    // cout << res << endl << endl;
                }
            }
        }
    }

    // Remove duplicates.
    for (auto p = rotations.cbegin(); p != rotations.cend(); ++p) {
        bool uniq = true;
        for (auto q = p + 1; q != rotations.cend(); ++q) {
            if (*p == *q) {
                uniq = false;
                break;
            }
        }
        if (uniq) {
            rotations_uniq.push_back(*p);
        }
    }

    return rotations_uniq;
}

/* Read data points of a single scanner. */
vector<Vector3d> read_scanner(void) {
    int x, y, z;
    char comma;
    string line;
    vector<Vector3d> vectors;

    while (true) {
        getline(cin, line);
        if (line.size() == 0) {
            break;
        }
        stringstream line_stream(line);
        line_stream >> x;
        line_stream >> comma;
        line_stream >> y;
        line_stream >> comma;
        line_stream >> z;
        vectors.push_back(Vector3d(x, y, z));
    }
    return vectors;
}

/* Read scanner data from cin.
 * Returns scanner data.
 */
vector<vector<Vector3d>> read_input(void) {
    string line;
    vector<vector<Vector3d>> scanner_data;
    while (getline(cin, line)) {
        scanner_data.push_back(read_scanner());
    }
    return scanner_data;
}

/* Rotate all vectors in vs with rotation matrix m. */
vector<Vector3d> rotate(const Matrix3d &m, vector<Vector3d> vs) {
    for (Vector3d &v : vs) {
        v = m * v;
    }
    return vs;
}

/* Translate all vectors in vs with translation vector t. */
vector<Vector3d> translate(vector<Vector3d> vs, Vector3d t) {
    for (Vector3d &v : vs) {
        v = v + t;
    }
    return vs;
}

/* append all points in vs to beacons. don't duplicate points. */
void append_beacons(vector<Vector3d> &beacons, const vector<Vector3d> vs) {
    for (const auto &v : vs) {
        bool found = false;
        for (auto p = beacons.cbegin(); p != beacons.cend(); ++p) {
            if (v == *p) {
                found = true;
                break;
            }
        }
        if (!found) {
            beacons.push_back(v);
        }
    }
}

/* return the number of matching points between vs and ws.
 * Assumes that all points in vs and ws themselves are unique */
size_t overlap(vector<Vector3d> vs, vector<Vector3d> ws) {
    size_t count = 0;
    for (const auto &v : vs) {
        for (const auto &w : ws) {
            if (v == w) {
                count++;
            }
        }
    }
    return count;
}

int manhattan_distance(const Vector3d &v, const Vector3d w) {
    Vector3d d = v - w;
    return abs(d.x()) + abs(d.y()) + abs(d.z());
}

int max_manhattan(const vector<Vector3d> sc) {
    int max_dist = 0;

    for (auto s1 = sc.cbegin(); s1 != sc.cend(); ++s1) {
        for (auto s2 = s1 + 1; s2 != sc.cend(); ++s2) {
            int dist = manhattan_distance(*s1, *s2); 
            if (dist > max_dist) {
                max_dist = dist;
            }
        }
    }

    return max_dist;
}

int main() {
    vector<Matrix3d> rotations = calc_rotations();

    vector<vector<Vector3d>> scanners = read_input();

    // Take scanner as the fixed starting point.
    vector<Vector3d> beacons = scanners[0];
    scanners.erase(scanners.begin());

    vector<Vector3d> scanner_locations;
    /* As long as we have unmatched scanners. */
    while (scanners.size() > 0) {
        /* For all scanners left. */
        for (auto scanner = scanners.begin(); scanner != scanners.end();
             ++scanner) {
            /* Try all rotations. */
            for (const auto &rot : rotations) {
                vector<Vector3d> vs_r = rotate(rot, *scanner);
                for (const auto &b1 : beacons) {
                    for (const auto &b2 : vs_r) {
                        /* overlay b2 on b1 and check for overlap. */
                        vector<Vector3d> vs_t = translate(vs_r, b1 - b2);
                        if (overlap(beacons, vs_t) >= 12) {
                            append_beacons(beacons, vs_t);
                            scanners.erase(scanner);  // safe, we break out
                            scanner_locations.push_back(b1 - b2);
                            goto scanner_removed;
                        }
                    }
                }
            }
        }
    scanner_removed:
        cout << "scanner matched" << endl;
    }

    for (const auto &s : scanner_locations) {
        cout << s.x() << "," << s.y() << "," << s.z() << endl;
    }
    cout << "Answer: " << max_manhattan(scanner_locations) << endl;

    return 0;
}
