#include <iostream>
#include <sstream>
#include <vector>
#include <queue>
#include <utility>

#include "map.h"

using namespace std;

Map::Map(istream &is) {
    string line;
    while(!is.eof()) {
        getline(is, line);
        if (line.empty()) {
            break;
        }

        /* read row and pad with 10 */
        vector<int> row;
        row.push_back(10);
        for (char c : line) {
            row.push_back(c - '0');
        }
        row.push_back(10);

        data.push_back(row);
    }
    size_t len = data[0].size();
    vector<int> padding = vector(len, 10);
    data.insert(data.begin(), padding);
    data.insert(data.end(), padding);
    row_length = data[0].size(); // include padding in length
    column_length = data.size();
}

ostream &operator<<(std::ostream &os, Map m) {
    for (const vector<int> &r : m.data) {
        for (int n : r) {
            if (n == 10) {
                os << "# ";
            } else {
                os << n << " ";
            }
        }
        os << endl; 
    }
    return os;
}

/* Sum all values that are smaller than their four neighbours to calculate
 * the risk. */
size_t Map::risk(void) {
    size_t risk = 0;
    /* Loop over all values, excluding the padding borders. */
    for (size_t r = 1; r < column_length - 1; ++r) {
        for (size_t c = 1; c < row_length - 1; ++c) {
            if (data[r][c] < data[r][c - 1]  && \
                    data[r][c] < data[r][c + 1] && \
                    data[r][c] < data[r - 1][c] && \
                    data[r][c] < data[r + 1][c]) {
                risk += data[r][c] + 1; // risk is value + 1
            }
        }
    }
    return risk;
}

/* Return a vector of lowpoints. */
vector<pair<size_t, size_t>> Map::lowpoints(void) {
    vector<pair<size_t, size_t>> v;
    /* Loop over all values, excluding the padding borders. */
    for (size_t r = 1; r < column_length - 1; ++r) {
        for (size_t c = 1; c < row_length - 1; ++c) {
            if (data[r][c] < data[r][c - 1]  && \
                    data[r][c] < data[r][c + 1] && \
                    data[r][c] < data[r - 1][c] && \
                    data[r][c] < data[r + 1][c]) {
                v.push_back(make_pair(r, c));
            }
        }
    }
    return v;
}

/* Return the size of the basin at low point l. */
int Map::calc_size(const pair<size_t, size_t> &l) {
    int size = 0;
    queue<pair<size_t, size_t>> q;
    q.push(l);
    data[l.first][l.second] = 9; // mark as visited

    while (!q.empty()) {
        auto p = q.front(); q.pop();
        ++size;

        /* Find valid neighbours and add to q to be visited later. */
        vector<pair<size_t, size_t>> nbours = neighbours(p);
        for (const auto &n : nbours) {
            q.push(n);
            data[n.first][n.second] = 9; // mark as visited
        }
    }
    return size;
}

/* Return a vector of valid neighbours of p.
 * ie. neighbours with a value less than 9. */
vector<pair<size_t, size_t>>
Map::neighbours(const pair<size_t, size_t> &p) const {
    vector<pair<size_t, size_t>> nbours;

    vector<pair<size_t, size_t>> offsets = {{-1,0}, {1, 0}, {0, -1}, {0, 1}};
    for (const auto &o : offsets) {
        pair<size_t, size_t> nbour = make_pair(p.first + o.first,
                p.second + o.second);
        if (get(nbour.first, nbour.second) < 9) {
            nbours.push_back(nbour);
        }
    }
    return nbours;
}
