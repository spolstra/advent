#include <iostream>
#include <vector>
#include <queue>

using namespace std;

class Map {
    vector<vector<int>> data; // row major data[row_index][column_index]
    size_t row_length; // row length including padding.
    size_t column_length; // column length including padding.

    public:
    Map(istream &is);
    int get(size_t r, size_t c) const { return data[r][c]; }
    size_t risk(void);
    vector<pair<size_t, size_t>> lowpoints(void);
    int calc_size(const pair<size_t, size_t> &l);
    vector<pair<size_t, size_t>> neighbours(const pair<size_t, size_t> &p) const;
    
    friend std::ostream &operator<<(std::ostream &os, Map m);
};
