#include <iostream>

#include "map.h"

using namespace std;

int main(void) {
    /* Read map data from cin. */
    Map m = Map(cin);

    /* Collect low points. */
    auto lows = m.lowpoints();

    /* Get basin sizes. */
    vector<int> basins;
    for (const auto &l : lows) {
        basins.push_back(m.calc_size(l));
    }

    /* Reverse sort basins. */
    sort(basins.begin(), basins.end(), greater<int>());
    cout << "Answer: " << basins[0] * basins[1] * basins[2] << endl;
    return 0;
}


